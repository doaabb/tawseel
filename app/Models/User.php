<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Collection;
use Laratrust\Traits\LaratrustUserTrait;

class User extends Authenticatable
{
    use LaratrustUserTrait;
    use HasFactory, Notifiable;

    use SoftDeletes ;
    protected $dates = ['deleted_at'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id',
        'name',
        'email',
        'password',
        'coordinator_name','phone','mobile','address','whatsapp','facebook','instagram',
        'image_name','image_path','zone','state','governorate','note',
        'state_id','governorate_id','zone_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function price(){
        return $this->hasMany('App\Models\Prices','user_id');
    }

    public function dealer_price(){
        return $this->hasMany('App\Models\Prices','dealer_id');
    }

    public function order(){
        return $this->hasMany('App\Models\Order','dealer_id');
    }
    public function order_representative(){
        return $this->hasMany('App\Models\Order','representative_id');
    }
    public function order_data_entry(){
        return $this->hasMany('App\Models\Order','data_entry_id');
    }
    public function states(){
        return $this->belongsTo('App\Models\State','state_id');
    }
    public function zones(){
        return $this->belongsTo('App\Models\Zone');
    }
    public function governorates(){
        return $this->belongsTo(Governorate::class,'governorate_id');
    }

    public function data_entry(){
        return $this->hasMany('App\Models\Packages','user_id');
    }
    public function driver_name(){
        return $this->hasMany(Packages::class,'driver_id');
    }
    public function delivery_name(){
        return $this->hasMany(Packages::class,'delivery_id');
    }
    public function activity_order(){
        return $this->hasMany(ActivityOrder::class,'user_id');
    }
    public function address_zone()
    {
        return $this->belongsToMany(Zone::class,'address_user','user_address_id','zone_id');
    }
    public function address_state()
    {
        return $this->belongsToMany(State::class,'address_user','user_address_id','state_id');
    }
    public function address_governorate()
    {
        return $this->belongsToMany(Governorate::class,'address_user','user_address_id','governorate_id');
    }
}
