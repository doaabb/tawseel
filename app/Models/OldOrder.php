<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OldOrder extends Model
{
    use HasFactory;
//    use SoftDeletes ;
    protected $dates = ['deliveredAt','handledAt','merchantOut','stockIn'];

    protected $fillable=['clientName',
        'clientPhone',
        'clientDistrict',
        'details',
        'notes',
        'barCode',
        'Price_item',
        'latitude',
        'longitude',
        'modified',
        'agent_id',
        'clientGovernorate_id',
        'clientState_id',
        'merchant_id',
        'orderPaymentStatus_id',
        'orderPaymentType_id',
        'orderStatus_id',
        'orderType_id',
        'location',
        'delivery_price',
        'cancellation_Price',

    ];
    public function old_user(){
        return $this->belongsTo('App\Models\AuthuserUser');
    }
    public function client_governorate(){
        return $this->belongsTo('App\Models\Governorate');
    }
    public function client_state(){
        return $this->belongsTo('App\Models\State');
    }
}
