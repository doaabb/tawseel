<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderType extends Model
{
    use HasFactory;
    use SoftDeletes ;
    protected $dates = ['deleted_at'];
    public $fillable=['user_id',
        'name',
        'category',
        'description',
    ];

    public function order(){
        return $this->hasMany('App\Models\Order','order_type_id');
    }
}
