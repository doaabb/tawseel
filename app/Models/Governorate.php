<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Governorate extends Model
{
    use HasFactory;
    use SoftDeletes ;
//    public $table=['governorates'];
    protected $dates = ['deleted_at'];
    public $fillable=['name','call_code','image_name','image_path'];
//    public $appends=['governorate_id'];
    public function state(){
        return $this->hasMany('App\Models\State');
    }
    public function zone(){
        return $this->hasMany('App\Models\Zone');
    }

    public function price(){
        return $this->hasMany('App\Models\Price','governorate_id');
    }
    public function order(){
        return $this->hasMany('App\Models\Order','governorate_id');
    }
    public function user(){
        return $this->hasMany('App\Models\User');
    }

    public function address_user(){
        return $this->belongsToMany(User::class,'address_user','user_address_id');
    }
    public function old_user(){
        return $this->belongsTo('App\Models\AuthuserUser');
    }
    public function old_client_order(){
        return $this->belongsTo('App\Models\AuthuserUser','clientGovernorate_id');
    }
//    public function getStudentSignatureAttribute()
//    {governorate_id
//        return $this->getFirstMedia('student_signature');
//    }

}
