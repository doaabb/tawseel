<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class State extends Model
{
    use HasFactory;
    use SoftDeletes ;
    protected $dates = ['deleted_at'];
    public $fillable=['id','name','governorate_id','image_name','image_path'];

    public function governorate(){

        return $this->belongsTo('App\Models\Governorate','governorate_id');
    }

    public function zone(){
        return $this->hasMany('App\Models\Zone');
    }
    public function address_user(){
        return $this->belongsToMany(User::class,'address_user','user_address_id');
    }

    public function price(){
        return $this->hasMany('App\Models\Prices','state_id');
    }

    public function order(){
        return $this->hasMany('App\Models\Order','state_id');
    }
    public function user(){
    return $this->hasMany('App\Models\User');
}
    public function old_user(){
    return $this->belongsTo('App\Models\AuthuserUser');
}

    public function old_client_order(){
        return $this->belongsTo('App\Models\OldOrder','clientState_id');
    }

    public function package_shipper(){
        return $this->hasMany('App\Models\Packages');
    }

    public function package_receiver(){
        return $this->hasMany('App\Models\Packages');
    }
}
