<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Zone extends Model
{
    use HasFactory;
    use SoftDeletes ;
    protected $dates = ['deleted_at'];
    public $fillable=['name','governorate_id','state_id','image_name','image_path'];



    public function governorate(){

        return $this->belongsTo('App\Models\Governorate','governorate_id');
    }
    public function state(){
        return $this->belongsTo('App\Models\State','state_id');
    }
    public function address_user(){
        return $this->belongsToMany(User::class,'address_user','user_address_id');
    }

    public function price(){
        return $this->hasMany('App\Models\Prices','zone_id');
    }

    public function order(){
        return $this->hasMany('App\Models\Order','zone_id');
    }
    public function user(){
        return $this->hasMany('App\Models\User');
    }
}
