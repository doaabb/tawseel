<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Merchant extends Model
{
    use HasFactory;
    use SoftDeletes ;
    protected $dates = ['deleted_at'];
    protected $fillable=['user_ptr_id',
        'phoneAlt',
        'description',
        'activityType',
        'lastOrder',
        'balance',
        'activated',
        'Employee_name',
    ];
    public function old_user(){
        return $this->belongsTo('App\Models\AuthuserUser');
    }
}
