<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Laratrust\Traits\LaratrustUserTrait;

class OldUser extends Model
{
    use LaratrustUserTrait;
    use HasFactory, Notifiable;
    use SoftDeletes ;
    protected $table=['authuser_user'];
    protected $dates = ['deleted_at'];
    public $fillable = ['id',
        'name',
        'email',
        'password',
        'phone','mobile','is_superuser','IDCard','district',
        'state_id','governorate_id','type_id','branch_id','is_active','created','lastlogin'
    ];
//    public $appends = ['id',
//        'name',
//        'email',
//        'password',
//        'phone','mobile','is_superuser','IDCard','district',
//        'state_id','governorate_id','type_id','branch_id','is_active'
//    ];
    protected $hidden = [
        'password',
    ];
    public function state(){
        return $this->hasMany('App\Models\State');
    }
    public function governorate(){

        return $this->belongsTo('App\Models\Governorate','governorate_id');
    }

    public function merchant(){
        return $this->hasMany('App\Models\Merchant','user_ptr_id');
    }
    public function old_order(){
        return $this->hasMany('App\Models\Merchant','merchant_id');
    }
    public function is_activate($id){
        if($id == 0) return false; else return true;
    }
//    public function is_activate($is_active)
//    {
//    }
}
