<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ActivityOrder extends Model
{
    use HasFactory;

    use SoftDeletes ;
    protected $dates = ['deleted_at'];
    public $fillable=['order_id','user_id','package_id','ip_address','description'];


    public function order(){
        return $this->belongsTo(Order::class);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function package(){
        return $this->belongsTo(Packages::class);
    }
}
