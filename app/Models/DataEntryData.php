<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataEntryData extends Model
{
    use HasFactory;
    /*
     * $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('order_id');

            $table->foreign('order_id')->references('id')->on('orders')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->string('pieces')->nullable();
            $table->string('total_price')->nullable();
            $table->text('description')->nullable();
     */
    protected $fillable=['user_id','order_id','pieces','total_price','description'];
    public function order(){
        return $this->belongsTo(Order::class);
    }
}
