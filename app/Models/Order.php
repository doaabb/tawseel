<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use HasFactory;

    use SoftDeletes ;
    protected $dates = ['deleted_at','date','pickup_date'];
    public $fillable=[
        'count_box',
        'time',
        'dealer_name',
        'dealer_id',
        'representative_id',
        'data_entry_id',
        'order_id',
        'order_type_id',
        'order_type',
        'price',
        'price_deliver',
        'price_cancel',
        'governorate_id',
        'state_id',
        'zone_id',
        'description',
        'order_status','is_active','is_calculate',
        ];

//    protected $appends = ['order_photo'];
////to be used with LevelTest and some other models
    public function orderable()
    {
        return $this->morphTo();
    }
    public function media(){
        return $this->hasMany(Media::class,'model_id');
    }

    public function dealer(){
        return $this->belongsTo('App\Models\User');
    }

    public function representative(){
        return $this->belongsTo('App\Models\User');
    }

    public function data_entry(){
        return $this->belongsTo('App\Models\User');
    }

    public function state(){
        return $this->belongsTo('App\Models\State');
    }
    public function zone(){
        return $this->belongsTo('App\Models\Zone');
    }
    public function orderType(){
        return $this->belongsTo('App\Models\OrderType');
    }
    public function dataentrydata(){
        return $this->hasOne(DataEntryData::class,'order_id');
    }

    public function packages(){
        return $this->hasMany(Packages::class,'order_id');
    }
    public function activity_order(){
        return $this->hasMany(ActivityOrder::class,'order_id');
    }
    public function governorate(){

        return $this->belongsToMany('App\Models\Governorate');
    }

}
