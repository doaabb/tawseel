<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Price extends Model
{
    use HasFactory;
    use SoftDeletes ;
    protected $dates = ['deleted_at'];

    protected $fillable=['user_id',
                        'dealer_id',
                        'state_id',
                        'price_deliver',
                        'price_cancel',
                        'zone_id',
                        'governorate_id',
                        'note',
                        ];

    public function user(){
        return $this->belongsTo('App\Models\User');
    }

    public function dealer(){
        return $this->belongsTo('App\Models\User');
    }


    public function state(){
        return $this->belongsTo('App\Models\State');
    }
    public function zone(){
        return $this->belongsTo('App\Models\Zone');
    }

    public function governorate(){

        return $this->belongsTo('App\Models\Governorate');
    }

}
