<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Milon\Barcode\DNS1D;

class Packages extends Model
{
    use HasFactory;
    use SoftDeletes ;
//    use DNS1D;
    protected $dates = ['deleted_at','pickup_date'];
    public $fillable=['user_id',
        'order_id',
        'media_id',
        'user_id',
        'code', 'cc', 'total_price', 'driver', 'picket_by',
        'pickup_date',  'pieces', 'weight', 'state_id_shipper',
        'tel_shipper', 'state_id_receiver', 'tel_receiver', 'barcode', 'random_number', 'path_barcode',
        'driver_id', 'delivery_id',
        'delivery', 'price_customer', 'price_deliver', 'price_cancel',
        ];
    /*
     *
            $table->string('delivery')->nullable();
            $table->double('price_customer')->nullable();
            $table->double('price_deliver')->nullable();
            $table->double('price_cancel')->nullable();
     */

    public function order(){
        return $this->belongsTo(Order::class);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function delivery_name(){
        return $this->belongsTo(User::class);
    }
    public function driver_name(){
        return $this->belongsTo(User::class);
    }

    public function activity_order(){
        return $this->hasMany(ActivityOrder::class,'package_id');
    }
    public function state_shipper(){
        return $this->belongsTo('App\Models\State','state_id_shipper');
    }

    public function state_receiver(){
        return $this->belongsTo('App\Models\State','state_id_receiver');
    }

    public function media(){
        return $this->hasMany(Media::class,'model_id');
    }
//
//'barcode'=>$barcode,
//'random_number'=>$number,
//'path_barcode'=>'/storage/'.$number.$request->image_id.$order->id.'.png'
    public static function get_barcode($number){
        return  base64_decode(DNS1D::getBarcodePNG("2021".$number, "C39+"));
    }

}
