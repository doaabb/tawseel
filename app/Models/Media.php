<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Media extends Model
{
    use HasFactory;

    use SoftDeletes ;

    protected $dates = ['deleted_at'];
//    /*
    /*
             *
                $table->morphs('model');
                $table->string('file_name');
                $table->string('file_path');
                $table->unsignedInteger('order_column')->nullable();
             */
    public $fillable=['file_name','file_path','order_column','model_id', 'model_type'];
    public function order(){
        return $this->belongsToMany(Order::class);
    }
    public function package(){
        return $this->belongsTo(Packages::class);
    }
}
