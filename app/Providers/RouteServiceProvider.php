<?php

namespace App\Providers;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * The path to the "home" route for your application.
     *
     * This is used by Laravel authentication to redirect users after login.
     *
     * @var string
     */
    public const HOME = '/home';

    public const DASHBOARD = '/dashboard/create_product';
    public const MYACCOUNT = '/dashboard/my_account';
    /**
     * The controller namespace for the application.
     *
     * When present, controller route declarations will automatically be prefixed with this namespace.
     *
     * @var string|null
     */
    // protected $namespace = 'App\\Http\\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        $this->configureRateLimiting();

        $this->routes(function () {
            Route::prefix('api')
                ->middleware('api')
                ->namespace($this->namespace)
                ->group(base_path('routes/api.php'));

            Route::middleware('web')
                ->namespace($this->namespace)
                ->group(base_path('routes/web.php'));
            // Admin Route file
            Route::prefix('/dashboard')
                ->namespace($this->namespace)
                ->middleware('web')
                ->group(base_path('routes/admin.php'));
            // Dealer Route file
            Route::prefix('/dealer')
                ->namespace($this->namespace)
                ->middleware('web')
                ->group(base_path('routes/dealer.php'));

            // representative Route file
            Route::prefix('/representative')
                ->namespace($this->namespace)
                ->middleware('web')
                ->group(base_path('routes/representative.php'));

            // representative Route file
            Route::prefix('/brunch')
                ->namespace($this->namespace)
                ->middleware('web')
                ->group(base_path('routes/brunch.php'));

            // Data Entry Route file
            Route::prefix('/date-entry')
                ->namespace($this->namespace)
                ->middleware('web')
                ->group(base_path('routes/data_entry.php'));

            // manager Route file
            Route::prefix('/manager')
                ->namespace($this->namespace)
                ->middleware('web')
                ->group(base_path('routes/manager.php'));

            // manager Route file
            Route::prefix('/accountant')
                ->namespace($this->namespace)
                ->middleware('web')
                ->group(base_path('routes/accountant.php'));

        });
    }

    /**
     * Configure the rate limiters for the application.
     *
     * @return void
     */
    protected function configureRateLimiting()
    {
        RateLimiter::for('api', function (Request $request) {
            return Limit::perMinute(60)->by(optional($request->user())->id ?: $request->ip());
        });
    }
}
