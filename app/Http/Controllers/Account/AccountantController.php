<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;

class AccountantController extends Controller
{
    public function __construct(){
//
        $this->middleware(['auth','role:super_admin|admin|accountant|accountant_dealer|accountant_delivery']);
    }
    public function index(){
        $users = User::all();
        return view('account.index',compact('users'));
    }
    public function dealer_account(){

        $role = Role::where('name','=','dealer')->first();
        $users =$role->users;
        return view('account.dealer',compact('users'));
    }
    public function account_dealer(Request $request){
        $orders = Order::where('dealer_id',$request->id)->get();
        return view('account.account_dealer',compact('orders'));
    }

    public function closed_account(Request $request){
        $orders = Order::where('dealer_id',$request->id)
            ->where('is_calculate',1)
            ->get();
        return view('account.closed_account',compact('orders'));
    }

    public function all_closed_account(){
        $orders = Order::where('is_calculate',1)
            ->get();
        return view('account.all_closed_account',compact('orders'));
    }

    public function alert_orders(){
        $orders = Order::where('is_calculate','!=',1)->where('is_active',1)->get();
        return view('account.alert_orders',compact('orders'));
    }
    public function close_order(Request $request){
        $order = Order::finde($request->id);
        $order->is_calculate = 1;
        $order->update();
        return [
            'status'=>200,
        ];
    }
    public function delivery_account(){

        $role = Role::where('name','=','delivery')->first();
        $users =$role->users;
        return view('account.delivery',compact('users'));
    }
    public function accountant_delivery(Request $request){
        $delivery = $request->id;
        $orders = Order::where('is_active','=',1)->get();
        return view('account.account_delivery',compact('orders','delivery'));
    }
}
