<?php

namespace App\Http\Controllers\Representative;

use App\Http\Controllers\Controller;
use App\Models\ActivityOrder;
use App\Models\Media;
use App\Models\Order;
use App\Models\Packages;
use App\Models\Price;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use phpDocumentor\Reflection\Types\Null_;

class OrderController extends Controller
{
    public function __construct(){
//
        $this->middleware(['auth','role:super_admin|admin|representative']);
    }

    public function index(){
        $user = auth()->user()->address_zone->pluck('id')->toArray();

        $order= Order::where('zone_id','!=',null)
            ->whereIn('zone_id',$user)
            ->orWhere('zone_id',auth()->user()->zone_id)
            ->whereNull('representative_id')->whereNotNull('zone_id')->get();

        $user_state = auth()->user()->address_state->pluck('id')->toArray();
        $order_state= Order::
        where(function ($query) use ($user_state , $user) {
            return $query->whereIn('state_id',$user_state)
                ->whereNotIn('zone_id',$user);
        })->where(function ($query) use ($user_state , $user) {
            return $query->where('zone_id',null)
                ->orWhere('state_id',auth()->user()->state_id);
        })->whereNull('representative_id')
            ->get();

        $user_gov = auth()->user()->address_governorate->pluck('id')->toArray();
        $order_gov= Order::
        where(function ($query) use ($user_state , $user , $user_gov) {
            return $query->whereIn('governorate_id',$user_gov)
                ->whereNotIn('state_id',$user_state)
                ->whereNotIn('zone_id',$user);
        })
            ->whereNull('representative_id')->get();
//        dd($order_state,$order_gov);

        return view('representative.index',compact('order','order_state','order_gov'));
    }
    public function my_order(){
        $order= Order::where('representative_id','=',auth()->user()->id)->get();
        return view('representative.my_order',compact('order'));

    }

    public function order_alert(){
        $order= Order::where('representative_id','=',auth()->user()->id)->where('order_status',4)->get();
        return view('representative.order_alert',compact('order'));

    }
    public function take_order(Request $request){

        $order = Order::find($request->id);
        if ($order != null || $order->representative_id != null) {
            $order->representative_id = auth()->user()->id;
            $order->order_status = 1;
            $order->update();
            ActivityOrder::create([
                'order_id'=>$order->id,
                'user_id'=>auth()->user()->id,
                'ip_address'=>$this->getUserIpAddr(),
                'description'=> ' Take Your Order '
            ]);
            return[
                'status_code'=>200,
            ];
        }else{
            return response()->json('no data');
        }
    }
    public function packages_rep(){
        $order= Packages::where('driver_id','=',auth()->user()->id)->get();
        return view('representative.packages',compact('order'));
    }

    public function delivery(){
        $order= Packages::where('driver_id','=',auth()->user()->id)->get();
        return view('representative.delivery',compact('order'));
    }
    protected function is_active(Request $request){
        $role = Packages::find($request->id);

//        dd($role->order);
        if ($role){
            $role->is_delivery = 1;

            $role->update();
            $activity = ActivityOrder::create([
                'order_id'=>$role->order_id??1,
                'user_id'=>auth()->user()->id,
                'ip_address'=>$this->getUserIpAddr(),
                'description'=>'Deliver package'.$role->code

            ]);
//            dd(count(Packages::where('order_id',$role->order_id)->where('is_delivery',null)->get()));
            if(count(Packages::where('order_id',$role->order_id)->where('is_delivery',null)->get()) == 0){
                $order = Order::find($role->order_id);
//                dd($order);
                $order->is_active = 1;
                $order->update();

                $activity = ActivityOrder::create([
                    'order_id'=>$order->id,
                    'user_id'=>auth()->user()->id,
                    'ip_address'=>$this->getUserIpAddr(),
                    'description'=>'It is Done ,All Packages Sent'

                ]);
            }
            return[
                'status_code'=>200,
            ];
        }else{
            return response()->json(['error'=>$role->errors()]);
        }

    }

    public function enable(Request $request){
        $order = Order::find($request->id);
        return view('representative.enable',compact('order'));
    }
    public function store_enable(Request $request){

        $message = Validator::make($request->all(), [

            'file' => 'required',

        ]);

        $imageName = null;
        $path = null;
        $extension = null;
        $order=Order::find($request->id);
        $log = false;
        if($order->media != null){
            $log = true;
        }else{
            $log = false;
        }
        if ($message->passes()) {
            foreach ($request->file('file') as $image) {
                $imagePath = $image;
                $imageName = time() . $image->getClientOriginalName();
                $extension = $imagePath->getClientOriginalExtension();
                $path = $image->storeAs('uploads', $imageName, 'public');
                $media = DB::table('media')
                    ->insert([
//                'order_column'=> $request->id,
                        'file_name' => $imageName,
                        'file_path' => '/storage/' . $path,
                        'model_type' => $extension,
                        'model_id' => $request->id,
                    ]);
            }


            $order->order_status = 2;
            $order->update();
            if($log == false){
                $activity = ActivityOrder::create([
                    'order_id'=>$order->id,
                    'user_id'=>auth()->user()->getAuthIdentifier(),
                    'ip_address'=>$this->getUserIpAddr(),
                    'description'=>'Receive your order'

                ]);
            }

            return [
                'status_code' => 200,
            ];
        }else{
            return response()->json(['error'=>$message->errors()]);
        }

    }

    function getUserIpAddr(){
        if(!empty($_SERVER['HTTP_CLIENT_IP'])){
            //ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            //ip pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    public function store_order_manager(Request $request){
        $order = Packages::find($request->id);
        $message = Validator::make($request->all(), [
            'driver_id'=>'required',
        ]);

        if ($message->passes()) {
            $order->delivery_id= $request->driver_id;
            $order->update();
            $activity = ActivityOrder::create([
                'order_id'=>$order->order_id,
                'package_id'=>$order->id,
                'user_id'=>auth()->user()->id,
                'ip_address'=>$this->getUserIpAddr(),

                'description'=>'Sent Order To Customer '

            ]);
            return[
                'status_code'=>200,
            ];
        }else{
            return response()->json(['error'=>$message->errors()]);
        }
    }

    protected function order_cancel_rep(Request $request){
        $order = Packages::find($request->id);
        $price_cancel = Price::where('state_id',$order->state_id_receiver)->first();
        $message = Validator::make($request->all(), [
            'description' => 'required',
        ]);
        if ($message->passes()) {
            $order->price_cancel= $price_cancel->price_cancel;
            $order->is_cancel= 1;
            $order->description= $request->description;
            $order->update();
            return[
                'status_code'=>200,
            ];
        }else{
            return response()->json(['error'=>$message->errors()]);
        }
    }
    protected function done_deliver(Request $request){
        $order = Packages::find($request->id);
        $price_cancel = Price::where('state_id',$order->state_id_receiver)->first();
        if($order){
            $order->price_deliver= $price_cancel->price_deliver;
            $order->price_cancel= null;
            $order->is_cancel= null;
            $order->update();
            return[
                'status_code'=>200,
            ];
        }else{
            return response()->json(['error'=>$order->errors()]);
        }
    }
    public function update_img_res(Request $request){
        $message = Validator::make($request->all(), [

            'file' => 'required',

        ]);
        $media = Media::find($request->image_id);

        if ($message->passes()) {
                $image = $request->file('file');
                $imagePath = $image;
                $imageName = time() . $image->getClientOriginalName();
                $extension = $imagePath->getClientOriginalExtension();
                $path = $image->storeAs('uploads', $imageName, 'public');
                $media->file_name = $imageName;
                $media->file_path =  '/storage/' . $path;
                $media->model_type =  $extension;
                $media->model_id =  $request->order_id;
                $media->alert_entry =  1;

                $media->update();
                $activity = ActivityOrder::create([
                    'order_id'=>$request->order_id,
                    'user_id'=>auth()->user()->getAuthIdentifier(),
                    'ip_address'=>$this->getUserIpAddr(),
                    'description'=>'Update The Package'

                ]);

            return [
                'image_src' =>$media->file_path,
                'status_code' => 200,
            ];
        }else{
            return response()->json(['error'=>$message->errors()]);
        }
//        dd($request->file('file'));
    }

    protected function sdelete(Request $request){
        $media = Media::find($request->id);
        $media->alert_entry =  0;
        $media->update();
        if ($media->delete()){
            return[
                'status_code'=>200,
            ];
        }else{
            return response()->json(['error'=>$media->errors()]);
        }
    }
}
