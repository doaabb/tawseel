<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Governorate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class GovernorateController extends Controller
{
    public function __construct(){
//
        $this->middleware(['auth','role:super_admin|admin']);
    }
    public function index(){

        $role = Governorate::all();
//        dd($role);
        return view('admin.governorate.index')->with('role',$role);
    }
    public function create(){
        $gov = Governorate::all();

        return view('admin.governorate.create',compact('gov'));
    }
    public function select_store(Request $request){
//        dd($request->all());
        if ($request->gove){
//            dd('gover');
            $this->store($request);

        }elseif ($request->state){
//            dd('state');
            StateController::store($request);
        }elseif ($request->zone){
//            dd('zone');
            zoneController::store($request);
        }else{
            return response()->json(['error'=> 'Dont Save']);
        }
    }
    public function store($request){

        $message = Validator::make($request->all(), [
            'name' => 'required',
        ]);
//        dd($request->all());
        if ($message->passes()) {

            // Store Data in DATABASE from HERE
            $role = Governorate::create([
                'name'      => $request->name,
                'call_code' => $request->call_code,
            ]);

            return[
                'status_code'=>200,
            ];
        }else{
            return response()->json(['error'=>$message->errors()]);

        }

//       return view('admin.role.create', ['pageConfigs' => $pageConfigs]);
    }
    public function edit(Request $request){
        $pageConfigs = ['pageHeader' => false];
        $role = Governorate::find($request->id);
//       dd($role);
        return view('admin.governorate.edit', ['pageConfigs' => $pageConfigs])->with('role',$role);
    }
    public function update(Request $request){


        $role = Governorate::find($request->id);
        $message = Validator::make($request->all(), [
            'name' => 'required|unique:roles',
        ]);
        if ($message->passes()) {
            $role->name = $request->name;
            $role->call_code = $request->call_code;

            return[
                'status_code'=>200,
            ];
        }

        return response()->json(['error'=>$message->errors()]);
//       return view('admin.role.create', ['pageConfigs' => $pageConfigs]);
    }
    protected function sdelete(Request $request){
        $role = Governorate::find($request->id);

//       $role->delete();
        if ($role->delete()){
            return[
                'status_code'=>200,
            ];
        }

        return response()->json(['error'=>$role->errors()]);
    }
}
