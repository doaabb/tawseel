<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Governorate;
use App\Models\State;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class StateController extends Controller
{
    public function __construct(){
//
        $this->middleware(['auth','role:super_admin|admin']);
    }
    public function index(){

        $role = State::all();
//        dd($role);
        return view('admin.state.index')->with('role',$role);
    }
    public function create(){
        $role = State::all();
        $gov = Governorate::all();
        return view('admin.state.create',compact('role','gov'));
    }
    static public function store( $request){

//        dd($request->all());
        $message = Validator::make($request->all(), [
            'name' => 'required',
        ]);
        if ($message->passes()) {

            $role = State::create([
                'name'      => $request->name,
                'governorate_id' => $request->governorate_id,
            ]);

            return[
                'status_code'=>200,
            ];
        }else{
            return response()->json(['error'=>$message->errors()]);

        }

//       return view('admin.role.create', ['pageConfigs' => $pageConfigs]);
    }
    public function edit(Request $request){
        $role = State::find($request->id);
//       dd($role);
        return view('admin.state.edit')->with('role',$role);
    }
    public function update(Request $request){


        $role = State::find($request->id);
        $message = Validator::make($request->all(), [
            'name' => 'required|unique:roles',
        ]);
        if ($message->passes()) {
            $role->name = $request->name;
            $role->governorate_id = $request->governorate_id;

            return[
                'status_code'=>200,
            ];
        }

        return response()->json(['error'=>$message->errors()]);
//       return view('admin.role.create', ['pageConfigs' => $pageConfigs]);
    }
    protected function sdelete(Request $request){
        $role = State::find($request->id);

//       $role->delete();
        if ($role->delete()){
            return[
                'status_code'=>200,
            ];
        }

        return response()->json(['error'=>$role->errors()]);
    }
}
