<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Governorate;
use App\Models\Role;
use App\Models\State;
use App\Models\User;
use App\Models\Zone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{

    public function __construct(){
//
        $this->middleware(['auth','role:super_admin|admin']);
    }
    public function index(){
        $users = User::all();
        return view('admin.users.index',compact('users'));
    }
    public function create(){
        $state = State::all();
        $gov = Governorate::all();
        $zone = Zone::all();
        $role = Role::all();
        return view('admin.users.create',compact('gov','state','zone','role'));
    }
    public function store(Request $request){
        $message = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'mobile' => ['required', 'unique:users'],
            'whatsapp' => ['unique:users'],
            'password' => ['required', 'string', 'min:8'],
        ]);
        if ($message->passes()) {
            $imageName = null;
            $path = null;
            if ($request->file('file')) {
                $imagePath = $request->file('file');
                $imageName = time().$imagePath->getClientOriginalName();
                $path =  $request->file('file')->storeAs('uploads', $imageName, 'public');
            }
            $governorate= $state= $zone = null;
            if($request->governorate_id != null){
                $governorate = Governorate::find($request->governorate_id)->name;
            }
           $user= User::create([
                'name'              => $request->name,
                'email'             => $request->email,
                'password'          => Hash::make($request->password),
                'coordinator_name'  => $request->coordinator_name,
                'mobile'            => $request->mobile,
                'address'           => $request->address,
                'whatsapp'          => $request->whatsapp,
                'facebook'          => $request->facebook,
                'instagram'         => $request->instagram,
                'note'              => $request->note,
                'governorate'       => $request->governorate,
                'image_name'       => $imageName,
                'image_path'       => $path,
                'governorate_id'   => $request->governorate_id,
                'state_id'          => $request->state_id,
                'zone_id'           => $request->zone_id,
            ]);// 'image_name','image_path',
            $user->attachRole($request->role);
            return[
                'status_code'=>200,
            ];
        }else{
            return response()->json(['error'=>$message->errors()]);

        }

    }
    public function edit(Request $request){
        $role = User::find($request->id);
        return view('admin.users.edit')->with('role',$role);
    }
    public function update(Request $request){
        $role = User::find($request->id);
        $message = Validator::make($request->all(), [
            'name' => 'required',
        ]);
        if ($message->passes()) {
            $role->name = $request->name;
            $role->governorate_id = $request->governorate_id;

            return[
                'status_code'=>200,
            ];
        }
        return response()->json(['error'=>$message->errors()]);
    }
    protected function sdelete(Request $request){
        $role = User::find($request->id);
        if ($role->delete()){
            return[
                'status_code'=>200,
            ];
        }
        return response()->json(['error'=>$role->errors()]);
    }
}
