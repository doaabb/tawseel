<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Governorate;
use App\Models\State;
use App\Models\Zone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class zoneController extends Controller
{
    public function __construct(){
//
        $this->middleware(['auth','role:super_admin|admin']);
    }
    public function index(){
        $role = Zone::all();
        return view('admin.zone.index')->with('role',$role);
    }
    public function create(){
//        $state = State::all();
        $gov = Governorate::all();
        return view('admin.zone.create',compact('gov'));
    }
   static public function store($request){
        $message = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        $gover = State::where('id',$request->state_id)->first();
        if ($message->passes()) {
            $role = Zone::create([
                'name'      => $request->name,
                'state_id' => $request->state_id,
                'governorate_id' => $gover->governorate->id,
            ]);

            return[
                'status_code'=>200,
            ];
        }else{
            return response()->json(['error'=>$message->errors()]);

        }

    }
    public function edit(Request $request){
        $role = Zone::find($request->id);
        return view('admin.zone.edit')->with('role',$role);
    }
    public function update(Request $request){


        $role = Zone::find($request->id);
        $message = Validator::make($request->all(), [
            'name' => 'required|unique:roles',
        ]);
        if ($message->passes()) {
            $role->name = $request->name;
            $role->governorate_id = $request->governorate_id;

            return[
                'status_code'=>200,
            ];
        }
        return response()->json(['error'=>$message->errors()]);
    }
    protected function sdelete(Request $request){
        $role = Zone::find($request->id);

        if ($role->delete()){
            return[
                'status_code'=>200,
            ];
        }

        return response()->json(['error'=>$role->errors()]);
    }
}
