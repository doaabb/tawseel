<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ActivityOrder;
use App\Models\Governorate;
use App\Models\OldOrder;
use App\Models\Order;
use App\Models\OrderType;
use App\Models\Packages;
use App\Models\Price;
use App\Models\Role;
use App\Models\State;
use App\Models\User;
use App\Models\Zone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class OrderController extends Controller
{


    public function __construct(){
//
        $this->middleware(['auth','role:super_admin|admin']);
    }
    public function index(){
        $state = State::all();
        $gov = Governorate::all();
        $zone = Zone::all();
        $order_type= OrderType::all();
        $order = Order::all();
        return view('admin.order.index',compact('order','gov','state','zone','order_type'));
    }
    public function show(Request $request){
        $order = Order::find($request->id);
        return view('admin.order.order',compact('order'));
    }

    public function read_barcode(){
        //receipt_from_delivery.blade.php
        return view('admin.order.receipt_from_delivery');
    }
    public function orders_old($id=0){
        $state = State::all();
        $gov = Governorate::all();
        $zone = Zone::all();
        $order_type= OrderType::all();
        $order = OldOrder::skip($id)->take(1000)->get();
        return view('admin.order.orders_old',compact('order','gov','state','zone','order_type'));
    }
    public function order_type(){
        $order = OrderType::all();
        return view('admin.order.order_type',compact('order'));
    }
    public function to_representative(Request $request){
        $order = Order::find($request->id);
        if($order){
            $role = Role::where('name','=','representative')->first();
            $users_zone = $role->users->where('zone_id',$order->zone_id);
            $users = $role->users->where('governorate_id','!=',$order->governorate_id)->where('state_id','!=',$order->state_id)->where('zone_id','!=',$order->zone_id);

//            dd($order,$users_zone,$role->users);
            $users_state = $role->users->where('state_id',$order->state_id)->where('zone_id','!=',$order->zone_id);
            $users_governorates = $role->users->where('governorate_id',$order->governorate_id)->where('state_id','!=',$order->state_id);

            return view('admin.order.to_representative',compact('users','order','users_governorates','users_state','users_zone'));

        }else{
            abort(404);
        }
    }

    function getUserIpAddr(){
        if(!empty($_SERVER['HTTP_CLIENT_IP'])){
            //ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            //ip pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }
    public function sent_to_representative(Request $request){

        $order = Order::find($request->id);
//        dd($request->all(),$request->id,$order);
        if ($order != null) {
            $order->representative_id = $request->representative;
            $order->order_status = 1;
            $order->update();

            $activity = ActivityOrder::create([
                'order_id'=>$order->id,
                'user_id'=>auth()->user()->id,
                'ip_address'=>$this->getUserIpAddr(),
                'description'=> auth()->user()->name. ' Sent Order To representative'

            ]);
            return[
                'status_code'=>200,
            ];
        }else{
            return response()->json('no data');
        }
    }
    public function to_data_entry(Request $request){
        $order = Order::find($request->id);
        if($order){
            $role = Role::where('name','=','data_entry')->first();
            $users = $role->users;

            return view('admin.order.to_data_entry',compact('users','order'));

        }else{
            abort(404);
        }
    }
    public function sent_to_data_entry(Request $request){

        $order = Order::find($request->id);
//        dd($request->all(),$request->id,$order);
        if ($order != null) {
            $order->data_entry_id = $request->data_entry_id;
            $order->order_status = 3;
            $order->update();
            return[
                'status_code'=>200,
            ];
        }else{
            return response()->json('no data');
        }
    }
    public function order_type_store(Request $request){
        $message = Validator::make($request->all(), [
            'name' => 'required',
        ]);
        if ($message->passes()) {
            OrderType::create([
                'name'              => $request->name,
                'category'          => $request->category,
                'description'       => $request->description,
                'user_id'           => auth()->user()->id,
            ]);
            return[
                'status_code'=>200,
            ];
        }else{
            return response()->json(['error'=>$message->errors()]);
        }
    }
    public function list(){
        $role = Order::all();
        return view('admin.order.index')->with('role',$role);
    }
    public function create(){
        $state = State::all();
        $gov = Governorate::all();
        $zone = Zone::all();
        $order_type= OrderType::all();
        return view('admin.order.create',compact('gov','state','zone','order_type'));
    }
    public function store(Request $request){
        $message = Validator::make($request->all(), [
            'count_box' => 'required',
            'state_id' => 'required',
        ]);

        $state = State::where('id',$request->state_id)->first();
        $order_type = OrderType::find($request->order_type_id)  ;
        $order_type= ($order_type != null )?$order_type->name : false;
//        dd($order_type);
        if ($message->passes()) {
            $role = Order::create([
                'time'              => $request->time,
                'dealer_name'       => auth()->user()->name,
                'dealer_id'         => auth()->user()->id,
                'count_box'         => $request->count_box,
                'order_type'        => $order_type,
                'order_type_id'     => $request->order_type_id,
                'price'             => $request->price,
                'price_deliver'     => $request->price_deliver,
                'zone_id' => $request->zone,
                'state_id' => $state->id,
                'governorate_id' => $state->governorate->id,
            ]);

            return[
                'status_code'=>200,
            ];
        }else{
            return response()->json(['error'=>$message->errors()]);

        }

    }
    public function edit_order(Request $request){
        $order = Order::find($request->id);
        $gov = Governorate::all();
        $order_type= OrderType::all();
        return view('admin.order.edit_order',compact('order','gov','order_type'));
    }
    public function update_order(Request $request){


        $order = Order::find($request->id);
        $message = Validator::make($request->all(),
            [
                'count_box' => 'required',
                'state_id' => 'required',
            ]);


        $state = State::where('id',$request->state_id)->first();
        $order_type = OrderType::find($request->order_type_id)  ;
        $order_type= ($order_type != null )?$order_type->name : false;
        $price_deliver = Price::where('state_id',$state->id)->first();

        $price_deliver= ($price_deliver != null )?$price_deliver->price_deliver : false;
//        dd($order,$request->id);
        if ($message->passes()) {
            $order->time= $request->time;
            $order->dealer_name= auth()->user()->name;
            $order->dealer_id= auth()->user()->id;
            $order->count_box= $request->count_box;
            $order->order_type= $order_type;
            $order->order_type_id= $request->order_type_id;
            $order->price= $request->price;
            $order->price_deliver= $price_deliver;
            $order->zone_id= $request->zone;
            $order->state_id=  $state->id;
            $order->governorate_id=  $state->governorate->id;
            $order->update();

            $activity = ActivityOrder::create([
                'order_id'=>$order->id,
                'user_id'=>auth()->user()->id,
                'ip_address'=>$this->getUserIpAddr(),
                'description'=>'Update Your Order'

            ]);
            return[
                'status_code'=>200,
                'zone'=>$order->zone? $order->zone->name: '',
                'state'=>$order->state->name,
                'id'=> $order->id
            ];
        }else{
            return response()->json(['error'=>$message->errors()]);
        }
    }
    protected function sdelete(Request $request){
        $role = Order::find($request->id);

        if ($role->delete()){
            return[
                'status_code'=>200,
            ];
        }else{
            return response()->json(['error'=>$role->errors()]);
        }


    }

    protected function dis_active(Request $request){
        $role = Order::find($request->id);

        if ($role){
            $role->is_active = 1;

            $role->update();
            $activity = ActivityOrder::create([
                'order_id'=>$role->id,
                'user_id'=>auth()->user()->id,
                'ip_address'=>$this->getUserIpAddr(),
                'description'=>'It is Done'

            ]);
            return[
                'status_code'=>200,
            ];
        }else{
            return response()->json(['error'=>$role->errors()]);
        }

    }

    protected function sdelete_order_type(Request $request){
        $role = OrderType::find($request->id);

        if ($role->delete()){
            return[
                'status_code'=>200,
            ];
        }else{
            return response()->json(['error'=>$role->errors()]);
        }
    }
    protected function order_cancel(Request $request){
        $order = Packages::find($request->id);
        $price_cancel = Price::where('state_id',$order->state_id_receiver)->first();
        $message = Validator::make($request->all(), [
            'description' => 'required',
        ]);
        if ($message->passes()) {
            $order->price_cancel= $price_cancel->price_cancel;
            $order->description= $request->description;
            $order->update();
            return[
                'status_code'=>200,
            ];
        }else{
            return response()->json(['error'=>$message->errors()]);
        }
    }
    protected function order_deliver(Request $request){
        $order = Packages::find($request->id);
        $price_cancel = Price::where('state_id',$order->state_id_receiver)->first();
        if($order){
            $order->price_deliver= $price_cancel->price_deliver;
            $order->update();
            return[
                'status_code'=>200,
            ];
        }else{
            return response()->json(['error'=>$order->errors()]);
        }
    }
}
