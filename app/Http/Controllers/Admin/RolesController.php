<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RolesController extends Controller
{

    public function __construct(){
//
        $this->middleware(['auth','role:super_admin|admin']);
    }
    public function index(){
        $pageConfigs = ['pageHeader' => false];
        $role = Role::all();
//       dd($role);
        return view('admin.role.index', ['pageConfigs' => $pageConfigs])->with('role',$role);
    }
    public function create(){
        $pageConfigs = ['pageHeader' => false];
        $role = Role::all();
//       dd($role);
        return view('admin.role.create', ['pageConfigs' => $pageConfigs])->with('role',$role);
    }
    public function store(Request $request){

        $pageConfigs = ['pageHeader' => false];
        $message = Validator::make($request->all(), [
            'name' => 'required',
        ]);
        if ($message->passes()) {

            // Store Data in DATABASE from HERE
            $role = Role::updateOrCreate([
                'name' => $request->name
            ], [
                'display_name' => $request->display_name,
                'description' => $request->description,
            ]);

            return[
                'status_code'=>200,
            ];
        }

        return response()->json(['error'=>$message->errors()]);
//       return view('admin.role.create', ['pageConfigs' => $pageConfigs]);
    }
    public function edit_role(Request $request){
        $pageConfigs = ['pageHeader' => false];
        $role = Role::find($request->id);
//       dd($role);resources/views/admin/role/edit.blade.php
        return view('admin.role.edit', ['pageConfigs' => $pageConfigs])->with('role',$role);
    }
    public function update_role(Request $request){


        $role = Role::find($request->id);
        $message = Validator::make($request->all(), [
            'name' => 'required|unique:roles',
        ]);
        if ($message->passes()) {
            $role->name = $request->name;
            $role->description = $request->description;
            $role->display_name = $request->display_name;

            return[
                'status_code'=>200,
            ];
        }

        return response()->json(['error'=>$message->errors()]);
//       return view('admin.role.create', ['pageConfigs' => $pageConfigs]);
    }
    protected function sdelete_role(Request $request){
        $role = Role::find($request->id);

//       $role->delete();
        if ($role->delete()){
            return[
                'status_code'=>200,
            ];
        }

        return response()->json(['error'=>$role->errors()]);
    }
    public function res_role(){

        $role = Role::where('name','=','representative')->first();
        $users = $role->users;
        return view('admin.role.representative',compact('users'));
    }

    public function acc_role(){

        $role = Role::where('name','=','accountant_dealer')->first();
        $role2 = Role::where('name','=','accountant_delivery')->first();
        $users = $role->users;
        $users2 = $role2->users;
        return view('admin.role.accountant',compact('users','users2'));
    }
    public function store_res(Request $request){
        $user = User::find($request->user_id);
//        dd($user->address_zone());
        if($request->zone_id){
            $user->address_zone()->attach( $request->zone_id);

        }

        if($request->state_id){
            $user->address_state()->attach( $request->state_id);

        }

        if($request->governorate_id){
            $user->address_governorate()->attach( $request->governorate_id);

        }

        return [
            'status_code'=>200,
            'user'=>$user->id
        ];
    }
}
