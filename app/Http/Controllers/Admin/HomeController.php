<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct(){
//
        $this->middleware(['auth','role:super_admin|admin']);
    }
    public function index(){
//        dd(\Auth::user());
        return view('admin.index');
    }
}
