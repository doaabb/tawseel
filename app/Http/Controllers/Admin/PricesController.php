<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Governorate;
use App\Models\Price;
use App\Models\Role;
use App\Models\State;
use App\Models\Zone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PricesController extends Controller
{

    public function __construct(){
//
        $this->middleware(['auth','role:super_admin|admin']);
    }
    public function index(){
        $role = Role::where('name','=','dealer')->first();
        $users = $role->users;
        $prices = Price::all();
        $gov = Governorate::all();
        return view('admin.order.price',compact('prices', 'gov','users'));
    }

    public function store(Request $request){
        $message = Validator::make($request->all(), [
            'state_id' => 'required',
        ]);
        $state = State::where('id',$request->state_id)->first();

//        dd($request->price_deliver);
//        dd($zone->governorate);
        if ($message->passes()) {

            $role = Price::create([
                'user_id'      => auth()->user()->id,
                'dealer_id' => $request->dealer_id,
                'price_deliver' => $request->price_deliver,
                'price_cancel' => $request->price_cancel,
                'note' => $request->note,
                'state_id' => $state->id,
                'governorate_id' => $state->governorate->id,
            ]);

            return[
                'status_code'=>200,
            ];
        }else{
            return response()->json(['error'=>$message->errors()]);

        }

    }
    public function edit(Request $request){
        $role = Zone::find($request->id);
        return view('admin.zone.edit')->with('role',$role);
    }
    public function update(Request $request){


        $role = Zone::find($request->id);
        $message = Validator::make($request->all(), [
            'name' => 'required|unique:roles',
        ]);
        if ($message->passes()) {
            $role->name = $request->name;
            $role->governorate_id = $request->governorate_id;

            return[
                'status_code'=>200,
            ];
        }
        return response()->json(['error'=>$message->errors()]);
    }
    protected function sdelete(Request $request){
        $role = Price::find($request->id);

        if ($role->delete()){
            return[
                'status_code'=>200,
            ];
        }

        return response()->json(['error'=>$role->errors()]);
    }
}
