<?php

namespace App\Http\Controllers\Dealer;

use App\Http\Controllers\Controller;
use App\Models\ActivityOrder;
use App\Models\Governorate;
use App\Models\Order;
use App\Models\OrderType;
use App\Models\Packages;
use App\Models\Price;
use App\Models\State;
use App\Models\Zone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class OrderController extends Controller
{

    public function __construct(){
//
        $this->middleware(['auth','role:super_admin|admin|dealer']);
    }
    function getUserIpAddr(){
        if(!empty($_SERVER['HTTP_CLIENT_IP'])){
            //ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            //ip pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }
    public function index(){
        $role = Order::all();
        return view('dealer.order.create')->with('role',$role);
    }
    public function list(){
        $role = Order::all();
        return view('dealer.order.index')->with('role',$role);
    }
    public function create(){
        $state = State::all();
        $gov = Governorate::all();
        $zone = Zone::all();
        $order_type= OrderType::all();
        return view('dealer.order.create',compact('gov','state','zone','order_type'));
    }
    public function store(Request $request){


        $message = Validator::make($request->all(), [
            'count_box' => 'required',
            'state_id' => 'required',
        ]);


        $state = State::where('id',$request->state_id)->first();
        $order_type = OrderType::find($request->order_type_id)  ;
        $order_type= ($order_type != null )?$order_type->name : false;
        $price_deliver = Price::where('state_id',$state->id)->first();

        $price_deliver= ($price_deliver != null )?$price_deliver->price_deliver : false;
//        dd($order_type);
        if ($message->passes()) {
            $info = Order::create([
                'time'              => $request->time,
                'dealer_name'       => auth()->user()->name,
                'dealer_id'         => auth()->user()->id,
                'count_box'         => $request->count_box,
                'order_type'        => $order_type,
                'order_type_id'     => $request->order_type_id,
                'price'             => $request->price,
                'price_deliver'     => $price_deliver,
                'zone_id' => $request->zone,
                'state_id' => $state->id,
                'governorate_id' => $state->governorate->id,
            ]);
            $activity = ActivityOrder::create([
                'order_id'=>$info->id,
                'user_id'=>auth()->user()->id,
                'ip_address'=>$this->getUserIpAddr(),
                'description'=>'Add New Order'

            ]);

            return[
                'status_code'=>200,
                'info' =>$info,
                'zone'=>$info->zone? $info->zone->name: '',
                'state'=>$info->state->name,
                'id'=> $info->id
            ];
        }else{
            return response()->json(['error'=>$message->errors()]);
        }
    }
    public function edit(Request $request){
        $order = Order::find($request->id);
        $state = State::all();
        $gov = Governorate::all();
        $zone = Zone::all();
        $order_type= OrderType::all();
        return view('dealer.order.edit_order',compact('order','gov','state','zone','order_type'));
    }
    public function update(Request $request){

        $order = Order::find($request->id);
        $message = Validator::make($request->all(),
            [
            'count_box' => 'required',
            'state_id' => 'required',
        ]);


        $state = State::where('id',$request->state_id)->first();
        $order_type = OrderType::find($request->order_type_id)  ;
        $order_type= ($order_type != null )?$order_type->name : false;
        $price_deliver = Price::where('state_id',$state->id)->first();

        $price_deliver= ($price_deliver != null )?$price_deliver->price_deliver : false;
//        dd($order,$request->id);
        if ($message->passes()) {
            $order->time= $request->time;
            $order->dealer_name= auth()->user()->name;
            $order->dealer_id= auth()->user()->id;
            $order->count_box= $request->count_box;
            $order->order_type= $order_type;
            $order->order_type_id= $request->order_type_id;
            $order->price= $request->price;
            $order->price_deliver= $price_deliver;
            $order->zone_id= $request->zone;
            $order->state_id=  $state->id;
            $order->governorate_id=  $state->governorate->id;
            $order->update();

            $activity = ActivityOrder::create([
                'order_id'=>$order->id,
                'user_id'=>auth()->user()->id,
                'ip_address'=>$this->getUserIpAddr(),
                'description'=>'Update Order'

            ]);
            return[
                'status_code'=>200,
                'zone'=>$order->zone? $order->zone->name: '',
                'state'=>$order->state->name,
                'id'=> $order->id
            ];
        }else{
            return response()->json(['error'=>$message->errors()]);
        }
    }
    protected function sdelete(Request $request){
        $role = Order::find($request->id);

        if ($role->delete()){
            return[
                'status_code'=>200,
            ];
        }

        return response()->json(['error'=>$role->errors()]);
    }

    public function get_zone(Request $request){
        $state = State::find($request->id);

        $zone = $state->zone->pluck('id','name')->toJson();
        return[
            'status'=>200,
            'zone' => $zone,
        ];
    }
    public function get_state(Request $request){
        $gove = Governorate::find($request->id);

        $state = $gove->state->pluck('id','name')->toJson();
        return[
            'status'=>200,
            'state' => $state,
        ];
    }
    public function get_status(Request $request){
        $activity = ActivityOrder::where('order_id',$request->id)->get();
         if($activity){
             $logs =array();
             $user_name =array();
             $user_role =array();
             $created_date =array();
             $created_time =array();
            foreach ($activity as $index=>$log){
                $user_name[] = [$index=>$log->user->name];
                $user_role[] = $log->user->roles->first()->name;
                $created_date[] = date('Y-m-d',strtotime($log->created_at));
                $created_time[] = date('H:m:s a',strtotime($log->created_at));
                $logs[] = $log->toJson();
            }
            $new_log  = $activity->pluck('description');
//             dd( $user_role[0]);
             return [
                 'status'=>200,
                 'logs'=>$logs,
                 'new_log'=>$new_log,
                 'activity'=>$activity->toJson(),
                 'user_name'=>$user_name,
                 'user_role'=>$user_role,
                 'created_date'=>$created_date,
                 'created_time'=>$created_time,
             ];
        }else{
             return [
                 'status'=>500
             ];
         }
    }


    public function account_dealer(Request $request){
        $orders = Order::where('dealer_id',auth()->user()->id)->get();
        return view('dealer.order.statement',compact('orders'));
    }
    public function deliver_order(Request $request){
        $orders = Order::where('dealer_id',auth()->user()->id)
            ->where('is_active','=',1)->where('is_calculate','!=',1)->get();
        return view('dealer.order.deliver_order',compact('orders'));
    }
    public function cancel_orders(Request $request){
        $orders = Order::where('dealer_id',auth()->user()->id)
            ->where('is_active','=',1)
            ->where('is_calculate',0)->get();
        return view('dealer.order.cancel_orders',compact('orders'));
    }
    public function waiting_orders(Request $request){
        $orders = Order::where('dealer_id',auth()->user()->id)
            ->where('is_active','!=',1)
            ->where('is_calculate','!=',1)->get();
        return view('dealer.order.waiting_orders',compact('orders'));
    }
    public function opining_orders(Request $request){
        $orders = Order::where('dealer_id',auth()->user()->id)
            ->where('is_active','!=',1)
            ->where('is_calculate','!=',1)->get();
        return view('dealer.order.opining_orders',compact('orders'));
    }
    public function closed_orders(Request $request){
        $orders = Order::where('dealer_id',auth()->user()->id)->where('is_active','!=',1)
            ->where('is_calculate',1)->get();
        return view('dealer.order.closed_orders',compact('orders'));
    }
//    resources/views/dealer/order/statement.blade.php
}
