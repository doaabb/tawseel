<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use App\Models\ActivityOrder;
use App\Models\Governorate;
use App\Models\Order;
use App\Models\Packages;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use function Sodium\compare;

class OrderController extends Controller
{
    public function __construct(){
//
        $this->middleware(['auth','role:super_admin|admin|manager|store']);
    }

    public function index(){
        $order= Order::where('order_status','=',3)->get();
//        dd($order);
        return view('manager.index',compact('order'));
    }

    public function show(Request $request){
        $packages = Packages::find($request->id);
        $order= Order::find($packages->order_id);
//        dd($order);
        if($order){
            $gov = Governorate::all();
            return view('manager.order', compact('order','gov'));
        }else{
            abort(404);
        }
    }

    function getUserIpAddr(){
        if(!empty($_SERVER['HTTP_CLIENT_IP'])){
            //ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            //ip pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }
    public function store(Order $id){
        if ($id != null) {
            $id->order_status = 4;
            $id->update();
            $activity = ActivityOrder::create([
                'order_id'=>$id->id,
                'user_id'=>auth()->user()->id,
                'ip_address'=>$this->getUserIpAddr(),

                'description'=>'Check Order Status'
            ]);
            return[
                'status_code'=>200,
            ];
        }else{
            return response()->json('no data');
        }
    }
    public function read_barcode(){
        return view('manager.read_barcode');
    }
    public function search_res(){
        return view('manager.search_res');
    }
    public function result_mobile(Request $request){
        $id = User::where('mobile',$request->mobile)->first();
        $order = Order::where('representative_id',$id->id)->pluck('id')->toArray();
        $packages = Packages::whereIn('order_id',$order)
            ->get();

        if ($id != null)
        return ['status'=>200,
            'id'=>$id->id,
            'packages'=>$packages,
            'order'=>$order
        ];
        else
            return ['status'=>404];
  }
    public function result_barcode(Request $request){
        $id = Packages::where('random_number',$request->barcode)->orWhere('code',$request->barcode)->first();

        if ($id != null)
        return ['status'=>200,'id'=>$id->id];
        else
            return ['status'=>404];
  }
    public function result_code(Request $request){
        $id = Packages::where('code',$request->code)->first();

        if ($id != null)
        return ['status'=>200,'id'=>$id->id];
        else
            return ['status'=>404];
  }

    public function result_code_or_barcode(Request $request){
        $id = Packages::where('code',$request->barcode)
            ->orWhere('barcode',$request->barcode)->first();

        if($id) {
            $id->package_status = 0;
            $id->update();

            return ['status' => 200, 'id' => $id->id];
        }else{
            return ['status'=>404];
        }
  }

    public function result_delivery(Request $request){
        if ($request->mobile != null){
            $id = User::where('mobile',$request->mobile)->first();

            $packages = Packages::where('driver_id',$id->id)
                ->get();
          if($id && $id->roles->first()->name === 'representative'){
              $items=[];
              $row=[];
              foreach ($packages as $package){
                  $items[]= $package;
                  $row[]= '<tr>
<td>'.$package->cc.'</td>
<td>'.$package->code.'</td>
<td>'.$package->order->dealer->name.'</td>
<td>'.$package->order->dealer->mobile.'</td>
<td>'.$package->state_shipper->name.'</td>
<td>'.$package->state_receiver->name.'</td>
</tr>';
              }
              return [
                  'id'=>$id->id,
                  'name'=>$id->name,
                  'mobile'=>$id->mobile,
                  'email'=>$id->email,
                  'image_path'=>$id->image_path ?? '../dist/img/user1-128x128.jpg',
                  'state'=>$id->states?$id->states->name:'',
                  'items'=>$items,
                  'row'=>$row,
                  'status'=>201
              ];
          } else{
              return response()->json(['error'=>'
           يرجى التأكد من رقم الجوال
           ']);
          }
//            dd($packages,$items, Packages::where('driver_id',$id->id)->pluck('id','cc','code','pieces')->toArray() );
        }
        if ($request->barcode != null){

            $package = Packages::where('code',$request->barcode)
                ->orWhere('barcode',$request->barcode)->first();
if($package != null){
    $package->driver_id = $request->id;
    $package->update();

    $items=[];
    $row=[];

    $row[]= '<tr>
<td>'.$package->cc.'</td>
<td>'.$package->code.'</td>
<td>'.$package->order->dealer->name.'</td>
<td>'.$package->order->dealer->mobile.'</td>
<td>'.$package->state_shipper->name.'</td>
<td>'.$package->state_receiver->name.'</td>
</tr>';
    return [
        'items'=>$items,
        'row'=>$row,
        'status'=>201
    ];

} else{
    return response()->json(['error'=>'
تحقق من القيمة المدخلة
           ']);
}
        }
    }

    public function package(Request $request){
        $id= Packages::find($request->id);
        $role = Role::where('name','representative')->first();
        $driver = $role->users;
//        dd($order);
        if($id){
            $gov = Governorate::all();
            return view('manager.package', compact('driver','id','gov'));
        }else{
            abort(404);
        }
    }
    public function store_order_manager(Request $request){
        $order = Packages::find($request->id);
        $message = Validator::make($request->all(), [
            'driver_id'=>'required',
        ]);

        if ($message->passes()) {
          $order->delivery_id= $request->driver_id;
          $order->update();
            $activity = ActivityOrder::create([
                'order_id'=>$order->order_id,
                'package_id'=>$order->id,
                'user_id'=>auth()->user()->id,
                'ip_address'=>$this->getUserIpAddr(),

                'description'=>'Sent Order To delivery'

            ]);
//            $order->order_status=3;
//            $order->update();
            return[
                'status_code'=>200,
            ];
        }else{
            return response()->json(['error'=>$message->errors()]);
        }
    }
    public function apply_order(Request $request){

        $order = Order::find($request->id);
        $order->is_active = 0;
        $order->update();
        return redirect()->back();
    }
    public function check_package(Request $request){

        $order = Packages::find($request->id);
        $order->package_status = 0;
        $order->update();
        return[
            'status_code'=>200,
        ];
    }

}
