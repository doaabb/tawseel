<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use App\Models\ActivityOrder;
use App\Models\Order;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;

class ManagerController extends Controller
{
    public function dealer(){
        $role = Role::where('name','dealer')->first();
        $users= $role->users;
        return view('manager.dealer',compact('users'));
    }

    public function representative(){
        $role = Role::where('name','representative')->first();
        $users= $role->users;
        return view('manager.representative',compact('users'));
    }

    public function representative_show(Request $request){
        $role = Role::where('name','representative')->first();
        $users= $role->users->pluck('id');
        $info = User::find($request->id);
        $orders = Order::where('representative_id',$request->id)->get();
        return view('manager.representative_show',compact('orders','info'));
    }
    public function Receipt_from_delivery(){
        return view('manager.receipt_from_delivery');
    }

    public function search_with_code(){
        return view('manager.search_with_code');
    }

    function getUserIpAddr(){
        if(!empty($_SERVER['HTTP_CLIENT_IP'])){
            //ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            //ip pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    public function sent_alert_to_res(Request $request){
        $order = Order::find($request->id);
        $order->order_status = 4;

        $activity = ActivityOrder::create([
            'order_id'=>$order->id,
            'user_id'=>auth()->user()->id,
            'ip_address'=>$this->getUserIpAddr(),
            'description'=>'Sent Alert To respresentative'

        ]);

        return redirect()->back();
    }
}
