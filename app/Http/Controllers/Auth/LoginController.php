<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\AuthuserUser;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\ValidationException;
use function Sodium\randombytes_random16;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

//        dd(Session::get('_token'));

        $this->middleware('guest')->except('logout');
    }

    protected function validateLogin(Request $request)
    {
        $request->validate([
            $this->username() => 'required|string',
            'password' => 'required|string',
        ]);

    }
    protected function validateLogin_number(Request $request)
    {
        $request->validate([
            'mobile' => 'required|string',
            'password' => 'required|string',
        ]);

    }

    public function login(Request $request)
    {
        $this->validateLogin($request);
//        dd($this->is_found($request->email));
        if($this->is_found($request->email)){
            $this->is_transfer($this->is_found($request->email), $request);
        }

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if (method_exists($this, 'hasTooManyLoginAttempts') &&
            $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    public function login_with_number_view(){
        return view('auth.login_with_number');
    }
    public function login_with_number(Request $request)
    {    // Check validation
//        dd($request->mobile);
        $this->validate($request, [
            'mobile' => 'required|regex:/[0-9]{10}/|digits:10',
        ]);

        // Get user record
        $user = User::where('mobile', $request->get('mobile'))->first();

        // Check Condition Mobile No. Found or Not
        if($request->get('mobile') != $user->mobile) {
            \Session::put('errors', 'Your mobile number not match in our system..!!');
            return back();
        }


        // Set Auth Details
        \Auth::login($user);

        // Redirect home page
        return redirect()->route('home');
    }

    // ليعيد الصلاحية
    public function get_type($id){
        if ($id = 1) {
            return 'admin';
        }elseif($id = 2){
            return 'super_admin';
        }elseif($id = 3){
            return 'agent';
        }elseif($id = 4){
            return 'dealer';
        }elseif($id = 5){
            return 'test';
        }else{
            return false;
        }
    }
    // ازا كان المستخدم موجود مسبقاً او لا
    protected function is_found($id){
        $user = AuthuserUser::where('email',$id)->first();
        if($user) return $user->id;
        else   return false;
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $request->session()->invalidate();

        return $this->loggedOut($request) ?: redirect('/');
    }
    public function is_transfer( $id,Request $request){
        $id=AuthuserUser::find($id);
        $status = $id->is_activate($id->is_active);
//        dd($id->type_id);
        if ($status){
            return true;
        }else{
            $Pass=mt_rand(100000,999999).substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZ"),0,1).substr(str_shuffle("abcdefghijklmnopqrstuvwxyz"),0,1);

            $random = str_shuffle($Pass);
//            $new_user = User::create([
//                'old_user_id'=>$id->id,
//                'name'=>$id->name,
//                'email'=>$id->email,
//                'mobile'=>$id->mobile,
//                'phone'=>$id->phone,
//                'state_id'=>$id->state_id,
//                'password'=>Hash::make($random),
//                'code_activate'=>$random,
//                'governorate_id'=>$id->governorate_id,
//            ]);
////            dd($this->get_type($id->type_id));
//            $new_user->attachRole($this->get_type($id->type_id));
//            Mail::to($request->mail);
            $data = ['password' => $random,'email'=>$id->email];

            $emails=$id->email;
            Mail::send('emails.welcome', $data , function ($message) use ($emails){
                $message->to([$emails])->cc('doaaabbar6@gmail.com');
                $message->from('info@waselleeom.com', 'info wasellee om')
                    ->subject
                    ('Reset Waselleeom Password');
            });
            return  route('password.reset',Session::get('_token'));
        }
    }
    public function redirectTo()
    {
        $user = auth()->user();

        if($user->hasRole('super_admin|admin')){
            return 'dashboard/home';
        }elseif($user->hasRole('dealer')){
            return 'dealer/create_order';
        }elseif($user->hasRole('representative')){
            return 'representative/home';
        }elseif($user->hasRole('data_entry')){
            return 'date-entry/home';
        }elseif($user->hasRole('store|manager')){
            return 'manager/home';
        }elseif($user->hasRole('accountant')){
            return 'accountant/home';
        }elseif($user->hasRole('user')){
            return '/home';
        }else{
            return '/';
        }
    }

}
