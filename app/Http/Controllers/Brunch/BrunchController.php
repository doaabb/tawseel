<?php

namespace App\Http\Controllers\Brunch;

use App\Http\Controllers\Controller;
use App\Models\ActivityOrder;
use App\Models\Order;
use Illuminate\Http\Request;

class BrunchController extends Controller
{
    public function index(){
        $order =Order::all();
        return view('brunch.index',compact('order'));
    }
    public function order(Request $request){
        $order =Order::find($request->id);
        return view('brunch.order',compact('order'));
    }

    function getUserIpAddr(){
        if(!empty($_SERVER['HTTP_CLIENT_IP'])){
            //ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            //ip pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    public function transfer(Request $request){
        $order = Order::find($request->id);

        $activity = ActivityOrder::create([
            'order_id'=>$order->id,
            'user_id'=>auth()->user()->id,
            'ip_address'=>$this->getUserIpAddr(),
            'description'=>'Sent order To '.$request->state.' Brunch'

        ]);

    }
}
