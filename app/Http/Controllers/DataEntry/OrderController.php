<?php

namespace App\Http\Controllers\DataEntry;

use App\Http\Controllers\Controller;
use App\Models\ActivityOrder;
use App\Models\Governorate;
use App\Models\Media;
use App\Models\Order;
use App\Models\Packages;
use App\Models\Role;
//use Barryvdh\DomPDF\Facade as PDF;
use Dompdf\Dompdf;
use Illuminate\Support\Facades\App;
use PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Milon\Barcode\DNS1D;

class OrderController extends Controller
{
    public function __construct()
    {
//        ini_set('max_execution_time', 300); //300 seconds = 5 minutes
//        ini_set('memory_limit', '-1');

    }

    public function index(){

        $gov = Governorate::all();
        $order= Order::where('order_status','>=',2)->get();
        $role = Role::where('name','delivery')->first();
        $role2 = Role::where('name','representative')->first();
        $delivery = $role->users;
        $driver = $role2->users;

        return view('data_entry.index',compact('order','gov','delivery','driver'));
    }

    public function all_order(){

        $gov = Governorate::all();
        $order= Order::where('order_status','>=',2)->get();
        $role = Role::where('name','delivery')->first();
        $role2 = Role::where('name','representative')->first();
        $delivery = $role->users;
        $driver = $role2->users;

        return view('data_entry.all_order',compact('order','gov','delivery','driver'));
    }

    public function alerts(){

        $gov = Governorate::all();
        $media = Media::where('alert_entry',1)->pluck('model_id');
//        dd($media->order);
        $order= Order::where('order_status','>=',2)->whereIn('id',$media)
            ->get();
        $role = Role::where('name','delivery')->first();
        $role2 = Role::where('name','representative')->first();
        $delivery = $role->users;
        $driver = $role2->users;

        return view('data_entry.alert_order',compact('order','gov','delivery','driver'));
    }

    public function insert(Order $id){
        if($id){

            $gov = Governorate::all();
            return view('data_entry.enable', compact('id','gov'));
        }else{
           abort(404);
        }
    }

    public function print(){
        $id = Packages::first();
        if($id){

            $gov = Governorate::all();
            return view('data_entry.print', compact('id','gov'));
        }else{
           abort(404);
        }
    }
    public function download_pdf(Request $request){
//        $dompdf = new Dompdf();
        $order=  $id =  $order = Order::get();
        $html = '';
        foreach($order as $id)
        {

            $order=$id;
            $view = view('data_entry.barcode.print_pdf')
                ->with(compact('order', 'id'));
            $html .= $view->render();
        }
        $pdf = PDF::loadHTML($html);
        $sheet = $pdf->setPaper('a5', 'landscape');
        return $sheet->download('download.pdf');




    }
    public function store(Request $request){
        $order = Order::find($request->id);
        $message = Validator::make($request->all(), [
        'total_price'=>'required',
        'pieces'=>'required',
        ]);

        if ($message->passes()) {
//            $number=mt_rand(1000,9999);
//            $d = new DNS1D();
//            $barcode= Storage::put('uploads/'.$number.$request->image_id.$order->id.'.png',
//                    base64_decode($d->getBarcodePNG("2021".$number, "C39+")));

            $insert = Packages::updateOrCreate([
                'media_id'=>$request->image_id,
                ],[
                'user_id'=>auth()->user()->id,
                'order_id'=>$request->id,
                'total_price'=>$request->total_price,
                'pieces'=>$request->pieces,
                'driver_id'=>$request->driver_id,
                'picket_by'=>$request->picket_by,
                'pickup_date'=>$request->pickup_date,
                'weight'=>$request->weight,
                'state_id_shipper'=>$request->state_id_shipper,
                'tel_shipper'=>$request->tel_shipper,
                'state_id_receiver'=>$request->state_id_receiver,
                'tel_receiver'=>$request->tel_receiver,
                'cc'=>$request->cc,
                'code'=>$request->code,
//                'barcode'=>$barcode,
                'random_number'=>'2021'.mt_rand(1000,9999),
//                'path_barcode'=>'/storage/'.$number.$request->image_id.$order->id.'.png'
            ]);
            $media = Media::find($insert->media_id);
            $media->alert_entry = null;
            $media->update();
            if(count($order->media) == count($order->packages)){
                ActivityOrder::create([
                    'order_id'=>$order->id,
                    'package_id'=>$insert->id,
                    'user_id'=>auth()->user()->id,
                    'ip_address'=>$this->getUserIpAddr(),
                    'description'=>'Insert Package Number '.$insert->code .' In Data Base'.' And all Packages has been inserted'

                ]);
                $order->order_status=3;
                $order->update();
            }else{
                ActivityOrder::create([
                    'order_id'=>$order->id,
                    'package_id'=>$insert->id,
                    'user_id'=>auth()->user()->id,
                    'ip_address'=>$this->getUserIpAddr(),

                    'description'=>'Insert Package Number '.$insert->code .' In Data Base'

                ]);

            }
            return[
                'status_code'=>200,
                'image_id'=>$request->image_id
            ];
        }else{
            return response()->json(['error'=>$message->errors()]);
        }
    }

    function getUserIpAddr(){
        if(!empty($_SERVER['HTTP_CLIENT_IP'])){
            //ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            //ip pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }
    public function setting_print(Request $request){

        $order = Order::find($request->id);
        return view('data_entry.setting_print',compact('order'));
    }

    public function save_print(Request $request){

        $order = Order::find($request->id);
        $count = $request->number;
        $number=mt_rand(1000,9999);
        while ($count > 0){
            $d = new DNS1D();
            $name = time().$number;
            $barcode= Storage::disk('public')
                ->put($name.'.png',
                    base64_decode($d->getBarcodePNG("2021".$number, "C39+")));
//            $number = $request->number;
            $store = Packages::create([

                'user_id'=>auth()->user()->id,
                'order_id'=>$request->id,
                'barcode'=>$name,
                'random_number'=>'2021'.$number,
                'path_barcode'=>'/storage/'.$name.'.png'
            ]);

            $activity = ActivityOrder::create([
                'order_id'=>$order->id,
                'user_id'=>auth()->user()->id,
                'ip_address'=>$this->getUserIpAddr(),

                'description'=>'Data entry Print pages'

            ]);

            $count--;
            $number++;
        }
        return [
            'status_code'=>200
        ];
    }
    public function print_pages(Request $request){
      $id =  $order = Packages::where('order_id',$request->id)->get();
        return view('data_entry.print',compact('order','id'));
    }
}
