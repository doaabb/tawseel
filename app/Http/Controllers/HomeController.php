<?php

namespace App\Http\Controllers;

use App\Models\Governorate;
use App\Models\State;
use App\Models\Zone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        dd(Auth::user());


//        dd(\auth());
//        $user = \Auth::user();
//        if($user){
//            if ($user->hasRole('super_admin')||$user->hasRole('admin')) {
//            }else{
//                redirect('/');
//            }
//        }else{
//            abort('404');
//        }
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
    public function profile(){

        $state = State::all();
        $gov = Governorate::all();
        $zone = Zone::all();
        return view('all_users.profile',compact('state','gov'));
    }
    public function update_profile(Request $request){
        $user = \auth()->user();
        $message = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
//            'email' => 'required|email|unique:users,email,'.$user->id.',id' ,
            'mobile' => ['required', 'unique:users,mobile,'.$user->id.',id'],
            'whatsapp' => ['required', 'unique:users,whatsapp,'.$user->id.',id'],
//            'whatsapp' => ['unique:users'],
//            'password' => ['required', 'string', 'min:8'],
        ]);
        /*
         *
        'email' => 'required|email|unique:users,email,'.$user->id.',id'
         */
        if ($message->passes()) {
            $imageName = null;
            $path = null;
            if ($request->file('file')) {
                $imagePath = $request->file('file');
                $imageName = time() . $imagePath->getClientOriginalName();
                $path = $request->file('file')->storeAs('uploads', $imageName, 'public');
            }
            $user->name = $request->name;
            $user->mobile = $request->mobile;
            $user->phone = $request->phone;
            $user->whatsapp = $request->whatsapp;
            $user->facebook = $request->facebook;
            $user->instagram = $request->instagram;
            $user->address = $request->address;
            $user->governorate_id = $request->governorate_id;
            $user->state_id = $request->state_id;
            $user->image_name = $request->file('file') ? $imageName : \auth()->user()->image_name;
            $user->image_path = $request->file('file') ? $path : \auth()->user()->image_path;
            $user->update();
        }
//            dd($user,response()->json(['error'=>$message->errors()]));
        return redirect()->back();
        /*
         *
                'image_name'        =>  $imageName,
                ''        =>  ?? null,
            $table->string('')->nullable();
            $table->string('')->nullable();
            $table->string('')->nullable();
            $table->string('')->nullable();
            $table->string('image_name')->nullable();
            $table->string('image_path')->nullable();
            $table->string('')->nullable();// الولاية
            $table->string('state')->nullable();// الولاية
            $table->string('governorate')->nullable();// المحافظة
            $table->string('note')->nullable();// ملاحظة
            $table->integer('')->nullable();
            $table->integer('')->nullable();
            $table->integer('zone_id')->nullable();
         */
    }
}
