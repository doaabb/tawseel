<?php

use Illuminate\Support\Facades\Route;

// Admin Routes
//Route::group(['middleware' => 'role:admin'],function(){
    Route::get('/home',['App\Http\Controllers\Admin\HomeController','index'])->name('dashboard');
//});
/* Role */
Route::get('role',[\App\Http\Controllers\Admin\RolesController::class,'index'])->name('role');
Route::get('res_role',[\App\Http\Controllers\Admin\RolesController::class,'res_role'])->name('res_role');
Route::get('acc_role',[\App\Http\Controllers\Admin\RolesController::class,'acc_role'])->name('acc_role');
Route::get('create_role',[\App\Http\Controllers\Admin\RolesController::class,'create'])->name('create_role');
Route::post('store_role',[\App\Http\Controllers\Admin\RolesController::class,'store'])->name('store_role');
Route::post('store_res',[\App\Http\Controllers\Admin\RolesController::class,'store_res'])->name('store_res');
Route::get('edit_role/{id}',[\App\Http\Controllers\Admin\RolesController::class,'edit_role'])->name('edit_role');
Route::post('update_role',[\App\Http\Controllers\Admin\RolesController::class,'update_role'])->name('update_role');
Route::post('sdelete_role',[\App\Http\Controllers\Admin\RolesController::class,'sdelete_role'])->name('sdelete_role');

/* User */
Route::get('users',[\App\Http\Controllers\Admin\UserController::class,'index'])->name('users');
Route::get('create_user',[\App\Http\Controllers\Admin\UserController::class,'create'])->name('create_user');
Route::post('store_user',[\App\Http\Controllers\Admin\UserController::class,'store'])->name('store_user');
Route::get('edit_user/{id}',[\App\Http\Controllers\Admin\UserController::class,'edit'])->name('edit_user');
Route::post('update_user',[\App\Http\Controllers\Admin\UserController::class,'update'])->name('update_user');
Route::post('sdelete_user',[\App\Http\Controllers\Admin\UserController::class,'sdelete'])->name('sdelete_user');

/* Governorate */
Route::get('gov',[\App\Http\Controllers\Admin\GovernorateController::class,'index'])->name('gov');
Route::get('create_gov',[\App\Http\Controllers\Admin\GovernorateController::class,'create'])->name('create_gov');
Route::post('store_gov',[\App\Http\Controllers\Admin\GovernorateController::class,'store'])->name('store_gov');
Route::get('edit_gov/{id}',[\App\Http\Controllers\Admin\GovernorateController::class,'edit'])->name('edit_gov');
Route::post('update_gov',[\App\Http\Controllers\Admin\GovernorateController::class,'update'])->name('update_gov');
Route::post('sdelete_gov',[\App\Http\Controllers\Admin\GovernorateController::class,'sdelete'])->name('sdelete_gov');
Route::post('select_store',[\App\Http\Controllers\Admin\GovernorateController::class,'select_store'])->name('select_store');

/* State */
Route::get('state',[\App\Http\Controllers\Admin\StateController::class,'index'])->name('state');
Route::get('create_state',[\App\Http\Controllers\Admin\StateController::class,'create'])->name('create_state');
Route::post('store_state',[\App\Http\Controllers\Admin\StateController::class,'store'])->name('store_state');
Route::get('edit_state/{id}',[\App\Http\Controllers\Admin\StateController::class,'edit'])->name('edit_state');
Route::post('update_state',[\App\Http\Controllers\Admin\StateController::class,'update'])->name('update_state');
Route::post('sdelete_state',[\App\Http\Controllers\Admin\StateController::class,'sdelete'])->name('sdelete_state');

/* Zone */
Route::get('zone',[\App\Http\Controllers\Admin\zoneController::class,'index'])->name('zone');
Route::get('create_zone',[\App\Http\Controllers\Admin\zoneController::class,'create'])->name('create_zone');
Route::post('store_zone',[\App\Http\Controllers\Admin\zoneController::class,'store'])->name('store_zone');
Route::get('edit_zone/{id}',[\App\Http\Controllers\Admin\zoneController::class,'edit'])->name('edit_zone');
Route::post('update_zone',[\App\Http\Controllers\Admin\zoneController::class,'update'])->name('update_zone');
Route::post('sdelete_zone',[\App\Http\Controllers\Admin\zoneController::class,'sdelete'])->name('sdelete_zone');


/* Order */
Route::get('read_barcode',[\App\Http\Controllers\Admin\OrderController::class,'read_barcode'])->name('admin_read_barcode');
Route::get('orders',[\App\Http\Controllers\Admin\OrderController::class,'index'])->name('admin_orders');
Route::get('order/{id}',[\App\Http\Controllers\Admin\OrderController::class,'show'])->name('admin_order');
Route::get('edit_order_admin/{id}',[\App\Http\Controllers\Admin\OrderController::class,'edit_order'])->name('edite_order_admin');
Route::post('update_order',[\App\Http\Controllers\Admin\OrderController::class,'update_order'])->name('update_order_admin');
Route::get('orders_old',[\App\Http\Controllers\Admin\OrderController::class,'orders_old'])->name('orders_old');
Route::get('to_representative/{id}',[\App\Http\Controllers\Admin\OrderController::class,'to_representative'])->name('to_representative');
Route::get('to_data_entry/{id}',[\App\Http\Controllers\Admin\OrderController::class,'to_data_entry'])->name('to_data_entry');
Route::post('sent_to_representative',[\App\Http\Controllers\Admin\OrderController::class,'sent_to_representative'])->name('sent_to_representative');
Route::post('sent_to_data_entry',[\App\Http\Controllers\Admin\OrderController::class,'sent_to_data_entry'])->name('sent_to_data_entry');
Route::get('create_order',[\App\Http\Controllers\Admin\OrderController::class,'create'])->name('admin_create_order');
Route::post('store_order',[\App\Http\Controllers\Admin\OrderController::class,'store'])->name('admin_store_order');
Route::get('edit_order/{id}',[\App\Http\Controllers\Admin\OrderController::class,'order_type_store'])->name('admin_edit_order');
Route::post('update_order',[\App\Http\Controllers\Admin\OrderController::class,'update'])->name('admin_update_order');
Route::post('sdelete_order',[\App\Http\Controllers\Admin\OrderController::class,'sdelete'])->name('admin_sdelete_order');
Route::post('dis_active',[\App\Http\Controllers\Admin\OrderController::class,'dis_active'])->name('dis_active');
/* Order Type */
Route::get('order_type',[\App\Http\Controllers\Admin\OrderController::class,'order_type'])->name('order_type');
Route::post('order_type_store',[\App\Http\Controllers\Admin\OrderController::class,'order_type_store'])->name('order_type_store');
Route::post('sdelete_order_type',[\App\Http\Controllers\Admin\OrderController::class,'sdelete_order_type'])->name('sdelete_order_type');
Route::post('order_cancel',[\App\Http\Controllers\Admin\OrderController::class,'order_cancel'])->name('order_cancel');
Route::post('order_deliver',[\App\Http\Controllers\Admin\OrderController::class,'order_deliver'])->name('order_deliver');

/*Prices */
Route::get('prices',[\App\Http\Controllers\Admin\PricesController::class,'index'])->name('prices');
Route::post('price_store',[\App\Http\Controllers\Admin\PricesController::class,'store'])->name('price_store');
Route::post('sdelete_price',[\App\Http\Controllers\Admin\PricesController::class,'sdelete'])->name('sdelete_price');
//Route::post('price_cancel',[\App\Http\Controllers\Admin\PricesController::class,'price_cancel'])->name('price_cancel');
