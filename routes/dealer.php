<?php

use Illuminate\Support\Facades\Route;

// Admin Routes
//Route::group(['middleware' => 'role:admin'],function(){
Route::get('/home',['\App\Http\Controllers\Dealer\OrderController','index'])->name('dashboard-dealer');
//});
/* Order */
Route::get('orders',[\App\Http\Controllers\Dealer\OrderController::class,'create'])->name('orders');
Route::get('create_order',[\App\Http\Controllers\Dealer\OrderController::class,'create'])->name('create_order');
Route::post('store_order',[\App\Http\Controllers\Dealer\OrderController::class,'store'])->name('store_order');
Route::get('edit_order/{id}',[\App\Http\Controllers\Dealer\OrderController::class,'edit'])->name('edit_order');
Route::post('update_order',[\App\Http\Controllers\Dealer\OrderController::class,'update'])->name('update_order');
Route::post('sdelete_order',[\App\Http\Controllers\Dealer\OrderController::class,'sdelete'])->name('sdelete_order');

Route::post('get_zone',[\App\Http\Controllers\Dealer\OrderController::class,'get_zone'])->name('get_zone');
Route::post('get_status',[\App\Http\Controllers\Dealer\OrderController::class,'get_status'])->name('get_status');
Route::get('statement',[\App\Http\Controllers\Dealer\OrderController::class,'account_dealer'])->name('statement_dealer');
Route::get('deliver_order',[\App\Http\Controllers\Dealer\OrderController::class,'deliver_order'])->name('deliver_order');
Route::get('cancel_orders',[\App\Http\Controllers\Dealer\OrderController::class,'cancel_orders'])->name('cancel_orders');
Route::get('waiting_orders',[\App\Http\Controllers\Dealer\OrderController::class,'waiting_orders'])->name('waiting_orders');
