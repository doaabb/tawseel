<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/register', function() {
    return redirect('/login');
});

Route::post('/register', function() {
    return redirect('/login');
});
Route::get('/login_with_number_view', [App\Http\Controllers\Auth\LoginController::class, 'login_with_number_view'])->name('login_with_number_view');
Route::get('/login_with_number', [App\Http\Controllers\Auth\LoginController::class, 'login_with_number'])->name('login_with_number');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/profile',[\App\Http\Controllers\HomeController::class,'profile'])->name('profile');
Route::post('/update_profile',[\App\Http\Controllers\HomeController::class,'update_profile'])->name('update_profile');
