<?php

use Illuminate\Support\Facades\Route;

// Admin Routes
//Route::group(['middleware' => 'role:admin'],function(){
Route::get('/home',[App\Http\Controllers\Manager\OrderController::class,'index'])->name('dashboard-manager');
Route::get('/dealer',[App\Http\Controllers\Manager\ManagerController::class,'dealer'])->name('dealer');
//});
/* Order */
Route::get('order/{id}',[\App\Http\Controllers\Manager\OrderController::class,'show'])->name('order_manager');
Route::get('package/{id}',[\App\Http\Controllers\Manager\OrderController::class,'package'])->name('package_manager');
//Route::get('orders_rep',[\App\Http\Controllers\Representative\OrderController::class,'index'])->name('orders_rep');
//Route::get('enable_order/{id}',[\App\Http\Controllers\Representative\OrderController::class,'enable'])->name('enable_order');
Route::post('store_order_manager',[App\Http\Controllers\Manager\OrderController::class,'store_order_manager'])->name('store_order_manager');
Route::post('active_order',[App\Http\Controllers\Manager\OrderController::class,'store'])->name('result_mobile');
Route::get('read_barcode',[App\Http\Controllers\Manager\OrderController::class,'result_mobile'])->name('read_barcode');
Route::get('search_res',[App\Http\Controllers\Manager\OrderController::class,'search_res'])->name('search_res');
Route::post('result_mobile',[App\Http\Controllers\Manager\OrderController::class,'result_mobile'])->name('result_mobile');
Route::post('result_barcode',[App\Http\Controllers\Manager\OrderController::class,'result_barcode'])->name('result_barcode');
Route::post('result_code',[App\Http\Controllers\Manager\OrderController::class,'result_code'])->name('result_code');
Route::get('representative_manager',[\App\Http\Controllers\Manager\ManagerController::class,'representative'])->name('representative_manager');
Route::get('representative_show/{id}',[\App\Http\Controllers\Manager\ManagerController::class,'representative_show'])->name('representative_show');
Route::post('representative_search',[\App\Http\Controllers\Manager\ManagerController::class,'representative_search'])->name('representative_search');
Route::post('check_package',[\App\Http\Controllers\Manager\OrderController::class,'check_package'])->name('check_package');
Route::post('result_code_or_barcode',[\App\Http\Controllers\Manager\OrderController::class,'result_code_or_barcode'])->name('result_code_or_barcode');
Route::post('result_delivery',[\App\Http\Controllers\Manager\OrderController::class,'result_delivery'])->name('result_delivery');


Route::get('Receipt_from_delivery',[App\Http\Controllers\Manager\ManagerController::class,'Receipt_from_delivery'])->name('Receipt_from_delivery');
Route::get('search_with_code',[App\Http\Controllers\Manager\ManagerController::class,'search_with_code'])->name('search_with_code');
Route::get('sent_alert_to_res/{id}',[App\Http\Controllers\Manager\ManagerController::class,'sent_alert_to_res'])->name('sent_alert_to_res');
Route::get('apply_order/{id}',[App\Http\Controllers\Manager\OrderController::class,'apply_order'])->name('apply_order');
//Route::post('result_barcode',[App\Http\Controllers\Manager\OrderController::class,'result_barcode'])->name('result_barcode');
