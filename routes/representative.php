<?php

use Illuminate\Support\Facades\Route;

// Admin Routes
//Route::group(['middleware' => 'role:admin'],function(){
Route::get('/home',['\App\Http\Controllers\Representative\OrderController','index'])->name('dashboard-representative');
//});
/* Order */
Route::get('orders_rep',[\App\Http\Controllers\Representative\OrderController::class,'index'])->name('orders_rep');
Route::get('order_alert',[\App\Http\Controllers\Representative\OrderController::class,'order_alert'])->name('order_alert');
Route::get('delivery',[\App\Http\Controllers\Representative\OrderController::class,'delivery'])->name('delivery');
Route::get('my_order',[\App\Http\Controllers\Representative\OrderController::class,'my_order'])->name('my_order');
Route::get('packages_rep',[\App\Http\Controllers\Representative\OrderController::class,'packages_rep'])->name('packages_rep');
Route::get('enable_order/{id}',[\App\Http\Controllers\Representative\OrderController::class,'enable'])->name('enable_order');
Route::post('take_order',[\App\Http\Controllers\Representative\OrderController::class,'take_order'])->name('take_order');
Route::post('store_enable',[\App\Http\Controllers\Representative\OrderController::class,'store_enable'])->name('store_enable');
Route::post('store_order_rep',[\App\Http\Controllers\Representative\OrderController::class,'store_order_rep'])->name('store_order_rep');
Route::post('done_deliver',[\App\Http\Controllers\Representative\OrderController::class,'done_deliver'])->name('done_deliver');
Route::post('order_cancel_rep',[\App\Http\Controllers\Representative\OrderController::class,'order_cancel_rep'])->name('order_cancel_rep');
Route::post('update_img_res',[\App\Http\Controllers\Representative\OrderController::class,'update_img_res'])->name('update_img_res');
Route::post('sdelete_media',[\App\Http\Controllers\Representative\OrderController::class,'sdelete'])->name('sdelete_media');
Route::post('is_active',[\App\Http\Controllers\Representative\OrderController::class,'is_active'])->name('is_active');
