<?php

use Illuminate\Support\Facades\Route;

// Admin Routes
//Route::group(['middleware' => 'role:admin'],function(){
Route::get('/home',[App\Http\Controllers\Account\AccountantController::class,'index'])->name('dashboard-account');
Route::get('/dealer',[App\Http\Controllers\Account\AccountantController::class,'dealer_account'])->name('dealer_account');
Route::get('/account_dealer/{id}',[App\Http\Controllers\Account\AccountantController::class,'account_dealer'])->name('account_dealer');
Route::get('/delivery',[App\Http\Controllers\Account\AccountantController::class,'delivery_account'])->name('delivery_account');
Route::get('/all_closed_account',[App\Http\Controllers\Account\AccountantController::class,'all_closed_account'])->name('all_closed_account');
Route::get('/alert_orders',[App\Http\Controllers\Account\AccountantController::class,'alert_orders'])->name('alert_orders');
Route::get('/closed_account/{id}',[App\Http\Controllers\Account\AccountantController::class,'closed_account'])->name('closed_account');
Route::get('/accountant_delivery/{id}',[App\Http\Controllers\Account\AccountantController::class,'accountant_delivery'])->name('accountant_delivery');


//});
