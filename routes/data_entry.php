<?php

use Illuminate\Support\Facades\Route;

// Admin Routes
//Route::group(['middleware' => 'role:admin'],function(){
Route::get('/home',[App\Http\Controllers\DataEntry\OrderController::class,'index'])->name('dashboard-entry');
Route::get('/all_order',[App\Http\Controllers\DataEntry\OrderController::class,'all_order'])->name('all_order');
Route::get('/alerts-entry',[App\Http\Controllers\DataEntry\OrderController::class,'alerts'])->name('alerts-entry');
//});
/* Order */
Route::get('order/{id}',[\App\Http\Controllers\DataEntry\OrderController::class,'insert'])->name('data_entry_order');
Route::get('setting_print/{id}',[\App\Http\Controllers\DataEntry\OrderController::class,'setting_print'])->name('setting_print');
Route::get('print/{id}',[\App\Http\Controllers\DataEntry\OrderController::class,'print_pages'])->name('print');
Route::post('save_print/{id}',[\App\Http\Controllers\DataEntry\OrderController::class,'save_print'])->name('save_print');
Route::get('print_pdf/{id}',[\App\Http\Controllers\DataEntry\OrderController::class,'download_pdf'])->name('download_pdf');
//Route::get('orders_rep',[\App\Http\Controllers\Representative\OrderController::class,'index'])->name('orders_rep');
//Route::get('enable_order/{id}',[\App\Http\Controllers\Representative\OrderController::class,'enable'])->name('enable_order');
Route::post('store_order_entry',[App\Http\Controllers\DataEntry\OrderController::class,'store'])->name('store_order_entry');
