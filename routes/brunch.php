<?php

use Illuminate\Support\Facades\Route;

// Admin Routes
//Route::group(['middleware' => 'role:admin'],function(){
Route::get('/dashboard-brunch',[App\Http\Controllers\Brunch\BrunchController::class,'index'])->name('dashboard-brunch');
//});
/* Order */
Route::get('order/{id}',[\App\Http\Controllers\Brunch\BrunchController::class,'order'])->name('brunch_order');
Route::post('transfer',[\App\Http\Controllers\Brunch\BrunchController::class,'transfer'])->name('transfer_order');
