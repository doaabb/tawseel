<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $user = User::create([
            'name' => 'super admin',
            'email' => 'super_admin@app.com',
            'password' => Hash::make('12345678'),
        ]);
        $user->attachRole('super_admin');

        $user = User::create([
            'name' => 'dealer',
            'email' => 'dealer@app.com',
            'password' => Hash::make('12345678'),
        ]);
        $user->attachRole('dealer');

        $user = User::create([
            'name' => 'representative',
            'email' => 'representative@app.com',
            'password' => Hash::make('12345678'),
        ]);
        $user->attachRole('representative');

        $user = User::create([
            'name' => 'Data Entry',
            'email' => 'data_entry@app.com',
            'password' => Hash::make('12345678'),
        ]);
        $user->attachRole('data_entry');

        $user = User::create([
            'name' => 'agent',
            'email' => 'agent@app.com',
            'password' => Hash::make('12345678'),
        ]);
        $user->attachRole('agent');

        $user = User::create([
            'name' => 'operation',
            'email' => 'operation@app.com',
            'password' => Hash::make('12345678'),
        ]);
        $user->attachRole('operation');

        $user = User::create([
            'name' => 'manager',
            'email' => 'manager@app.com',
            'password' => Hash::make('12345678'),
        ]);
        $user->attachRole('manager');

        $user = User::create([
            'name' => 'store',
            'email' => 'store@app.com',
            'password' => Hash::make('12345678'),
        ]);
        $user->attachRole('store');

        $user = User::create([
            'name' => 'brunch',
            'email' => 'brunch@app.com',
            'password' => Hash::make('12345678'),
        ]);
        $user->attachRole('brunch');
        $user = User::create([
            'name' => 'accountant',
            'email' => 'accountant@app.com',
            'password' => Hash::make('12345678'),
        ]);
        $user->attachRole('accountant');

    }
}
