<?php

namespace Database\Seeders;

use App\Models\Governorate;
use Illuminate\Database\Seeder;

class GovernorateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $gov = Governorate::create([
            'id'=>1,
            'name'=>'مسقط'
        ]);
        $gov = Governorate::create([
            'id'=>2,
            'name'=>'ظفار'
        ]);
        $gov = Governorate::create([
            'id'=>3,
            'name'=>'مسندم'
        ]);
        $gov = Governorate::create([
            'id'=>4,
            'name'=>'البريمي'
        ]);
        $gov = Governorate::create([
            'id'=>5,
            'name'=>'الداخلية'
        ]);
        $gov = Governorate::create([
            'id'=>7,
            'name'=>'شمال الباطنة'
        ]);
        $gov = Governorate::create([
            'id'=>8,
            'name'=>'جنوب الباطنة'
        ]);
        $gov = Governorate::create([
            'id'=>9,
            'name'=>'شمال الشرقية'
        ]);
        $gov = Governorate::create([
            'id'=>10,
            'name'=>'جنوب الشرقية'
        ]);
        $gov = Governorate::create([
            'id'=>11,
            'name'=>'الظاهرة'
        ]);
        $gov = Governorate::create([
            'id'=>12,
            'name'=>'الوسطى'
        ]);
    }
}
