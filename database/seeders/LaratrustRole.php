<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class LaratrustRole extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $superAdmin = Role::create([
            'name' => 'super_admin',
            'display_name' => 'Super Admin',
            'description' => 'Can do anything in the project',
        ]);
        // admin role
        $user = Role::create([
            'name' => 'admin',
            'display_name' => 'Admin',
            'description' => 'Can do specific tasks ',
        ]);
        // operation role
        $user = Role::create([
            'name' => 'operation',
            'display_name' => 'Operation',
            'description' => 'Can do specific tasks ',
        ]);
        // user role
        $user = Role::create([
            'name' => 'user',
            'display_name' => 'User',
            'description' => 'Can do specific tasks ',
        ]);
        // Dealer role
        $user = Role::create([
            'name' => 'dealer',
            'display_name' => 'Dealer',
            'description' => 'Can do specific tasks ',
        ]);
        // representative role
        $user = Role::create([
            'name' => 'representative',
            'display_name' => 'Representative',
            'description' => 'Can do specific tasks ',
        ]);
        // Accountant role
        $user = Role::create([
            'name' => 'accountant',
            'display_name' => 'Accountant',
            'description' => 'Can do specific tasks ',
        ]);
        // Data Entry role
        $user = Role::create([
            'name' => 'data_entry',
            'display_name' => 'Data Entry',
            'description' => 'Can do specific tasks ',
        ]);
        // Agent Entry role
        $user = Role::create([
            'name' => 'agent',
            'display_name' => 'Agent',
            'description' => 'Can do specific tasks ',
        ]);
        // Manager Entry role
        $user = Role::create([
            'name' => 'manager',
            'display_name' => 'Manager',
            'description' => 'Can do specific tasks ',
        ]);
        // delivery Entry role
        $user = Role::create([
            'name' => 'delivery',
            'display_name' => 'Delivery',
            'description' => 'Can do specific tasks ',
        ]);
        // driver Entry role
        $user = Role::create([
            'name' => 'driver',
            'display_name' => 'Driver',
            'description' => 'Can do specific tasks ',
        ]);

        // driver Entry role
        $user = Role::create([
            'name' => 'store',
            'display_name' => 'Store',
            'description' => 'Can do specific tasks ',
        ]);

        // driver Entry role
        $user = Role::create([
            'name' => 'brunch',
            'display_name' => 'Brunch',
            'description' => 'Can do specific tasks ',
        ]);

        // Accountant role
        $user = Role::create([
            'name' => 'accountant_dealer',
            'display_name' => 'Accountant Dealer',
            'description' => 'Can do specific tasks ',
        ]);

        // Accountant role
        $user = Role::create([
            'name' => 'accountant_delivery',
            'display_name' => 'Accountant Delivery',
            'description' => 'Can do specific tasks ',
        ]);
    }
}
