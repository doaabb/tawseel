<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('states')->insert([
           [
               'id'=>1,
               'name'=>'السيب',
               'governorate_id'=>1,
           ],
           [
               'id'=>2,
               'name'=>'مسقط',
               'governorate_id'=>1,
           ],
           [
               'id'=>3,
               'name'=>'مطرح',
               'governorate_id'=>1,
           ],
           [
               'id'=>4,
               'name'=>'العامرات',
               'governorate_id'=>1,
           ],
           [
               'id'=>5,
               'name'=>'بوشر',
               'governorate_id'=>1,
           ],
           [
               'id'=>7,
               'name'=>'قريات',
               'governorate_id'=>1,
           ],
           [
               'id'=>8,
               'name'=>'صلالة',
               'governorate_id'=>2,
           ],
           [
               'id'=>9,
               'name'=>'طاقة',
               'governorate_id'=>2,
           ],
           [
               'id'=>10,
               'name'=>'مرباط',
               'governorate_id'=>2,
           ],
           [
               'id'=>11,
               'name'=>'رخيوت',
               'governorate_id'=>2,
           ],
           [
               'id'=>12,
               'name'=>'ثمريت',
               'governorate_id'=>2,
           ],
           [
               'id'=>13,
               'name'=>'سدح',
               'governorate_id'=>2,
           ],
           [
               'id'=>14,
               'name'=>'ضلكوت',
               'governorate_id'=>2,
           ],
           [
               'id'=>15,
               'name'=>'المزيونة',
               'governorate_id'=>2,
           ],
           [
               'id'=>16,
               'name'=>'شليم',
               'governorate_id'=>2,
           ],
           [
               'id'=>17,
               'name'=>'جزر الحلانيات',
               'governorate_id'=>2,
           ],
           [
               'id'=>18,
               'name'=>'خصب',
               'governorate_id'=>3,
           ],
           [
               'id'=>19,
               'name'=>'دبا',
               'governorate_id'=>3,
           ],
           [
               'id'=>20,
               'name'=>'بخا',
               'governorate_id'=>3,
           ],
           [
               'id'=>21,
               'name'=>'مدحاء',
               'governorate_id'=>3,
           ],
           [
               'id'=>22,
               'name'=>'البريمي',
               'governorate_id'=>4,
           ],
           [
               'id'=>23,
               'name'=>'محضة',
               'governorate_id'=>4,
           ],
           [
               'id'=>24,
               'name'=>'السنينة',
               'governorate_id'=>4,
           ],
           [
               'id'=>25,
               'name'=>'نزوى',
               'governorate_id'=>5,
           ],
           [
               'id'=>26,
               'name'=>'بهلاء',
               'governorate_id'=>5,
           ],
           [
               'id'=>27,
               'name'=>'منح',
               'governorate_id'=>5,
           ],
           [
               'id'=>28,
               'name'=>'الحمراء',
               'governorate_id'=>5,
           ],
           [
               'id'=>29,
               'name'=>'أدم',
               'governorate_id'=>5,
           ],
           [
               'id'=>30,
               'name'=>'أزكي',
               'governorate_id'=>5,
           ],
           [
               'id'=>31,
               'name'=>'سمائل',
               'governorate_id'=>5,
           ],
           [
               'id'=>32,
               'name'=>'بدبد',
               'governorate_id'=>5,
           ],
           [
               'id'=>33,
               'name'=>'صحار',
               'governorate_id'=>7,
           ],
           [
               'id'=>34,
               'name'=>'شناص',
               'governorate_id'=>7,
           ],
           [
               'id'=>35,
               'name'=>'لوى',
               'governorate_id'=>7,
           ],
           [
               'id'=>36,
               'name'=>'صحم',
               'governorate_id'=>7,
           ],
           [
               'id'=>37,
               'name'=>'الخابورة',
               'governorate_id'=>7,
           ],
           [
               'id'=>38,
               'name'=>'السويق',
               'governorate_id'=>7,
           ],
           [
               'id'=>39,
               'name'=>'الرستاق',
               'governorate_id'=>8,
           ],
           [
               'id'=>40,
               'name'=>'العوابي',
               'governorate_id'=>8,
           ],
           [
               'id'=>41,
               'name'=>'نخل',
               'governorate_id'=>8,
           ],
           [
               'id'=>42,
               'name'=>'وادي المعاول',
               'governorate_id'=>8,
           ],
           [
               'id'=>43,
               'name'=>'بركاء',
               'governorate_id'=>8,
           ],
           [
               'id'=>44,
               'name'=>'المصنعة',
               'governorate_id'=>8,
           ],
           [
               'id'=>45,
               'name'=>'إبراء',
               'governorate_id'=>9,
           ],
           [
               'id'=>46,
               'name'=>'المضيبي',
               'governorate_id'=>9,
           ],
           [
               'id'=>47,
               'name'=>'بدية',
               'governorate_id'=>9,
           ],
           [
               'id'=>48,
               'name'=>'القابل',
               'governorate_id'=>9,
           ],
           [
               'id'=>49,
               'name'=>'وادي بني خالد',
               'governorate_id'=>9,
           ],
           [
               'id'=>50,
               'name'=>'دماء والطائيين',
               'governorate_id'=>9,
           ],
           [
               'id'=>51,
               'name'=>'صور',
               'governorate_id'=>10,
           ],
           [
               'id'=>52,
               'name'=>'الكامل والوافي',
               'governorate_id'=>10,
           ],
           [
               'id'=>53,
               'name'=>'جعلان بني بو حسن',
               'governorate_id'=>10,
           ],
           [
               'id'=>54,
               'name'=>'جعلان بني بو علي',
               'governorate_id'=>10,
           ],
           [
               'id'=>55,
               'name'=>'مصيرة',
               'governorate_id'=>10,
           ],
           [
               'id'=>56,
               'name'=>'عبري',
               'governorate_id'=>11,
           ],
           [
               'id'=>57,
               'name'=>'ينقل',
               'governorate_id'=>11,
           ],
           [
               'id'=>58,
               'name'=>'ضنك',
               'governorate_id'=>11,
           ],
           [
               'id'=>59,
               'name'=>'هيماء',
               'governorate_id'=>12,
           ],
           [
               'id'=>60,
               'name'=>'الدقم',
               'governorate_id'=>12,
           ],
           [
               'id'=>61,
               'name'=>'محوت',
               'governorate_id'=>12,
           ],
           [
               'id'=>62,
               'name'=>'الجازر',
               'governorate_id'=>12,
           ],
           [
               'id'=>63,
               'name'=>'الهجر',
               'governorate_id'=>12,
           ],
           /*
(8, 'صلالة', 2),
(9, 'طاقة', 2),
(10, 'مرباط', 2),
(11, 'رخيوت', 2),
(12, 'ثمريت', 2),
(13, 'سدح', 2),
(14, 'ضلكوت', 2),
(15, 'المزيونة', 2),
(16, 'شليم', 2),
(17, 'جزر الحلانيات', 2),
(18, 'خصب', 3),
(19, 'دبا', 3),
(20, 'بخا', 3),
(21, 'مدحاء', 3),
(22, 'البريمي', 4),
(23, 'محضة', 4),
(24, 'السنينة', 4),
(25, 'نزوى', 5),
(26, 'بهلاء', 5),
(27, 'منح', 5),
(28, 'الحمراء', 5),
(29, 'أدم', 5),
(30, 'أزكي', 5),
(31, 'سمائل', 5),
(32, 'بدبد', 5),
(33, 'صحار', 7),
(34, 'شناص', 7),
(35, 'لوى', 7),
(36, 'صحم', 7),
(37, 'الخابورة', 7),
(38, 'السويق', 7),
(39, 'الرستاق', 8),
(40, 'العوابي', 8),
(41, 'نخل', 8),
(42, 'وادي المعاول', 8),
(43, 'بركاء', 8),
(44, 'المصنعة', 8),
(45, 'إبراء', 9),
(46, 'المضيبي', 9),
(47, 'بدية', 9),
(48, 'القابل', 9),
(49, 'وادي بني خالد', 9),
(50, 'دماء والطائيين', 9),
(51, 'صور', 10),
(52, 'الكامل والوافي', 10),
(53, 'جعلان بني بو حسن', 10),
(54, 'جعلان بني بو علي', 10),
(55, 'مصيرة', 10),
(56, 'عبري', 11),
(57, 'ينقل', 11),
(58, 'ضنك', 11),
(59, 'هيماء', 12),
(60, 'الدقم', 12),
(61, 'محوت', 12),
(62, 'الجازر', 12),
(63, 'الهجر', 12)
            */
       ]);

    }
}
