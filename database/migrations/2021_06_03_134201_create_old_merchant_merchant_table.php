<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOldMerchantMerchantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('merchant_merchant', function (Blueprint $table) {
            $table->id();
            $table->integer('user_ptr_id')->nullable();
            $table->string('phoneAlt',12)->nullable();
            $table->text('description')->nullable();
            $table->string('activityType',50)->nullable();
            $table->dateTime('lastOrder')->nullable();
            $table->double('balance')->nullable();
            $table->boolean('activated')->nullable();
            $table->string('Employee_name',50)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('old_merchant_merchant');
    }
}
