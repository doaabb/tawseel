<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('order_id');
            $table->unsignedBigInteger('media_id')->nullable();

            $table->foreign('order_id')->references('id')->on('orders')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('media_id')->references('id')->on('media')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->text('description')->nullable();
            $table->string('code')->nullable();
            $table->string('cc')->nullable();
            $table->string('total_price')->nullable();
            $table->string('driver')->nullable();
            $table->string('picket_by')->nullable();
            $table->dateTime('pickup_date')->nullable();
            $table->string('pieces')->nullable();
            $table->string('weight')->nullable();
            $table->integer('state_id_shipper')->nullable();
            $table->string('tel_shipper')->nullable();
            $table->string('state_id_receiver')->nullable();
            $table->string('tel_receiver')->nullable();
            $table->longText('barcode')->nullable();
            $table->integer('random_number')->nullable();
            $table->string('path_barcode')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
}
