<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAuthuserUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('authuser_users', function (Blueprint $table) {
            $table->id();
            $table->string('name',50)->nullable();
            $table->string('email',254)->nullable();
            $table->string('phone',12)->nullable();
            $table->string('password',128)->nullable();
            $table->tinyInteger('is_superuser')->nullable();
            $table->string('IDCard',100)->nullable();
            $table->string('photo',100)->nullable();
            $table->text('district')->nullable();
            $table->dateTime('created')->nullable();
            $table->dateTime('last_login')->nullable();
            $table->integer('governorate_id')->nullable();
            $table->integer('state_id')->nullable();
            $table->integer('type_id')->nullable();
            $table->integer('branch_id')->nullable();
            $table->boolean('is_active')->nullable();
            $table->softDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('authuser_users');
    }
}
