<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->integer('dealer_id');
            $table->double('count_box');
            $table->string('dealer_name')->nullable();
            $table->integer('representative_id')->nullable();
            $table->integer('data_entry_id')->nullable();
            $table->integer('order_id')->nullable();
            $table->integer('order_type_id')->nullable();
            $table->string('order_type')->nullable();
            $table->double('price')->nullable();
            $table->double('price_deliver')->nullable();
            $table->double('price_cancel')->nullable();
            $table->double('price_from_client')->nullable();
            $table->date('date')->nullable();
            $table->time('time')->nullable();
            $table->integer('governorate_id')->nullable();
            $table->integer('state_id')->nullable();
            $table->integer('zone_id')->nullable();
            $table->integer('order_status')->default(null)->nullable();
            $table->boolean('is_active')->default(false)->nullable();
            $table->boolean('is_calculate')->default(false)->nullable();
            $table->text('description')->nullable();
            $table->longText('barcode')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
