<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOldOrderOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('old_orders', function (Blueprint $table) {
            $table->id();
            $table->string('clientName')->nullable();
            $table->string('clientPhone')->nullable();
            $table->text('clientDistrict')->nullable();
            $table->text('details')->nullable();
            $table->text('notes')->nullable();
            $table->text('barCode')->nullable();
            $table->double('Price_item')->nullable();
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->date('modified')->nullable();
            $table->dateTime('merchantOut')->nullable();
            $table->dateTime('stockIn')->nullable();
            $table->dateTime('handledAt')->nullable();
            $table->dateTime('deliveredAt')->nullable();
            $table->integer('agent_id')->nullable();
            $table->integer('clientGovernorate_id')->nullable();
            $table->integer('clientState_id')->nullable();
            $table->integer('merchant_id')->nullable();
            $table->integer('orderPaymentStatus_id')->nullable();
            $table->integer('orderPaymentType_id')->nullable();
            $table->integer('orderStatus_id')->nullable();
            $table->integer('orderType_id')->nullable();
            $table->string('location',200)->nullable();
            $table->double('delivery_price')->nullable();
            $table->double('cancellation_Price')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('old_order_order');
    }
}
