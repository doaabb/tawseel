{{--@auth()--}}
{{--    @if(auth()->user()->hasRole('super_admin') != null )--}}
@extends('new_layout.app')

@section('title', 'Prices')
{{--@section('page_title','View & Add Prices')--}}
@push('style')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

    <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}">
    <style>
        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            background-color: #343a40;
            border: 1px solid #343a40;
        }
    </style>
@endpush
@section('content')

    @role('admin|super_admin')
    {{--    @role('admin|super_admin')--}}
    <!-- Main content -->
            <div class="card card-default">
                <div class="card-header">
                    <h3 class="card-title">Add Price</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="row">
                        <div class="status">
                        </div>

                        <form class="form-horizontal col-md-12" id="create" METHOD="post" action="#"
                              enctype="multipart/form-data" autocomplete="on"
                        >
                            {{ csrf_field()}}

                            <div class="form-group">
                                <label class="form-label" for="basic-addon-name">Select Dealer </label>

                                <select
                                    class="form-control"
                                    name="dealer_id"
                                >
                                    <option ></option>
                                    @foreach($users as $user)
                                        <option value="{{ $user->id }}"> {{ $user->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="form-label" for="basic-addon-price_deliver">Price Deliver</label>

                                    <input
                                        type="number"
                                        id="basic-addon-price_deliver"
                                        class="form-control"
                                        placeholder="Price Deliver"
                                        aria-label="price_deliver"
                                        name="price_deliver"
                                        aria-describedby="basic-addon-price_deliver"
                                    />
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="form-label" for="basic-addon-price_cancel">Price Cancel</label>

                                    <input
                                        type="number"
                                        id="basic-addon-price_cancel"
                                        class="form-control"
                                        placeholder="Price Cancel"
                                        aria-label="price_cancel"
                                        name="price_cancel"
                                        aria-describedby="basic-addon-price_cancel"
                                    />
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">


                                    <label class="form-label" for="basic-addon-count_box">
                                        Governorate && State && Zone
                                    </label>


                                    <select class="form-control select2" name="state_id" style="width: 100%;">
                                        @foreach($gov as $gover)
                                            <optgroup label="{{ $gover->name }}" >

                                                @foreach($gover->state as $state)
                                                    <option  value="{{ $state->id }}">{{ $state->name }}</option>
                                                @endforeach
                                            </optgroup>

                                        @endforeach
                                    </select>
                                </div>
                            <div class="form-group col-md-6">
                                <label class="form-label" for="basic-addon-note">Note</label>

                                <input
                                    type="text"
                                    id="basic-addon-note"
                                    class="form-control"
                                    placeholder="Note"
                                    aria-label="note"
                                    name="note"
                                    aria-describedby="basic-addon-note"
                                />
                            </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <button type="submit" id="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.card-body -->
                    {{--                    <div class="card-footer">--}}
                    {{--                        .--}}
                    {{--                    </div>--}}
                </div>
                <!-- /.card -->
            </div>
   <!-- /.content -->

    <div class="col-12">

        <!-- /.card -->

        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Prices </h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table id="tbl-pieces" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th >id</th>
                        <th >Dealer</th>
                        <th>Price Deliver</th>
                        <th>Price Cancel</th>
                        <th >Action</th>
                        <th >State</th>
                        <th >Zone</th>
                        <th >Governorate</th>
                        <th >Note</th>
                        <th >Added By </th>
                        <th >Created at</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach( $prices as $index=>$price)
{{--                        @dd($prices[5]->dealer)--}}
                        <tr class="row_{{ $price->id }}">
                            <td >{{ $index+1}}</td>
                            <td >{{ $price->dealer?$price->dealer->name :'All'}}</td>
                            <td>{{ $price->price_deliver}}</td>
                            <td>{{ $price->price_cancel}}</td>
                            <td>
                                <div class="d-inline-flex">
                                    <a class="pr-1 dropdown-toggle hide-arrow text-primary"
                                       data-toggle="dropdown">

                                        <i data-feather="more-vertical"></i>
                                        <i class="more-vertical"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        {{--                                                <a href="javascript:;" class="dropdown-item">--}}
                                        {{--                                                    <i class="file-text"></i>View Users</a>--}}
                                        {{--                                        <a href="{{ route('edit_role', ['id' => $roles->id]) }}" class="dropdown-item">--}}
                                        {{--                                            Edit</a>--}}
                                        <a href="#" data-key="{{ $price->id }}"
                                           class="dropdown-item delete-record">

                                            Delete</a>
                                    </div>
                                </div>
                                <a href="javascript:;" class="item-edit">
                                    <i class="icon-wrapper"></i></a>
                            </td>
                            <td>{{ $price->state->name}}</td>
                            <td>{{ $price->zone ? $price->zone->name : ''}}</td>
                            <td>{{ $price->governorate->name}}</td>
                            <td>{{ $price->note}}</td>
                            <td>{{ $price->user->name}}</td>
                            <td>{{ $price->created_at}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th >id</th>
                        <th >Dealer</th>
                        <th>Price Deliver</th>
                        <th>Price Cancel</th>
                        <th >Action</th>
                        <th >State</th>
                        <th >Zone</th>
                        <th >Governorate</th>
                        <th >Note</th>
                        <th >Added By </th>
                        <th >Created at</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->

    @endrole
    {{--    @endrole--}}
@endsection

@push('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <!-- DataTables  & Plugins -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    {{--    <script src="dist/js/demo.js"></script>--}}
    <!-- Page specific script -->
    <script src="{{ asset('plugins/select2/js/select2.full.min.js') }}"></script>
    <script>
        $('.select2').select2();
        $(function () {
            $("#example1").DataTable({
                "responsive": true, "lengthChange": false, "autoWidth": false,
                "buttons": ["copy", "csv", "excel", "pdf", "print"]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
            $('#tbl-pieces').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });
        });
    </script>
    <script>


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('.delete-record').click(function (e) {
            e.preventDefault();
            let id = $(this).data("key");
            console.log(id);
            $.ajax({
                url: '{{ route('sdelete_price') }}',
                type: 'POST',
                async: true,
                cache: false,
                data: {
                    'id':id
                },
                success: function (response) {
                    toastr.success(" Your work has been saved", "Success");
                    $('.row_'+id).hide();

                },
                error: function () {
                    toastr.error(" Your work has been not saved", "Error");
                },
            })
        });
    </script>
    <script>
        $("#prices").addClass('active');
        $("#prices_view").parent().parent().parent().addClass('menu-open');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $("form").on("submit", function(event){

            event.preventDefault();

            let formData = new FormData(this);
            var formValues= $(this).serialize();

            console.log(formData);
            $.ajax({
                url: '{{ route('price_store') }}',
                type: 'POST',
                async: true,
                cache: false,
                data: formData,
                contentType: false,
                processData: false,
                success: function (response) {
                    printMsg(response);
                },
                error: function () {
                    $('.status').append('حصل خطأ أثناء الاتصال بالانترنت');
                },
            })
        });
        function printMsg (msg) {
            if($.isEmptyObject(msg.error)){
                toastr.success(" Your work has been saved", "Success");
                // $('input').val('') ;
            }else{
                $.each( msg.error, function( key, value ) {
                    // $('.status').text(value).css('color','red');
                    toastr.error(value, "Danger");
                });
            }
        }
    </script>
@endpush

{{--@else--}}
{{--@endif--}}
{{--@endauth--}}
