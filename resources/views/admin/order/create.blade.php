{{--@auth()--}}
{{--    @if(auth()->user()->hasRole('super_admin') != null )--}}
@extends('new_layout.app')

@section('title', 'Create New Order')
{{--@section('page_title','Create New Order')--}}
@push('style')
    {{-- Page Css files --}}

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
@endpush

@section('content')

        @role('admin|super_admin')
            <div class="card card-default">
                <div class="card-header">
                    <h3 class="card-title">Add Order</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="row">


                        <div class="status">

                        </div>
                        <form class="form-horizontal col-md-12" id="create" METHOD="post" action="#"
                              enctype="multipart/form-data" autocomplete="on"
                        >
                            {{ csrf_field()}}

                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="form-label" for="basic-addon-count_box">
                                        The number of goods
                                    </label>

                                    <input
                                        type="number"
                                        id="basic-addon-count_box"
                                        class="form-control"
                                        placeholder="The number of goods"
                                        aria-label="The number of goods"
                                        name="count_box"
                                        aria-describedby="basic-addon-count_box"
                                        required
                                    />
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="form-label" for="basic-addon-count_box">
                                        Governorate && State && Zone
                                    </label>


                                    <select class="form-control select2" name="zone" style="width: 100%;">
                                        @foreach($gov as $gover)
                                            <optgroup label="{{ $gover->name }}" style="font-size: 18px;background: #f4f6f9;">

                                                @foreach($gover->state as $state)

                                                    <optgroup label="{{ $state->name }}">

                                                        @foreach($state->zone as $zone)
                                                <option value="{{ $zone->id }}">{{ $zone->name }}</option>
                                                        @endforeach
                                                    </optgroup>
                                                @endforeach
                                            </optgroup>

                                        @endforeach
                                    </select>
                                </div>

                            </div>

                            <div class="row">

                                <div class="form-group col-md-6">

                                    <label  class="form-label">
                                        Date
                                    </label>
                                    <input type="date"  id="datepicker" name="date"
                                           class="form-control" >
                                </div>

                                <div class="form-group col-md-6">
                                    <label  class="form-label">Period</label>

                                    <input type="time"  id="timepicker" name="time"
                                           class="form-control" >

                                </div>
                                <!-- /.input group -->
                            </div>

                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="form-label" for="basic-addon-price">
                                        Price
                                    </label>

                                    <input
                                        type="text"
                                        id="basic-addon-price"
                                        class="form-control"
                                        placeholder="Price"
                                        aria-label="Price"
                                        name="price"
                                        aria-describedby="basic-addon-price"
                                    />
                                </div>
                                <div class="form-group col-md-6">

                                    <label>Order Type</label>

                                    <select class="form-control select2" name="order_type_id" style="width: 100%;">
                                        <option></option>
                                        @foreach($order_type as $type)
                                            <option value="{{ $type->id }}">{{ $type->name }}</option>
                                        @endforeach
                                    </select>
                                </div>


                            </div>

                            <div class="form-group">
                                <label>Description</label>
                                <textarea
                                    class="form-control"
                                    aria-label="description"
                                    name="description"
                                    aria-describedby="basic-addon-description"
                                >
                                </textarea>
                            </div>


                            {{--                                <div class="form-group">--}}
                            {{--                                    <label>Governorate</label>--}}
                            {{--                                    <select class="form-control select2" name="governorate_id" style="width: 100%;">--}}
                            {{--                                        @foreach($gov as $gover)--}}
                            {{--                                        <option value="{{ $gover->id }}">{{ $gover->name }}</option>--}}
                            {{--                                        @endforeach--}}
                            {{--                                    </select>--}}
                            {{--                                </div>--}}


                            {{--                                <div class="form-group">--}}
                            {{--                                    <label>State</label>--}}
                            {{--                                    <select class="form-control select2" name="state_id" style="width: 100%;">--}}
                            {{--                                        @foreach($state as $gover)--}}
                            {{--                                        <option value="{{ $gover->id }}">{{ $gover->name }}</option>--}}
                            {{--                                        @endforeach--}}
                            {{--                                    </select>--}}
                            {{--                                </div>--}}


                            <div class="row">
                                <div class="col-12">
                                    <button type="submit" id="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.card-body -->
                    {{--                    <div class="card-footer">--}}
                    {{--                        .--}}
                    {{--                    </div>--}}
                </div>
                <!-- /.card -->
            </div>


        @endrole
@endsection

@push('script')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script>

        $("#create_order").addClass('active');
        $("#create_order").parent().parent().parent().addClass('menu-open');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $("form").on("submit", function(event){

            event.preventDefault();

            let formData = new FormData(this);
            var formValues= $(this).serialize();

            console.log(formData);
            $.ajax({
                url: '{{ route('admin_store_order') }}',
                type: 'POST',
                async: true,
                cache: false,
                data: formData,
                contentType: false,
                processData: false,
                success: function (response) {
                    printMsg(response);
                },
                error: function () {
                    $('.status').append('حصل خطأ أثناء الاتصال بالانترنت');
                },
            })
        });
        function printMsg (msg) {
            if($.isEmptyObject(msg.error)){
                console.log(msg.success);
                toastr.success(" Your work has been saved", "Success");
                $('input').val('') ;
            }else{
                $.each( msg.error, function( key, value ) {
                    $('.status').text(value).css('color','red');
                });
            }
        }
    </script>
@endpush

{{--@else--}}
{{--@endif--}}
{{--@endauth--}}
