{{--@auth()--}}
{{--    @if(auth()->user()->hasRole('super_admin') != null )--}}
@extends('new_layout.app')

@section('title', 'Order Type')
{{--@section('page_title','View & Add Order Type')--}}
@push('style')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

    <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
@endpush
@section('content')

    @role('admin|super_admin')
    {{--    @role('admin|super_admin')--}}
            <div class="card card-default">
                <div class="card-header">
                    <h3 class="card-title">Create Order Type</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="row">
                        <div class="status">
                        </div>

                        <form class="form-horizontal col-md-12" id="create" METHOD="post" action="#"
                              enctype="multipart/form-data" autocomplete="on"
                        >
                            {{ csrf_field()}}
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="form-label" for="basic-addon-name">Order type name</label>

                                    <input
                                        type="text"
                                        id="basic-addon-name"
                                        class="form-control"
                                        placeholder="Order Type"
                                        aria-label="Order Type"
                                        name="name"
                                        aria-describedby="basic-addon-name"
                                    />
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="form-label" for="basic-addon-category">Category</label>

                                    <input
                                        type="text"
                                        id="basic-addon-category"
                                        class="form-control"
                                        placeholder="Category"
                                        aria-label="Category"
                                        name="category"
                                        aria-describedby="basic-addon-category"
                                    />
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6 ">
                                    <label class="form-label" for="basic-addon-note">Description</label>

                                    <textarea rows="2"
                                        id="basic-addon-description"
                                        class="form-control"
                                        placeholder="Description"
                                        aria-label="description"
                                        name="description"
                                        aria-describedby="basic-addon-description"
                                    ></textarea>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <button type="submit" id="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.card-body -->
                    {{--                    <div class="card-footer">--}}
                    {{--                        .--}}
                    {{--                    </div>--}}
                </div>
                <!-- /.card -->
            </div>


    <div class="col-12">

        <!-- /.card -->

        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Prices </h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table id="tbl-pieces" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th >id</th>
                        <th>Name</th>
                        <th>Category</th>
                        <th >Description</th>
                        <th >Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach( $order as $index=>$orders)
                        <tr class="row_{{ $orders->id }}">
                            <td >{{ $index+1}}</td>
                            <td>{{ $orders->name}}</td>
                            <td>{{ $orders->category}}</td>
                            <td>{{ $orders->description}}</td>
                            <td>
                                <div class="d-inline-flex">
                                    <a class="pr-1 dropdown-toggle hide-arrow text-primary"
                                       data-toggle="dropdown">

                                        <i data-feather="more-vertical"></i>
                                        <i class="more-vertical"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        {{--                                                <a href="javascript:;" class="dropdown-item">--}}
                                        {{--                                                    <i class="file-text"></i>View Users</a>--}}
                                        {{--                                        <a href="{{ route('edit_role', ['id' => $roles->id]) }}" class="dropdown-item">--}}
                                        {{--                                            Edit</a>--}}
                                        <a href="#" data-key="{{ $orders->id }}"
                                           class="dropdown-item delete-record">

                                            Delete</a>
                                    </div>
                                </div>
                                <a href="javascript:;" class="item-edit">
                                    <i class="icon-wrapper"></i></a>
                            </td>

                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th >id</th>
                        <th>Name</th>
                        <th>Category</th>
                        <th >Description</th>
                        <th >Action</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->

    @endrole
    {{--    @endrole--}}
@endsection

@push('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <!-- DataTables  & Plugins -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    {{--    <script src="dist/js/demo.js"></script>--}}
    <!-- Page specific script -->
    <script>
        $(function () {
            $("#example1").DataTable({
                "responsive": true, "lengthChange": false, "autoWidth": false,
                "buttons": ["copy", "csv", "excel", "pdf", "print"]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
            $('#tbl-pieces').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });
        });
    </script>
    <script>

        $("#list_gov").addClass('active');

        $("#list_gov").parent().parent().parent().addClass('menu-open');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('.delete-record').click(function (e) {
            e.preventDefault();
            let id = $(this).data("key");
            console.log(id);
            $.ajax({
                url: '{{ route('sdelete_order_type') }}',
                type: 'POST',
                async: true,
                cache: false,
                data: {
                    'id':id
                },
                success: function (response) {
                    toastr.success(" Your work has been saved", "Success");
                    $('.row_'+id).hide();

                },
                error: function () {
                    toastr.error(" Your work has been not saved", "Error");
                },
            })
        });
    </script>
    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $("form").on("submit", function(event){

            event.preventDefault();

            let formData = new FormData(this);
            var formValues= $(this).serialize();

            console.log(formData);
            $.ajax({
                url: '{{ route('order_type_store') }}',
                type: 'POST',
                async: true,
                cache: false,
                data: formData,
                contentType: false,
                processData: false,
                success: function (response) {
                    printMsg(response);
                },
                error: function () {
                    $('.status').append('حصل خطأ أثناء الاتصال بالانترنت');
                },
            })
        });
        function printMsg (msg) {
            if($.isEmptyObject(msg.error)){
                toastr.success(" Your work has been saved", "Success");
                // $('input').val('') ;
            }else{
                $.each( msg.error, function( key, value ) {
                    // $('.status').text(value).css('color','red');
                    toastr.error(value, "Danger");
                });
            }
        }
    </script>
@endpush

{{--@else--}}
{{--@endif--}}
{{--@endauth--}}
