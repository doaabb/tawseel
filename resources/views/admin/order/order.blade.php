@extends('layouts.app')

@section('new_layout', 'Orders')
@section('page_title','List Users')


@push('style')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

    <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
@endpush
@section('content')


    @role('admin|super_admin|accountant|accountant_dealer')

    <!-- Complex Headers -->


    <div class="col-12">

        <!-- /.card -->

        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Order Number : {{ $order->id }}</h3>
            </div>
            <div class="modal fade" id="modal-md" role="dialog" >
                <div class="modal-dialog modal-md  modal-dialog-scrollable" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Plaese Enter Description</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">

                            <form class="form-horizontal col-md-12" id="create" METHOD="post" action="#"
                                  enctype="multipart/form-data" autocomplete="on"
                            >
                                {{ csrf_field()}}
                                {{--                        @dd($id->media)--}}
                                <input type="hidden" name="id" value="" id="order_id">
                              <div class="row">
                                    <div class="form-group col-md-6">
                                        <label class="form-label" for="basic-addon-tel_receiver">
                                            Note *
                                        </label>

                                        <textarea
                                            type="text"
                                            rows="2"
                                            name="description"
                                        ></textarea>
                                    </div>
                                </div>
                                <div class="modal-footer justify-content-between">
                                    {{--                                <button type="button" class="btn btn-primary">Save changes</button>--}}
                                    <div class="row">
                                        <div class="col-12">
                                            <button type="submit" id="submit" class="btn btn-primary">Submit</button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>

            <!-- /.card-header -->
                 <div class="card-body">
                    <h4>

                    </h4>
                     <table id="tbl-pieces"  class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th >CC</th>
                            <th >Code</th>
                            <th >Pieces</th>
                            <th >Price</th>
                            <th >Price Deliver</th>
                            <th >Price Cancel</th>
                            <th >Cancel</th>
                            <th >Deliver</th>
                            <th >Driver</th>
                            <th >Delivery</th>
                            <th >Price from customer</th>
                            <th >State Shipper</th>
                            <th >State Receiver</th>
                            <th >Date Deliver</th>
                            <th >Total Price From Dealer</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php $total_price_deliver = 0; @endphp
                        @php $total_price_cancel = 0; @endphp
                        @php $total_price = 0; @endphp
                        @php $total_price_customer = 0; @endphp
                        @foreach($order->packages as $key=>$package)

                            <tr class="row_{{ $order->id }}">
                                <td>{{ $package->cc}}</td>
                                <td>{{ $package->code}}</td>
                                <td>{{ $package->pieces}}</td>
                                <td>{{ $package->total_price}}</td>
                                <td>{{ $package->price_deliver}}</td>
                                <td>{{ $package->price_cancel}}</td>
                                <td>@if($package->price_cancel != null)
                                -
                                    @else
                                        <span class="send-record_cancel badge bg-danger"
                                              data-key="{{ $package->id }}" data-toggle="modal"
                                               data-target="#modal-md">
                                            cancel
                                        </span>
                                    @endif
                                </td>
                                <td>@if($package->is_delivery == null)

                                        <a class="badge bg-info send-record_deliver"
                                           data-key="{{ $package->id }}"  >
ارساله للتوصيل؟
                                        </a>
                                    @elseif($package->is_delivery = 1)
                                        <span class="badge bg-success " >
                                            تم توصيله
                                        </span>
                                    @else
                                        تم الالغاء
                                    @endif
                                </td>
                                <td>{{ $package->driver}}</td>
                                <td>{{ $package->delivery}}</td>
                                <td>{{ $package->price_customer}}</td>
                                <td>{{ $package->state_id_shipper}}</td>
                                <td>{{ $package->state_id_receiver}}</td>
                                <td>{{ date('Y-m-d H:m:s', strtotime($package->created_at))}}</td>
                                <td>{{ $order->price }}</td>

                            </tr>
                            @php $total_price_deliver += $package->price_deliver; @endphp
                            @php $total_price_cancel += $package->price_cancel; @endphp
                            @php $total_price += $package->total_price; @endphp
                            @php $total_price_customer += $package->price_customer; @endphp
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th >CC</th>
                            <th >Code</th>
                            <th >Pieces</th>
                            <th >{{ $total_price }}</th>
                            <th >{{ $total_price_deliver }}</th>
                            <th >{{ $total_price_cancel }}</th>
                            <th >Cancel</th>
                            <th >Deliver</th>
                            <th >Driver</th>
                            <th >Delivery</th>
                            <th >{{ $total_price_customer }}</th>
                            <th >State Shipper</th>
                            <th >State Receiver</th>
                            <th >Date Deliver</th>
                            <th >Total Price From Dealer</th>
                        </tr>
                        </tfoot>
                    </table>

                </div>
        <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->


    @endrole
@stop
@push('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <!-- DataTables  & Plugins -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    {{--    <script src="dist/js/demo.js"></script>--}}
    <!-- Page specific script -->


    <script>
        $('.send-record_cancel').click(function (){

            let id = $(this).data("key");
            $('#order_id').val(id);
        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('.send-record_deliver').click(function (e) {
            e.preventDefault();
            let id = $(this).data("key");
            console.log(id);
            $.ajax({
                url: '{{ route('order_deliver') }}',
                type: 'POST',
                async: true,
                cache: false,
                data: {
                    'id':id
                },
                success: function (response) {
                    toastr.success(" Your work has been saved", "Success");
                    // $('.row_'+id).hide();
                },
                error: function () {
                    toastr.error(" Your work has been not saved", "Error");
                },
            })
        });

        $("form").on("submit", function(event){

            event.preventDefault();

            let formData = new FormData(this);
            var formValues= $(this).serialize();

            // console.log(formData);
            $.ajax({
                url: '{{ route('order_cancel') }}',
                type: 'POST',
                async: true,
                cache: false,
                data: formData,
                contentType: false,
                processData: false,
                success: function (response) {
                    printMsg(response);
                    // $("#clock_"+response.image_id).removeClass('fa-clock').addClass('fa-check');
                },
                error: function (msg) {
                    $.each( msg.error, function( key, value ) {
                        toastr.error(value, "Danger");
                    });
                },
            })
        });
        function printMsg (msg) {
            if($.isEmptyObject(msg.error)){
                console.log(msg.success);
                toastr.success(" Your work has been saved", "Success");
                $('input').val('') ;
                $('#modal-md').modal('hide');

            }else{
                $.each( msg.error, function( key, value ) {
                    toastr.error(value, "Danger");
                });
            }
        }
    </script>
    <script>
        $(function () {
            // $("#example1").DataTable({
            //     "responsive": true, "lengthChange": false, "autoWidth": false,
            //     "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
            // }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
            // $('table').DataTable();

            $('table.table').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });

        });
    </script>
    <script>

        $("#dealer_user").addClass('active');
        $("#dealer_user").parent().parent().parent().addClass('menu-open');
    </script>
@endpush
