@extends('new_layout.app')

@section('title', 'To')
{{--@section('page_title','List State')--}}

@push('style')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
@endpush
@section('content')


    @role('admin|super_admin')

    <!-- Complex Headers -->

    <div class="col-12">

        <!-- /.card -->

        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Order Number {{ $order->id }} </h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">


                    <div class="status">

                    </div>
                    <form class="form-horizontal col-md-12" id="create" METHOD="post" action="#"
                          enctype="multipart/form-data" autocomplete="on"
                    >
                        {{ csrf_field()}}
                        <input type="hidden" name="id" value="{{ $order->id }}">


                        {{--                        @foreach($users as $user)--}}

                        {{--                            @dd($user,$user->zone,$user->governorate())--}}
                        {{--                            <option value="{{ $user->id }}">{{ $user->name }} from {{ $user->governorate->name }}</option>--}}
                        {{--                        @endforeach--}}
                        <div class="form-group">
                            <label>Data Entry</label>
                            <select class="form-control select2" name="data_entry_id" style="width: 100%;">

                                    @foreach($users as $user)
                                        @if($user )
                                            <option value="{{ $user->id }}">
                                                {{ $user->name }}
                                            </option>
                                        @endif
                                    @endforeach
                            </select>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <button type="submit" id="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.card-body -->
                {{--                    <div class="card-footer">--}}
                {{--                        .--}}
                {{--                    </div>--}}
            </div>
            <!-- /.card -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->


    @endrole
@stop
@push('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $("form").on("submit", function(event){

            event.preventDefault();

            let formData = new FormData(this);
            var formValues= $(this).serialize();

            console.log(formData);
            $.ajax({
                url: '{{ route('sent_to_data_entry') }}',
                type: 'POST',
                async: true,
                cache: false,
                data: formData,
                contentType: false,
                processData: false,
                success: function (response) {
                    printMsg(response);
                },
                error: function () {
                    $('.status').html('حصل خطأ أثناء الاتصال بالانترنت');
                },
            })
        });
        function printMsg (msg) {
            if($.isEmptyObject(msg.error)){
                console.log(msg.success);
                toastr.success(" Your work has been saved", "Success");
                // $('input').val('') ;
            }else{
                $.each( msg.error, function( key, value ) {
                    $('.status').html(value).css('color','red');
                });
            }
        }
    </script>
@endpush
