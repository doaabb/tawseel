{{--@auth()--}}
{{--    @if(auth()->user()->hasRole('super_admin') != null )--}}
@extends('new_layout.app')

@section('title', 'Create New Zone')
{{--@section('page_title','Create New Zone')--}}
@push('style')
    {{-- Page Css files --}}

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
@endpush

@section('content')

        @role('admin|super_admin')
     <div class="card card-default">
                <div class="card-header">
                    <h3 class="card-title">Add Zone</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="row">


                        <div class="status">

                        </div>
                        <form class="form-horizontal col-md-12" id="create" METHOD="post" action="#"
                              enctype="multipart/form-data" autocomplete="on"
                        >
                            {{ csrf_field()}}
                            <div class="form-group">
                                <label class="form-label" for="basic-addon-name">Name</label>

                                <input
                                    type="text"
                                    id="basic-addon-name"
                                    class="form-control"
                                    placeholder="Name"
                                    aria-label="Name"
                                    name="name"
                                    aria-describedby="basic-addon-name"
                                    required
                                />
                            </div>


                            {{--                                <div class="form-group">--}}
                            {{--                                    <label>Governorate</label>--}}
                            {{--                                    <select class="form-control select2" name="governorate_id" style="width: 100%;">--}}
                            {{--                                        @foreach($gov as $gover)--}}
                            {{--                                        <option value="{{ $gover->id }}">{{ $gover->name }}</option>--}}
                            {{--                                        @endforeach--}}
                            {{--                                    </select>--}}
                            {{--                                </div>--}}


                            <div class="form-group">

                                <select class="form-control select2" name="state_id" style="width: 100%;">
                                    <optgroup label="Governorate">
                                        @foreach($gov as $gover)
                                            <optgroup label="{{ $gover->name }}">

                                                @foreach($gover->state as $state)

                                                    <option value="{{ $state->id }}">{{ $state->name }}</option>

                                                @endforeach
                                            </optgroup>

                                        @endforeach
                                    </optgroup>
                                </select>
                                {{--                                    <label>State</label>--}}
                                {{--                                    <select class="form-control select2" name="state_id" style="width: 100%;">--}}
                                {{--                                        @foreach($state as $gover)--}}
                                {{--                                        <option value="{{ $gover->id }}">{{ $gover->name }}</option>--}}
                                {{--                                        @endforeach--}}
                                {{--                                    </select>--}}
                            </div>


                            <div class="row">
                                <div class="col-12">
                                    <button type="submit" id="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.card-body -->
                    {{--                    <div class="card-footer">--}}
                    {{--                        .--}}
                    {{--                    </div>--}}
                </div>
                <!-- /.card -->
            </div>


        @endrole
@endsection

@push('script')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script>
        $("#create_zone").addClass('active');
        $("#create_zone").parent().parent().parent().addClass('menu-open');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $("form").on("submit", function(event){

            event.preventDefault();

            let formData = new FormData(this);
            var formValues= $(this).serialize();

            console.log(formData);
            $.ajax({
                url: '{{ route('store_zone') }}',
                type: 'POST',
                async: true,
                cache: false,
                data: formData,
                contentType: false,
                processData: false,
                success: function (response) {
                    printMsg(response);
                },
                error: function () {
                    $('.status').append('حصل خطأ أثناء الاتصال بالانترنت');
                },
            })
        });
        function printMsg (msg) {
            if($.isEmptyObject(msg.error)){
                console.log(msg.success);
                toastr.success(" Your work has been saved", "Success");
                // $('input').val('') ;
            }else{
                $.each( msg.error, function( key, value ) {
                    $('.status').text(value).css('color','red');
                });
            }
        }
    </script>
@endpush

{{--@else--}}
{{--@endif--}}
{{--@endauth--}}
