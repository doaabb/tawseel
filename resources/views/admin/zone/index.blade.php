@extends('new_layout.app')

@section('title', 'Zone')
{{--@section('page_title','List Zone')--}}

@push('style')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

    <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
@endpush
@section('content')


    @role('admin|super_admin')

    <!-- Complex Headers -->

    <div class="col-12">

        <!-- /.card -->

        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Zone </h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table id="tbl-pieces" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th >id</th>
                        <th >Name</th>
                        <th>Governorate</th>
                                                <th >State</th>
                        <th >Action</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach( $role as $roles)
{{--                        @dd($roles)--}}
                        <tr class="row_{{ $roles->id }}">
                            <td >{{ $roles->id}}</td>
                            <td>{{ $roles->name}}</td>
                            <td >{{ $roles->governorate->name }}</td>
                            <td >{{ $roles->state->name }}</td>
                            <td>
                                <div class="d-inline-flex">
                                    <a class="pr-1 dropdown-toggle hide-arrow text-primary"
                                       data-toggle="dropdown">

                                        <i data-feather="more-vertical"></i>
                                        <i class="more-vertical"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        {{--                                                <a href="javascript:;" class="dropdown-item">--}}
                                        {{--                                                    <i class="file-text"></i>View Users</a>--}}
                                        {{--                                        <a href="{{ route('edit_role', ['id' => $roles->id]) }}" class="dropdown-item">--}}
                                        {{--                                            Edit</a>--}}
                                        <a href="#" data-key="{{ $roles->id }}"
                                           class="dropdown-item delete-record">

                                            Delete</a>
                                    </div>
                                </div>
                                <a href="javascript:;" class="item-edit">
                                    <i class="icon-wrapper"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th >id</th>
                        <th >Name</th>
                        <th>Governorate</th>
                        <th >State</th>
                        <th >Action</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->


    @endrole
@stop
@push('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <!-- DataTables  & Plugins -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    {{--    <script src="dist/js/demo.js"></script>--}}
    <!-- Page specific script -->
    <script>
        $(function () {
            // $("#example1").DataTable({
            //     "responsive": true, "lengthChange": false, "autoWidth": false,
            //     "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
            // }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
            $('#tbl-pieces').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });
        });
    </script>
    <script>

        $("#list_zone").addClass('active');
        $("#list_zone").parent().parent().parent().addClass('menu-open');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('.delete-record').click(function (e) {
            e.preventDefault();
            let id = $(this).data("key");
            console.log(id);
            $.ajax({
                url: '{{ route('sdelete_zone') }}',
                type: 'POST',
                async: true,
                cache: false,
                data: {
                    'id':id
                },
                success: function (response) {
                    toastr.success(" Your work has been saved", "Success");
                    $('.row_'+id).hide();

                },
                error: function () {
                    toastr.error(" Your work has been not saved", "Error");
                },
            })
        });
    </script>
@endpush
