{{--@auth()--}}
{{--    @if(auth()->user()->hasRole('super_admin') != null )--}}
@extends('new_layout.app')

@section('title', 'Create New Governorate')
{{--@section('page_title','Create New Governorate')--}}
@push('style')
    {{-- Page Css files --}}


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

    <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
@endpush

@section('content')

{{--    @role('admin|super_admin')--}}
<div class="col-md-12">
    <div class="row">
        <!-- Main content -->
        <div class=" col-md-4">
                <div class="card card-default">
                    <div class="card-header">
                        <h3 class="card-title">Add Governorate</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">


                            <div class="status">

                            </div>
                            <form class="form-horizontal col-md-12" id="create_gov" METHOD="post" action="#"
                                  enctype="multipart/form-data" autocomplete="on"
                            >
                                {{ csrf_field()}}
                                <input type="hidden" value="gove_add" name="gove" id="gove_add">
                                <div class="form-group">
                                    <label class="form-label" for="basic-addon-name">Name</label>

                                    <input
                                        type="text"
                                        id="basic-addon-name"
                                        class="form-control"
                                        placeholder="Name"
                                        aria-label="Name"
                                        name="name"
                                        aria-describedby="basic-addon-name"
                                        required
                                    />
                                </div>

                                <div class="form-group">
                                    <label class="form-label" for="basic-addon-display_name">Call Code</label>

                                    <input
                                        type="text"
                                        id="call_code"
                                        class="form-control"
                                        placeholder="Call Code"
                                        aria-label="Call Code"
                                        name="call_code"
                                        aria-describedby="basic-addon-call_code"
                                    />

                                </div>


                                <div class="row">
                                    <div class="col-12">
                                        <button type="submit" id="submit" class="btn btn-primary add_gove">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- /.card-body -->
                        {{--                    <div class="card-footer">--}}
                        {{--                        .--}}
                        {{--                    </div>--}}
                    </div>
                    <!-- /.card -->
                </div>
             <!-- /.container-fluid -->
        </div>
        <!-- /.content -->
        <!-- Main content -->
        <div class=" col-md-4">
                <div class="card card-default">
                    <div class="card-header">
                        <h3 class="card-title">Add State</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">


                            <div class="status">

                            </div>
                            <form class="form-horizontal col-md-12" id="create_state" METHOD="post" action="#"
                                  enctype="multipart/form-data" autocomplete="on"
                            >
                                {{ csrf_field()}}
                                <input type="hidden" value="state_add" name="state"  id="state_add">
                                <div class="form-group">
                                    <label class="form-label" for="basic-addon-name">Name</label>

                                    <input
                                        type="text"
                                        id="basic-addon-name"
                                        class="form-control"
                                        placeholder="Name"
                                        aria-label="Name"
                                        name="name"
                                        aria-describedby="basic-addon-name"
                                        required
                                    />
                                </div>


                                <div class="form-group">
                                    <label>Governorate</label>
                                    <select class="form-control select2 select_gover" name="governorate_id" style="width: 100%;">
                                        @foreach($gov as $gover)
                                            <option value="{{ $gover->id }}">{{ $gover->name }}</option>
                                        @endforeach
                                    </select>
                                </div>


                                <div class="row">
                                    <div class="col-12">
                                        <button type="submit" id="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- /.card-body -->
                        {{--                    <div class="card-footer">--}}
                        {{--                        .--}}
                        {{--                    </div>--}}
                    </div>
                    <!-- /.card -->
                </div>
        </div>
        <!-- /.content -->
        <!-- Main content -->
        <div class=" col-md-4">
                <div class="card card-default">
                    <div class="card-header">
                        <h3 class="card-title">Add Zone</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">


                            <div class="status">

                            </div>
                            <form class="form-horizontal col-md-12" id="create_zone" METHOD="post" action="#"
                                  enctype="multipart/form-data" autocomplete="on"
                            >
                                {{ csrf_field()}}

                                <input type="hidden" value="zone_add"  name="zone" id="zone_add">
                                <div class="form-group">
                                    <label class="form-label" for="basic-addon-name">Name</label>

                                    <input
                                        type="text"
                                        id="basic-addon-name"
                                        class="form-control"
                                        placeholder="Name"
                                        aria-label="Name"
                                        name="name"
                                        aria-describedby="basic-addon-name"
                                        required
                                    />
                                </div>

                                <div class="form-group">

                                    <label>Governorate && State</label>
                                    <select class="form-control select2" name="state_id" style="width: 100%;">
                                        <optgroup label="Governorate">
                                            @foreach($gov as $gover)
                                                <optgroup label="{{ $gover->name }}">

                                                    @foreach($gover->state as $state)

                                                        <option value="{{ $state->id }}">{{ $state->name }}</option>

                                                    @endforeach
                                                </optgroup>

                                            @endforeach
                                        </optgroup>
                                    </select>
                                </div>


                                <div class="row">
                                    <div class="col-12">
                                        <button type="submit" id="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- /.card-body -->
                        {{--                    <div class="card-footer">--}}
                        {{--                        .--}}
                        {{--                    </div>--}}
                    </div>
                    <!-- /.card -->
                </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /.content -->

    </div>

</div>

<!-- Complex Headers -->

<div class="col-12">

    <!-- /.card -->

    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Governorate & State & Zone Table</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <table id="tbl-pieces" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th >id</th>
                    <th >Name</th>
                    <th>Call Code</th>
                    <th >Count State</th>
                    <th >Count Zone</th>
                    <th >Action</th>
                </tr>
                </thead>

                <tbody>
                @foreach( $gov as $gover)
                    <tr class="row_{{ $gover->id }}">
                        <td >{{ $gover->id}}</td>
                        <td>{{ $gover->name}}</td>
                        <td >{{ $gover->call_code }}</td>
                        <td >{{ count($gover->state) }}</td>
                        <td >{{ count($gover->zone) }}</td>
{{--                        <td >{{ $gover->call_code }}</td>--}}
                        <td>
                            <div class="d-inline-flex">
                                <a class="pr-1 dropdown-toggle hide-arrow text-primary"
                                   data-toggle="dropdown">

                                    <i data-feather="more-vertical"></i>
                                    <i class="more-vertical"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    {{--                                                <a href="javascript:;" class="dropdown-item">--}}
                                    {{--                                                    <i class="file-text"></i>View Users</a>--}}
                                    {{--                                        <a href="{{ route('edit_role', ['id' => $roles->id]) }}" class="dropdown-item">--}}
                                    {{--                                            Edit</a>--}}
                                    <a href="#" data-key="{{ $gover->id }}"
                                       class="dropdown-item delete-record">

                                        Delete</a>
                                </div>
                            </div>
                            <a href="javascript:;" class="item-edit">
                                <i class="icon-wrapper"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>

                <tr>
                    <th >id</th>
                    <th >Name</th>
                    <th>Call Code</th>
                    <th >Count State</th>
                    <th >Count Zone</th>
                    <th >Action</th>
                </tr>
                </tfoot>
            </table>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
</div>
<!-- /.col -->


{{--    @endrole--}}
@endsection

@push('script')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <!-- DataTables  & Plugins -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    {{--    <script src="dist/js/demo.js"></script>--}}
    <!-- Page specific script -->
    <script>
        $(function () {
            // $("#example1").DataTable({
            //     "responsive": true, "lengthChange": false, "autoWidth": true,
            //     "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
            // }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
            $('#tbl-pieces').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });
        });
    </script>
    <script>
        $("#create_gov").addClass('active');
        $("#create_gov").parent().parent().parent().addClass('menu-open');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $("form").on("submit", function(event){

            event.preventDefault();

            let formData = new FormData(this);
            var formValues= $(this).serialize();

            $.ajax({
                url: '{{ route('select_store') }}',
                type: 'POST',
                async: true,
                cache: false,
                data: formData,
                contentType: false,
                processData: false,
                success: function (response) {
                    printMsg(response);
                },
                error: function (response) {
                    $.each( response.error, function( key, value ) {
                    toastr.error(value, "Danger");
                });
                },
            })
        });
        function printMsg (msg) {
            if($.isEmptyObject(msg.error)){
                // console.log(msg.success);
                toastr.success(" Your work has been saved", "Success");
                // $('input').val('') ;
            }else{
                    $.each( msg.error, function( key, value ) {
                        toastr.error(value, "Danger");
                });
            }
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('.delete-record').click(function (e) {
            e.preventDefault();
            let id = $(this).data("key");
            console.log(id);
            $.ajax({
                url: '{{ route('sdelete_gov') }}',
                type: 'POST',
                async: true,
                cache: false,
                data: {
                    'id':id
                },
                success: function (response) {
                    toastr.success(" Your work has been saved", "Success");
                    $('.row_'+id).hide();

                },
                error: function () {
                    $.each( msg.error, function( key, value ) {
                    toastr.error(value, "Danger");
                });
                },
            })
        });
    </script>
@endpush

{{--@else--}}
{{--@endif--}}
{{--@endauth--}}
