{{--@auth()--}}
{{--    @if(auth()->user()->hasRole('super_admin') != null )--}}
{{--@auth()--}}
{{--    @if(auth()->user()->hasRole('super_admin') != null )--}}
@extends('new_layout.app')

@section('title', 'Edit Role')
{{--@section('page_title','Edit Role')--}}
@push('style')
    {{-- Page Css files --}}

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
@endpush

@section('content')
    <!-- Validation -->
            <div class="col-md-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Edit {{ $role->display_name }} Role</h4>
                    </div>
                    <div class="card-body">

                        <div class="status">

                        </div>
                        <form class="form-horizontal"
                              enctype="multipart/form-data" autocomplete="on"
                        >
                            {{ csrf_field()}}
                            <div class="form-group">
                                <label class="form-label" for="basic-addon-name">Name</label>

                                <input
                                        type="text"
                                        id="basic-addon-name"
                                        class="form-control"
                                        placeholder="Name"
                                        aria-label="Name"
                                        name="name"
                                        value="{{ $role->name }}"
                                        aria-describedby="basic-addon-name"
                                        required
                                />
                            </div>

                            <div class="form-group">
                                <label class="form-label" for="basic-addon-display_name">display name</label>

                                <input
                                        type="text"
                                        id="display_name"
                                        class="form-control"
                                        placeholder="Display Name"
                                        aria-label="display name"
                                        name="display_name"
                                        value="{{ $role->display_name }}"
                                        aria-describedby="basic-addon-name"
                                />
                                <div class="invalid-feedback">Please enter display name.</div>
                            </div>

                            <div class="form-group">
                                <label class="form-label" for="basic-addon-description">description</label>

                                <input
                                        type="text"
                                        id="description"
                                        class="form-control"
                                        placeholder="Description"
                                        aria-label="description"
                                        name="description"
                                        aria-describedby="basic-addon-description"
                                        value="{{ $role->description }}"
                                />
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <button type="submit" id="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

    <!-- /Validation -->
@endsection


@push('script')
    <!-- Page js files -->

    <script>
        $("#create_role").addClass('active');
        $("#create_role").parent().parent().parent().addClass('menu-open');


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $("form").on("submit", function(event){

            event.preventDefault();

            let formData = new FormData(this);
            var formValues= $(this).serialize();

            console.log(formData);
            $.ajax({
                url: '{{ route('update_role') }}',
                type: 'POST',
                async: true,
                cache: false,
                data: formData,
                contentType: false,
                processData: false,
{{--                url: '{{ route('') }}',--}}

                success: function (response) {
                    printMsg(response);
                },
                error: function () {
                    printMsg(response);
                },
            })
        });
        function printMsg (msg) {
            if($.isEmptyObject(msg.error)){
                console.log(msg.success);
                $('.status').html('<strong>تمت التعديل بنجاح</strong>').css('color','green');
                $('input').val('') ;
            }else{
                $.each( msg.error, function( key, value ) {
                    $('.status').text(value).css('color','red');
                });
            }
        }
    </script>
@endpush

{{--@else--}}
{{--@endif--}}
{{--@endauth--}}
