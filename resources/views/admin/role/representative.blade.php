{{--@auth()--}}
{{--    @if(auth()->user()->hasRole('super_admin') != null )--}}
@extends('new_layout.app')

@section('title', 'Representative Roles')
{{--@section('page_title','Representative Roles')--}}
@push('style')
    {{-- Page Css files --}}

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}">
    <style>
        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            background-color: #343a40;
            border: 1px solid #343a40;
        }
    </style>
@endpush

@section('content')

    @role('admin|super_admin')
           <div class="card card-default">
                <div class="card-header">
                    <h3 class="card-title">Representative Roles</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="row">


                        <div class="status">

                        </div>
                        <form class="form-horizontal col-md-12" id="create" METHOD="post" action="#"
                              enctype="multipart/form-data" autocomplete="on"
                        >
                            {{ csrf_field()}}
                            <div class="form-group">
                                <label class="form-label" for="basic-addon-name">All Representative </label>

                                <select
                                    class="form-control"
                                    name="user_id"
                                >
                                    <option ></option>
                                    @foreach($users as $user)
                                    <option value="{{ $user->id }}"> {{ $user->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label class="form-label" for="basic-addon-display_name">Select Zones</label>
{{--                                <select name="category[]" class="select2" multiple>--}}
                                <select style="width: 100%"
                                    class=" form-control select2"
                                    name="zone_id[]" multiple
                                >
                                    <option ></option>
                                    @foreach(\App\Models\Zone::all() as $zone)
                                        <option value="{{ $zone->id }}"> {{ $zone->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label class="form-label" for="basic-addon-display_name">Select State</label>
{{--                                <select name="category[]" class="select2" multiple>--}}
                                <select style="width: 100%"
                                    class=" form-control select2"
                                    name="state_id[]" multiple
                                >
                                    <option ></option>
                                    @foreach(\App\Models\State::all() as $zone)
                                        <option value="{{ $zone->id }}"> {{ $zone->name }}</option>
                                    @endforeach
                                </select>
                            </div>


                            <div class="form-group">
                                <label class="form-label" for="basic-addon-display_name">Select Governorate</label>
{{--                                <select name="category[]" class="select2" multiple>--}}
                                <select style="width: 100%"
                                    class=" form-control select2"
                                    name="governorate_id[]" multiple
                                >
                                    <option ></option>
                                    @foreach(\App\Models\Governorate::all() as $zone)
                                        <option value="{{ $zone->id }}"> {{ $zone->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <button type="submit" id="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.card-body -->
                    {{--                    <div class="card-footer">--}}
                    {{--                        .--}}
                    {{--                    </div>--}}
                </div>
                <!-- /.card -->
            </div>

    @endrole
@endsection

@push('script')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <!-- Select2 -->
    <script src="{{ asset('plugins/select2/js/select2.full.min.js') }}"></script>
    <script>
        $("#res_role").addClass('active');
        $("#res_role").parent().parent().parent().addClass('menu-open');


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $("form").on("submit", function(event){

            event.preventDefault();

            let formData = new FormData(this);
            var formValues= $(this).serialize();

            console.log(formData);
            $.ajax({
                url: '{{ route('store_res') }}',
                type: 'POST',
                async: true,
                cache: false,
                data: formData,
                contentType: false,
                processData: false,
                success: function (response) {
                    printMsg(response);
                },
                error: function () {
                    $('.status').append('حصل خطأ أثناء الاتصال بالانترنت');
                },
            })
        });
        function printMsg (msg) {
            if($.isEmptyObject(msg.error)){
                toastr.success(" Your work has been saved", "Success");
                // $('input').val('') ;
            }else{
                $.each( msg.error, function( key, value ) {
                    // $('.status').text(value).css('color','red');
                    toastr.error(value, "Danger");
                });
            }
        }
        $('.select2').select2();

    </script>
@endpush

{{--@else--}}
{{--@endif--}}
{{--@endauth--}}
