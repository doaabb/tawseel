{{--@auth()--}}
{{--    @if(auth()->user()->hasRole('super_admin') != null )--}}
@extends('new_layout.app')

@section('title', 'Create New Role')
{{--@section('page_title','Create New Role')--}}
@push('style')
    {{-- Page Css files --}}

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
@endpush

@section('content')

    @role('admin|super_admin')
     <div class="card card-default">
                <div class="card-header">
                    <h3 class="card-title">Add Role</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="row">


                        <div class="status">

                        </div>
                        <form class="form-horizontal col-md-12" id="create" METHOD="post" action="#"
                              enctype="multipart/form-data" autocomplete="on"
                        >
                            {{ csrf_field()}}
                            <div class="form-group">
                                <label class="form-label" for="basic-addon-name">Name</label>

                                <input
                                    type="text"
                                    id="basic-addon-name"
                                    class="form-control"
                                    placeholder="Name"
                                    aria-label="Name"
                                    name="name"
                                    aria-describedby="basic-addon-name"
                                    required
                                />
                            </div>

                            <div class="form-group">
                                <label class="form-label" for="basic-addon-display_name">Display name</label>

                                <input
                                    type="text"
                                    id="display_name"
                                    class="form-control"
                                    placeholder="Display Name"
                                    aria-label="display name"
                                    name="display_name"
                                    aria-describedby="basic-addon-name"
                                />

                            </div>

                            <div class="form-group">
                                <label class="form-label" for="basic-addon-description">Description</label>

                                <input
                                    type="text"
                                    id="description"
                                    class="form-control"
                                    placeholder="Description"
                                    aria-label="description"
                                    name="description"
                                    aria-describedby="basic-addon-description"
                                    value="Can do specific tasks "
                                />
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <button type="submit" id="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.card-body -->
{{--                    <div class="card-footer">--}}
{{--                        .--}}
{{--                    </div>--}}
                </div>
                <!-- /.card -->
            </div>


    @endrole
@endsection

@push('script')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script>
        $("#create_role").addClass('active');
        $("#create_role").parent().parent().parent().addClass('menu-open');


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $("form").on("submit", function(event){

            event.preventDefault();

            let formData = new FormData(this);
            var formValues= $(this).serialize();

            console.log(formData);
            $.ajax({
                url: '{{ route('store_role') }}',
                type: 'POST',
                async: true,
                cache: false,
                data: formData,
                contentType: false,
                processData: false,
                success: function (response) {
                    printMsg(response);
                },
                error: function () {
                    $('.status').append('حصل خطأ أثناء الاتصال بالانترنت');
                },
            })
        });
        function printMsg (msg) {
            if($.isEmptyObject(msg.error)){
                toastr.success(" Your work has been saved", "Success");
                // $('input').val('') ;
            }else{
                $.each( msg.error, function( key, value ) {
                    // $('.status').text(value).css('color','red');
                    toastr.error(value, "Danger");
                });
            }
        }
    </script>
@endpush

{{--@else--}}
{{--@endif--}}
{{--@endauth--}}
