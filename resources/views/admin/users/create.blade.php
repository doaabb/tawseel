{{--@auth()--}}
{{--    @if(auth()->user()->hasRole('super_admin') != null )--}}
@extends('new_layout.app')

@section('title', 'Create New User')
{{--@section('page_title','Create New User')--}}
@push('style')
    {{-- Page Css files --}}

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
@endpush

@section('content')

{{--    @role('admin|super_admin')--}}
    <!-- Main content -->
            <!-- SELECT2 EXAMPLE -->
            <div class="card card-default">
                <div class="card-header">
                    <h3 class="card-title">Add User</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="row">
                        <div class="status">
                        </div>
                        <form class="form-horizontal col-md-12" id="create" METHOD="post" action="#"
                              enctype="multipart/form-data" autocomplete="on"
                        >
                            {{ csrf_field()}}
                            <div class="row">
                            <div class="form-group col-md-6">
                                <label class="form-label" for="basic-addon-name">Name</label>

                                <input
                                    type="text"
                                    id="basic-addon-name"
                                    class="form-control"
                                    placeholder="Name"
                                    aria-label="Name"
                                    name="name"
                                    aria-describedby="basic-addon-name"
                                    required
                                />
                            </div>
                            <div class="form-group col-md-6">
                                <label class="form-label" for="basic-addon-email">Email</label>
                                <input
                                    type="email"
                                    id="basic-addon-email"
                                    class="form-control"
                                    placeholder="Email"
                                    aria-label="Email"
                                    name="email"
                                    aria-describedby="basic-addon-email"
                                    required
                                />
                            </div>
                            </div>
                            <div class="row">

                                <div class="form-group col-md-6">
                                    <label class="form-label" for="basic-addon-password">Password</label>
                                    <input
                                        type="password"
                                        id="basic-addon-password"
                                        class="form-control"
                                        placeholder="Password"
                                        aria-label="password"
                                        name="password"
                                        aria-describedby="basic-addon-password"
                                        required
                                    />
                                </div>
                                <div class="form-group col-md-6">

                                    <label>Role</label>
                                    <select class="form-control select2" name="role" required style="width: 100%;">
                                        <option ></option>
                                        @foreach($role as $roles)
                                            <option value="{{ $roles->name }}">{{ $roles->display_name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                            </div>
                            <div class="form-group ">
                                <label class="form-label" for="basic-addon-image_name">Avatar</label>
                                <input
                                    type="file"
                                    id="basic-addon-image_name"
                                    class="form-control"
                                    placeholder="image_name"
                                    aria-label="image_name"
                                    name="file"
                                    aria-describedby="basic-addon-image_name"
                                />
                            </div>
                            <div class="row">

                                <div class="form-group col-md-6">
                                    <label class="form-label" for="basic-addon-count_box">
                                        Governorate & State
                                    </label>


                                    <select class="form-control select2" name="state_id" id="state_id" style="width: 100%;">
                                        @foreach($gov as $gover)
                                            <optgroup label="{{ $gover->name }}">
                                                @foreach($gover->state as $state)
                                                    <option value="{{ $state->id }}">{{ $state->name }}</option>
                                                @endforeach
                                            </optgroup>

                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="form-label" for="basic-addon-count_box">
                                        Zone
                                    </label>


                                    <select class="form-control select2" name="zone" id="get_zone" style="width: 100%;">
                                        <option>اختر ولاية لتظهر لك المناطق التابعة لها هنا</option>
                                    </select>
                                </div>

                            </div>

                            <div class="form-group ">
                                <label class="form-label" for="basic-addon-coordinator_name">Coordinator Name</label>

                                <input
                                    type="text"
                                    id="basic-addon-coordinator_name"
                                    class="form-control"
                                    placeholder="Coordinator Name"
                                    aria-label="coordinator_name"
                                    name="coordinator_name"
                                    aria-describedby="basic-addon-coordinator_name"
                                />
                            </div>

                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="form-label" for="basic-addon-mobile">Mobile</label>

                                    <input
                                        type="text"
                                        id="basic-addon-mobile"
                                        class="form-control"
                                        placeholder="Mobile"
                                        aria-label="mobile"
                                        name="mobile"
                                        aria-describedby="basic-addon-mobile"
                                        required
                                    />
                                </div>

                                <div class="form-group col-md-6">
                                    <label class="form-label" for="basic-addon-whatsapp">Whatsapp</label>

                                    <input
                                        type="text"
                                        id="basic-addon-whatsapp"
                                        class="form-control"
                                        placeholder="Whatsapp"
                                        aria-label="whatsapp"
                                        name="whatsapp"
                                        aria-describedby="basic-addon-whatsapp"
                                        required
                                    />
                                </div>


                            </div>

                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="form-label" for="basic-addon-facebook">Facebook</label>

                                    <input
                                        type="text"
                                        id="basic-addon-facebook"
                                        class="form-control"
                                        placeholder="Facebook"
                                        aria-label="facebook"
                                        name="facebook"
                                        aria-describedby="basic-addon-facebook"
                                    />
                                </div>

                                <div class="form-group col-md-6">
                                    <label class="form-label" for="basic-addon-instagram">Instagram</label>

                                    <input
                                        type="text"
                                        id="basic-addon-instagram"
                                        class="form-control"
                                        placeholder="Instagram"
                                        aria-label="instagram"
                                        name="instagram"
                                        aria-describedby="basic-addon-instagram"
                                    />
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="form-label" for="basic-addon-address">Address</label>

                                <input
                                    type="text"
                                    id="basic-addon-address"
                                    class="form-control"
                                    placeholder="Address"
                                    aria-label="address"
                                    name="address"
                                    aria-describedby="basic-addon-address"
                                />
                            </div>
                            <div class="form-group ">
                                <label class="form-label" for="basic-addon-note">Note</label>

                                <input
                                    type="text"
                                    id="basic-addon-note"
                                    class="form-control"
                                    placeholder="Note"
                                    aria-label="note"
                                    name="note"
                                    aria-describedby="basic-addon-note"
                                />
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <button type="submit" id="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.card-body -->
{{--                    <div class="card-footer">--}}
{{--                        .--}}
{{--                    </div>--}}
                </div>
                <!-- /.card -->
            </div>

    <!-- /.content -->

{{--    @endrole--}}
@endsection

@push('script')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script>
        $("#create_user").addClass('active');
        $("#create_user").parent().parent().parent().addClass('menu-open');

        $('#state_id').change(function (){
            let id = $(this).val();
            console.log(id);

            $.ajax({
                url: '{{ route('get_zone') }}',
                type: 'POST',
                async: true,
                cache: false,
                data: {
                    'id':id
                },
                success: function (response) {
                    if(response.status === 200) {
                        // $('#get_zone').html('<option value="'+response.zone)
                        console.log(response.zone.length);

                        if(response.zone.length === 2){
                            $('#get_zone').html('<option  >لا يوجد مناطق تابعة لهذه الولاية </option>');

                        }else{

                            $('#get_zone').html('');
                            jQuery.each(JSON.parse(response.zone), function(i, val) {
                                // $("#" + i).append(document.createTextNode(" - " + val));
                                $('#get_zone').append('<option  value="'+val+'">'+i +'</option>');
                                // console.log(i + val)
                            });

                        }
                    }
                },
                error: function () {
                },
            })

        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $("form").on("submit", function(event){

            event.preventDefault();

            let formData = new FormData(this);
            var formValues= $(this).serialize();

            console.log(formData);
            $.ajax({
                url: '{{ route('store_user') }}',
                type: 'POST',
                async: true,
                cache: false,
                data: formData,
                contentType: false,
                processData: false,
                success: function (response) {
                    printMsg(response);
                },
                error: function () {
                    $('.status').append('حصل خطأ أثناء الاتصال بالانترنت');
                },
            })
        });
        function printMsg (msg) {
            if($.isEmptyObject(msg.error)){
                toastr.success(" Your work has been saved", "Success");
                // $('input').val('') ;
            }else{
                $.each( msg.error, function( key, value ) {
                    // $('.status').text(value).css('color','red');
                    toastr.error(value, "Danger");
                });
            }
        }
    </script>
@endpush

{{--@else--}}
{{--@endif--}}
{{--@endauth--}}
