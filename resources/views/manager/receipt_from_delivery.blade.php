{{--@auth()--}}
{{--    @if(auth()->user()->hasRole('super_admin') != null )--}}
@extends('new_layout.app')

@section('title', 'Receipt_from_delivery')
{{--@section('page_title','View & Add Prices')--}}
@push('style')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

    <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
    <style>
        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            background-color: #343a40;
            border: 1px solid #343a40;
        }
        .hide{
            display:none;
        }
        tr {
            border: 1px solid #ccc;
        }
    </style>
@endpush
@section('content')

    @role('admin|super_admin|store|operation|manager')
    {{--    @role('admin|super_admin')--}}
    <div class="card card-default">
                <div class="card-header">
                    <h3 class="card-title">Receipt from delivery</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="row">
                        <div class="status">
                        </div>
{{--                        <form action="{{ route('result_barcode') }}" method="post">--}}
{{--                            <input type="text" name="barcode" onmouseover="this.focus();" />--}}
{{--                        </form>--}}

                        <form class="form-horizontal col-md-6" id="create" METHOD="post"
                              enctype="multipart/form-data" autocomplete="on"
                        >
                            {{ csrf_field()}}

                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label class="form-label" for="basic-addon-user_number">Delivery Number </label>

                                    <input
                                        type="text"
                                        id="basic-addon-user_number"
                                        class="form-control"
                                        placeholder="Delivery Number "
                                        aria-label="Delivery Number "
                                        name="mobile"
                                        aria-describedby="basic-addon-user_number"
                                    />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <button type="submit" id="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </form>


                            <form id="barcode" class="form-horizontal  col-md-6 hide">
                                @csrf

                                <input type="hidden" name="id" id="new_res">
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label class="form-label" for="basic-addon-barcode">Enter Barcode Or Code </label>

                                        <input
                                            type="text"
                                            id="basic-addon-barcode"
                                            class="form-control"
                                            placeholder="barcode Number "
                                            aria-label="barcode Number "
                                            onmouseover="this.focus();" name="barcode"
                                            aria-describedby="basic-addon-barcode"
                                        />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <button type="submit" id="submit" class="btn btn-success">Submit</button>
                                    </div>
                                </div>
                            </form>
                        <div class="hide col-md-6" id="information">
                            <div class="col-md-12">
                                <!-- Widget: user widget style 1 -->
                                <div class="card card-widget widget-user">
                                    <!-- Add the bg color to the header using any of the bg-* classes -->

                                    <div class="widget-user-image  text-center">
                                        <img class="img-circle elevation-2   rounded-circle media-bordered mb-2" id="image_path" src="../dist/img/user1-128x128.jpg" alt="User Avatar">
                                    </div>
                                    <div class="widget-user-header">
                                        <h3 class="widget-user-username" id="user_name"></h3>
                                        <h5 class="widget-user-desc" id="state"></h5>
                                    </div>
                                    <div class="card-footer">
                                        <div class="row">
                                            <div class="col-sm-4 border-right">
                                                <div class="description-block">
                                                    <h5 class="description-header" id="mobile"></h5>
                                                    <span class="description-text">Mobile</span>
                                                </div>
                                                <!-- /.description-block -->
                                            </div>
                                            <!-- /.col -->
{{--                                            <div class="col-sm-4 border-right">--}}
{{--                                                <div class="description-block">--}}
{{--                                                    <h5 class="description-header" i></h5>--}}
{{--                                                    <span class="description-text">FOLLOWERS</span>--}}
{{--                                                </div>--}}
{{--                                                <!-- /.description-block -->--}}
{{--                                            </div>--}}
                                            <!-- /.col -->
                                            <div class="col-sm-8">
                                                <div class="description-block">
                                                    <h5 class="description-header" id="email"></h5>
                                                    <span class="description-text">Email</span>
                                                </div>
                                                <!-- /.description-block -->
                                            </div>
                                            <!-- /.col -->
                                        </div>
                                        <!-- /.row -->
                                    </div>
                                </div>
                                <!-- /.widget-user -->
                            </div>

                        </div>
                    </div>
                    <!-- /.card-body -->
                    {{--                    <div class="card-footer">--}}
                    {{--                        .--}}
                    {{--                    </div>--}}
                </div>
                <!-- /.card -->
            </div>
            <!-- SELECT2 EXAMPLE -->
            <div class="card card-default">
                <div class="card-header">
                    <h3 class="card-title">Result Search</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="row">
                            <table id="data_table_new" class="hide" style="width:100%;">
                                <thead>
                                <tr>
                                    <th>CC</th>
                                    <th>Code</th>
                                    <th>Dealer Name</th>
                                    <th>Dealer Mobile</th>
                                    <th>State Shipper</th>
                                    <th>State Reciever</th>
                                </tr>
                                </thead>
                                <tbody class="body_result_new">

                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                </tbody>
                            </table>

                            <table id="data_table" class="hide" style="width:100%;">
                                <thead>
                                <tr>
                                    <th>CC</th>
                                    <th>Code</th>
                                    <th>Dealer Name</th>
                                    <th>Dealer Mobile</th>
                                    <th>State Shipper</th>
                                    <th>State Reciever</th>
                                </tr>
                                </thead>
                                <tbody class="body_result">

                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                </tbody>
                            </table>

                    </div>
                    <!-- /.card-body -->
                    {{--                    <div class="card-footer">--}}
                    {{--                        .--}}
                    {{--                    </div>--}}
                </div>
                <!-- /.card -->
            </div>


    @endrole
    {{--    @endrole--}}
@endsection

@push('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <!-- DataTables  & Plugins -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>

    <script>
        $("#receipt_from_delivery").addClass('active');
        $("#receipt_from_delivery").parent().parent().parent().addClass('menu-open');
        // $('#data_table').dataTable();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $("form").on("submit", function(event){

            event.preventDefault();

            let formData = new FormData(this);
            var formValues= $(this).serialize();

            console.log(formData);
            $.ajax({
                url: '{{ route('result_delivery') }}',
                type: 'POST',
                async: true,
                cache: false,
                data: formData,
                contentType: false,
                processData: false,
                beforeSend:function (){
                    $('#data_table').show();

                },
                success: function (response) {
                    if(response.status === 201){
                        $('#barcode').show();
                        $('#information').show();
                        console.log(response);
                        $('.body_result').append(response.row);
                        $('#user_name').append(response.name);
                        $('#email').append(response.email);
                        $('#mobile').append(response.mobile);
                        $('#state').html(response.state);
                        $('#image_path').attr('src',response.image_path);
                        $('#new_res').val(response.id);
                        $('#create').hide();

                    }
                    if(response.status === 200){
                        printMsg(response);
                        $('#data_table_new').show();
                        console.log(response);
                        $('.body_result_new').appendTo(response.row);

                    }
                    if(response.error){

                        toastr.error(response.error, "Danger");
                    }
                },
                error: function () {
                    toastr.error('Error Connection', "Danger");
                },
            })
        });
        function printMsg (msg) {
            if($.isEmptyObject(msg.error)){
                toastr.success(" Your work has been saved", "Success");
                $('input').val('') ;
            }else{
                $.each( msg.error, function( key, value ) {
                    // $('.status').text(value).css('color','red');
                    toastr.error(value, "Danger");
                });
            }
        }
    </script>
@endpush

{{--@else--}}
{{--@endif--}}
{{--@endauth--}}
