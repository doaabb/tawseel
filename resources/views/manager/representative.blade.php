@extends('new_layout.app')

@section('title', 'Users')
@section('page_title','List Users')

@push('style')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

    <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
@endpush
@section('content')


    @role('admin|super_admin|manager|store')

    <!-- Complex Headers -->

    <div class="col-12">

        <!-- /.card -->

        <div class="card">
            <div class="card-header">
                <h3 class="card-title">All Representative </h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table id="tbl-pieces" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th >Name</th>
                        <th>Email</th>
                        <th>Whatsapp</th>
                        <th>Image</th>
                        <th>Mobile</th>
                        <th>Show Orders</th>
                        <th>Governorate</th>
                        <th>State</th>
                        <th>Zone</th>
                        <th>Coordinator Name</th>
                        <th>Address</th>
                        <th>Facebook</th>
                    </tr>
                    </thead>

                    {{--                         'name',
                            'email',
                            'password',
                            'coordinator_name','mobile','address','whatsapp','facebook','instagram',
                            'image_name','image_path','zone','state','governorate','note',
                            'state_id','governorate_id','zone_id'--}}
                    <tbody>
                    @foreach( $users as $index=>$user)
                        <tr class="row_{{ $user->id }}">
                            <td>{{ $user->name}}</td><td>{{ $user->email}}</td>

                            <td>
                                <a
                                    target="_blank" href="https://api.whatsapp.com/send/?phone={{ $user->whatsapp}}&text&app_absent=0" >
                                    Whatsapp
                                </a></td>
                            <td><img src="{{ $user->image_path ?? asset('dist/img/AdminLTELogo.png')}}" height="45" width="45"></td>

                            <td><a href="tel://{{ $user->mobile}}">Call</a> </td>
                            <td><a href="{{ route('representative_show',['id'=>$user->id]) }}">Show orders</a></td>
                            <td>{{ $user->governorate}}</td>
                            <td>{{ $user->state}}</td><td>{{ $user->zone}}</td><td>{{ $user->coordinator_name}}</td>
                            <td>{{ $user->address}}</td><td>{{ $user->facebook}}</td>
                            {{--                            <td>{{ $user->whatsapp}}</td>--}}
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th >Name</th>
                        <th>Email</th>
                        <th>Whatsapp</th>
                        <th>Image</th>
                        <th>Mobile</th>
                        <th>Show Orders</th>
                        <th>Governorate</th>
                        <th>State</th>
                        <th>Zone</th>
                        <th>Coordinator Name</th>
                        <th>Address</th>
                        <th>Facebook</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->


    @endrole
@stop
@push('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <!-- DataTables  & Plugins -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    {{--    <script src="dist/js/demo.js"></script>--}}
    <!-- Page specific script -->
    <script>
        $(function () {
            // $("#example1").DataTable({
            //     "responsive": true, "lengthChange": false, "autoWidth": false,
            //     "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
            // }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
            $('#tbl-pieces').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });
        });
    </script>
    <script>

        $("#list_dealer").addClass('active');
        $("#list_dealer").parent().parent().parent().addClass('menu-open');
        {{--$.ajaxSetup({--}}
        {{--    headers: {--}}
        {{--        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')--}}
        {{--    }--}}
        {{--});--}}

        {{--$('.delete-record').click(function (e) {--}}
        {{--    e.preventDefault();--}}
        {{--    let id = $(this).data("key");--}}
        {{--    console.log(id);--}}
        {{--    $.ajax({--}}
        {{--        url: '{{ route('sdelete_user') }}',--}}
        {{--        type: 'POST',--}}
        {{--        async: true,--}}
        {{--        cache: false,--}}
        {{--        data: {--}}
        {{--            'id':id--}}
        {{--        },--}}
        {{--        success: function (response) {--}}
        {{--            toastr.success(" Your work has been saved", "Success");--}}
        {{--            $('.row_'+id).hide();--}}

        {{--        },--}}
        {{--        error: function () {--}}
        {{--            toastr.error(" Your work has been not saved", "Error");--}}
        {{--        },--}}
        {{--    })--}}
        {{--});--}}
    </script>
@endpush
