{{--@auth()--}}
{{--    @if(auth()->user()->hasRole('super_admin') != null )--}}
@extends('new_layout.app')

@section('title', 'Create & List Orders')
{{--@section('page_title','Create New Order')--}}
@push('style')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

    <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
@endpush
@section('content')

    @role('admin|super_admin|manager|operation|store')
    <!-- /.content -->
    <div class="col-12">

        <!-- /.card -->

        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Orders </h3>
            </div>
            <!-- card-header -->
            <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        {{--                            <th >id</th>--}}
                        <th>Dealer Name</th>
                        <th>Data Entry Name</th>
                        <th >Count Boxes</th>
                        <th >Count Boxes Data entry</th>
                        <th >Price</th>
                        <th >Price Deliver</th>
                        <th >Order Type</th>
                        <th >State</th>
                        <th >Action</th>
                        <th >Zone</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach( $order as $index=>$orders)
{{--                        @dd($orders->dataentrydata)--}}
                        <tr class="row_{{ $orders->id }}">
                            {{--                                <td >{{ $orders->id}}</td>--}}
                            <td>{{ $orders->dealer->name}}</td>
                            <td>{{ $orders->data_entry?$orders->data_entry->name: ''}}</td>
                           <td>{{ $orders->count_box}}</td>
                            <td>
                                @if($orders->packages)
                                    <a   class="badge bg-success" href="{{route('order_manager',['id'=>$orders->id])}}">
                                        {{count($orders->packages )}}
                                    </a>
                                    still {{$orders->count_box- count($orders->packages )}}
                                @else
                                    {{ $orders->count_box }}
                                @endif
                            </td>
                            <td>{{ $orders->price}}</td>
                            <td>{{ $orders->price_deliver}}</td>
                            <td>{{ $orders->order_type}}</td>
                            <td>{{ $orders->state->name}}</td>
                            <td>
                                @if($orders->is_active = 2)
                                    -
                                @else
                                <span class="badge bg-warning">
                                    <a href="{{ route('order_manager',['id'=>$orders->id]) }}">Enable</a>
                                </span>
                                @endif
{{--                                <div class="d-inline-flex">--}}
{{--                                    <a class="pr-1 dropdown-toggle hide-arrow text-primary"--}}
{{--                                       data-toggle="dropdown">--}}

{{--                                        <i data-feather="more-vertical"></i>--}}
{{--                                        <i class="more-vertical"></i>--}}
{{--                                    </a>--}}
{{--                                    <div class="dropdown-menu dropdown-menu-right">--}}
{{--                                        <a href="javascript:;" class="dropdown-item">--}}
{{--                                            <i class="file-text"></i>View Users</a>--}}
{{--                                        <a href="{{ route('enable_order',['id'=>$orders->id]) }}" class="dropdown-item send-record" data-key="{{ $orders->id }}">--}}
{{--                                        Go To Enable </a>--}}
{{--                                        <a href="#" data-key="{{ $orders->id }}"--}}
{{--                                           class="dropdown-item delete-record">--}}

{{--                                            Delete</a>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <a href="javascript:;" class="item-edit">--}}
{{--                                    <i class="icon-wrapper"></i></a>--}}
                            </td>

                            <td>{{ $orders->zone?$orders->zone->name:''}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        {{--                            <th >id</th>--}}
                        <th>Dealer Name</th>
                        <th>Data Entry Name</th>
                        <th >Count Boxes</th>
                        <th >Count Boxes Data entry</th>
                        <th >Price</th>
                        <th >Price Deliver</th>
                        <th >Order Type</th>
                        <th >State</th>
                        <th >Action</th>
                        <th >Zone</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->

    @endrole
@endsection

@push('script')

    <!-- jQuery -->
    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- DataTables  & Plugins -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    {{--    <script src="dist/js/demo.js"></script>--}}
    <!-- Page specific script -->
    <script>
        $(function () {
            $("#example1").DataTable({
                "responsive": true, "lengthChange": false, "autoWidth": false,
                "buttons": ["copy", "csv", "excel", "pdf", "print"]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
            // $('#tbl-pieces').DataTable({
            //     "paging": true,
            //     "lengthChange": true,
            //     "searching": true,
            //     "ordering": true,
            //     "info": true,
            //     "autoWidth": false,
            //     "responsive": true,
            // });
        });
    </script>
    <script>

    </script>
@endpush

{{--@else--}}
{{--@endif--}}
{{--@endauth--}}
