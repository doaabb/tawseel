{{--@auth()--}}
{{--    @if(auth()->user()->hasRole('super_admin') != null )--}}
@extends('new_layout.app')

@section('title', 'Search With Code')
{{--@section('page_title','View & Add Prices')--}}
@push('style')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

    <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}">
    <style>
        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            background-color: #343a40;
            border: 1px solid #343a40;
        }
        .hide{
        display:none;
        }
    </style>
@endpush
@section('content')

    @role('admin|super_admin|store|operation|manager')
    {{--    @role('admin|super_admin')--}}
          <div class="card card-default">
                <div class="card-header">
                    <h3 class="card-title">Search</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="row">
                        <div class="status">
                        </div>
                        {{--                        <form action="{{ route('result_barcode') }}" method="post">--}}
                        {{--                            <input type="text" name="barcode" onmouseover="this.focus();" />--}}
                        {{--                        </form>--}}

                        <form class="form-horizontal col-md-12" id="create" METHOD="post"
                              enctype="multipart/form-data" autocomplete="on"
                        >
                            {{ csrf_field()}}

                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="form-label" for="basic-addon-barcode">phone Number</label>

                                    <input
                                        type="text"
                                        onmouseover="this.focus();"
                                        id="basic-addon-code"
                                        class="form-control"
                                        placeholder="Code"
                                        aria-label="Code"
                                        name="mobile"
                                        aria-describedby="basic-addon-code"
                                        required
                                    />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <button type="submit" id="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
{{--                    <form   class="form-horizontal col-md-12 m-2" >--}}
{{--                        <input class="form-control" type="text">--}}
{{--                        <input type="submit">--}}
{{--                    </form>--}}
{{--                    <table class="hide">--}}
{{--                        <thead>--}}
{{--                        <tr>--}}
{{--                            <th >CC</th>--}}
{{--                            <th >Code</th>--}}
{{--                            <th >tel receiver</th>--}}
{{--                            <th >tel shipper</th>--}}
{{--                            <th >Pieces</th>--}}
{{--                            <th >Price</th>--}}
{{--                            <th >Price Deliver</th>--}}
{{--                            <th >Price Cancel</th>--}}
{{--                            <th >Price from customer</th>--}}
{{--                            <th >Date Deliver</th>--}}
{{--                            <th >Total Price From Dealer</th>--}}
{{--                        </tr>--}}
{{--                        </thead>--}}
{{--                        <tbody>--}}
{{--                        </tbody>--}}
{{--                    </table>--}}
                    <!-- /.card-body -->
                    {{--                    <div class="card-footer">--}}
                    {{--                        .--}}
                    {{--                    </div>--}}
                </div>
                <!-- /.card -->
            </div>

    @endrole
    {{--    @endrole--}}
@endsection

@push('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

    <script>
        $("#search_with_code").addClass('active');
        $("#search_with_code").parent().parent().parent().addClass('menu-open');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $("form").on("submit", function(event){

            event.preventDefault();

            let formData = new FormData(this);
            var formValues= $(this).serialize();

            console.log(formData);
            $.ajax({
                url: '{{ route('result_mobile') }}',
                type: 'POST',
                async: true,
                cache: false,
                data: formData,
                contentType: false,
                processData: false,
                success: function (response) {
                    printMsg(response);
                    if(response.status === 200){
                        window.location.replace('representative_show/'+response.id)
                    }
                },
                error: function () {
                    toastr.error('Error Connection', "Danger");
                },
            })
        });
        function printMsg (msg) {
            if($.isEmptyObject(msg.error)){
                toastr.success(" Your work has been saved", "Success");
                // $('input').val('') ;
            }else{
                $.each( msg.error, function( key, value ) {
                    // $('.status').text(value).css('color','red');
                    toastr.error(value, "Danger");
                });
            }
        }
    </script>
@endpush

{{--@else--}}
{{--@endif--}}
{{--@endauth--}}
