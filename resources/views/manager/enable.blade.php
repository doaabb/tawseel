@extends('new_layout.app')

@section('title', 'To')
{{--@section('page_title','List State')--}}

@push('style')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
@endpush
@section('content')


    @role('admin|super_admin|store|operation')

    <!-- Complex Headers -->

    <div class="col-12">

        <!-- /.card -->

        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Order Number {{ $id->id }} </h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">


                    <div class="status">

                    </div>
                    <form class="form-horizontal col-md-12" id="create" METHOD="post" action="#"
                          enctype="multipart/form-data" autocomplete="on"
                    >
                        {{ csrf_field()}}
{{--                        @dd($id->media)--}}
                        <input type="hidden" name="id" value="{{ $id->id }}">

                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="form-label" for="basic-addon-code">
                                    Code
                                </label>

                                <input
                                    type="text"
                                    id="basic-addon-code"
                                    class="form-control"
                                    placeholder="Code"
                                    aria-label="Code"
                                    name="code"
                                    value="{{ $id->code }}"
                                    aria-describedby="basic-addon-code"
                                    required
                                />
                            </div>
                            <div class="form-group col-md-6">
                                <label class="form-label" for="basic-addon-cc">
                                    CC
                                </label>

                                <input
                                    type="text"
                                    id="basic-addon-cc"
                                    class="form-control"
                                    placeholder="CC"
                                    aria-label="CC"
                                    value="{{ $id->cc }}"
                                    aria-describedby="basic-addon-cc"
                                    name="cc"
                                    required
                                />
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="form-label" for="basic-addon-total_price">
                                    Total Price
                                </label>

                                <input
                                    type="text"
                                    id="basic-addon-total_price"
                                    class="form-control"
                                    placeholder="Total Price"
                                    aria-label="Total Price"
                                    name="total_price"
                                    value="{{ $id->total_price }}"
                                    aria-describedby="basic-addon-total_price"
                                    required
                                />
                            </div>
                            <div class="form-group col-md-6">
                                <label class="form-label" for="basic-addon-driver">
                                    Driver
                                </label>

                                <input
                                    type="text"
                                    id="basic-addon-driver"
                                    class="form-control"
                                    placeholder="Driver"
                                    aria-label="Driver"
                                    value="{{ $id->driver }}"
                                    aria-describedby="basic-addon-driver"
                                    name="driver"
                                    required
                                />
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="form-label" for="basic-addon-picket_by">
                                    Picket By
                                </label>

                                <input
                                    type="text"
                                    id="basic-addon-picket_by"
                                    class="form-control"
                                    placeholder="Picket By"
                                    aria-label=" Picket By"
                                    name="picket_by"
                                    value="{{ $id->picket_by }}"
                                    aria-describedby="basic-addon-Picket By"
                                    required
                                />
                            </div>
                            <div class="form-group col-md-6">
                                <label class="form-label" for="basic-addon-pickup_date">
                                    pickup_date
                                </label>

                                <input
                                    type="datetime-local"
                                    id="basic-addon-pickup_date"
                                    class="form-control"
                                    placeholder="pickup_date"
                                    aria-label="pickup_date"
                                    value="{{ $id->pickup_date }}"
                                    aria-describedby="basic-addon-pickup_date"
                                    name="pickup_date"
                                    required
                                />
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="form-label" for="basic-addon-pieces">
                                    pieces
                                </label>

                                <input
                                    type="number"
                                    id="basic-addon-pieces"
                                    class="form-control"
                                    placeholder="pieces"
                                    aria-label="pieces"
                                    name="pieces"
                                    value="{{ $id->pieces }}"
                                    aria-describedby="basic-addon-pieces"
                                    required
                                />
                            </div>
                            <div class="form-group col-md-6">
                                <label class="form-label" for="basic-addon-weight">
                                    Weight
                                </label>

                                <input
                                    type="text"
                                    id="basic-addon-weight"
                                    class="form-control"
                                    placeholder="weight"
                                    aria-label="weight"
                                    name="weight"
                                    value="{{ $id->weight }}"
                                    aria-describedby="basic-addon-weight"
                                    required
                                />
                            </div>
                        </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="form-label" for="basic-addon-tel_shipper">
                                Tel Shipper
                            </label>

                            <input
                                type="text"
                                id="basic-addon-tel_shipper"
                                class="form-control"
                                placeholder="tel_shipper"
                                aria-label="tel_shipper"
                                name="tel_shipper"
                                value="{{ $id->tel_shipper }}"
                                aria-describedby="basic-addon-tel_shipper"
                                required
                            />
                        </div>
                            <div class="form-group col-md-6">
                                <label class="form-label" for="basic-addon-count_box">
                                    Governorate && State Shiper
                                </label>


                                <select class="form-control select2" name="state_id_shipper" id="state_id" style="width: 100%;">
                                    @foreach($gov as $gover)
                                        <optgroup label="{{ $gover->name }}">
                                            @foreach($gover->state as $state)
                                                <option value="{{ $state->id }}">{{ $state->name }}</option>
                                            @endforeach
                                        </optgroup>

                                    @endforeach
                                </select>
                            </div>
                        </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="form-label" for="basic-addon-tel_receiver">
                                Tel Receiver
                            </label>

                            <input
                                type="text"
                                id="basic-addon-tel_receiver"
                                class="form-control"
                                placeholder="tel_receiver"
                                aria-label="tel_shipper"
                                name="tel_receiver"
                                value="{{ $id->tel_receiver }}"
                                aria-describedby="basic-addon-tel_receiver"
                                required
                            />
                        </div>
                            <div class="form-group col-md-6">
                                <label class="form-label" for="basic-addon-count_box">
                                    Governorate && State Reciever
                                </label>


                                <select class="form-control select2" name="state_id_receiver" id="state_id_receiver" style="width: 100%;">
                                    @foreach($gov as $gover)
                                        <optgroup label="{{ $gover->name }}">
                                            @foreach($gover->state as $state)
                                                <option value="{{ $state->id }}">{{ $state->name }}</option>
                                            @endforeach
                                        </optgroup>

                                    @endforeach
                                </select>
                            </div>
                        </div>

                        {{--                        @foreach($users as $user)--}}

                        {{--                            @dd($user,$user->zone,$user->governorate())--}}
                        {{--                            <option value="{{ $user->id }}">{{ $user->name }} from {{ $user->governorate->name }}</option>--}}
                        {{--                        @endforeach--}}
{{--                        <div class="form-group">--}}
{{--                            <label>Representative</label>--}}
{{--                            <input type="file" name="file" multiple required class="form-control">--}}
{{--                        </div>--}}
                        <div class="row">
                            <div class="col-12">
                                <button type="submit" id="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.card-body -->
                {{--                    <div class="card-footer">--}}
                {{--                        .--}}
                {{--                    </div>--}}
            </div>
            <!-- /.card -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->


    @endrole
@stop
@push('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $("form").on("submit", function(event){

            event.preventDefault();

            let formData = new FormData(this);
            var formValues= $(this).serialize();

            console.log(formData);
            $.ajax({
                url: '{{ route('store_order_entry') }}',
                type: 'POST',
                async: true,
                cache: false,
                data: formData,
                contentType: false,
                processData: false,
                success: function (response) {
                    printMsg(response);
                },
                error: function () {
                    $('.status').html('حصل خطأ أثناء الاتصال بالانترنت');
                },
            })
        });
        function printMsg (msg) {
            if($.isEmptyObject(msg.error)){
                console.log(msg.success);
                toastr.success(" Your work has been saved", "Success");
                // $('input').val('') ;
            }else{
                $.each( msg.error, function( key, value ) {
                    toastr.error(value, "Danger");
                });
            }
        }
    </script>
@endpush
