{{--@auth()--}}
{{--    @if(auth()->user()->hasRole('super_admin') != null )--}}
@extends('new_layout.app')

@section('title', 'View Order')
{{--@section('page_title','Create New Order')--}}
@push('style')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

    <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
@endpush
@section('content')

    @role('admin|super_admin|store|operation|manager')

    <!-- /.content -->
    <div class="col-12">


        <!-- /.card -->

        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Order for {{ $order->dealer_name }} </h3>

            </div>
            <!-- card-header -->
            <div class="card-body">
                   @if($order->is_active == null)
                    <a class="btn btn-success pull-right" href="{{ route('apply_order',['id'=>$order->id]) }}" >
                        Apply Order
                    </a>

                    @endif
                    <br>
                    @if($order->price != $order->packages->whereNull('price_cancel')->sum('total_price')

)
                            Prices do not match :
                            <a class="btn btn-warning pull-left" href="{{ route('sent_alert_to_res',['id'=>$order->id]) }}" >
                                Send Alert To Representative
                            </a>
                            @elseif( ($order->count_box != $order->packages->whereNull('price_cancel')->sum('pieces')))
                      Pieces Count dont match :
                        <a class="btn btn-warning pull-left" href="{{ route('sent_alert_to_res',['id'=>$order->id]) }}" >

                        Send Alert To Representative
                    </a>
                        @else
                        Total Price without receiver = price from dealer = {{ $order->price }}
                        @endif
                       <p></p>
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        {{--                            <th >id</th>--}}
                        <th >Delivery Name</th>
                        <th >Delivery Number</th>
                        <th >Dealer Name</th>
                        <th >Data Entry Name</th>
{{--                        <th >View</th>--}}
                        <th >Code </th>
                        <th >CC</th>
                        <th >price order {{ $order->price }}</th>
{{--                        <th >State</th>--}}
{{--                        <th >Action</th>--}}
                    </tr>
                    </thead>
                    <tbody>
                    @php $price = 0; @endphp
                    @foreach( $order->packages as $index=>$orders)
{{--                                            @dd($orders)--}}
                        <tr class="row_{{ $orders->id }}">
                            <td>{{ $order->representative?$order->representative->name: ''}}</td>
                            <td>{{ $order->representative?$order->representative->mobile: ''}}</td>
                            <td>{{ $order->dealer?$order->dealer->name: ''}}</td>
                            <td>{{ $orders->user?$orders->user->name: ''}}</td>
                            <td>{{ $orders->code}}</td>
                            <td>{{ $orders->cc}}</td>
                            <td>{{ $orders->total_price}}</td>
{{--                            <td>{{ $orders->state_receiver->name}}</td>--}}
{{--                            <td>{{ $orders->state->name}}</td>--}}
{{--                            <td >--}}
{{--                                <a href="#" data-key="{{ $orders->id }}"--}}
{{--                                    class="badge bg-danger delete-record">--}}
{{--                                    Delete--}}
{{--                                </a>--}}
{{--                            </td>--}}

                        </tr>
                        @php $price = $price+ $orders->total_price; @endphp
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th >Delivery Name</th>
                        <th >Delivery Number</th>
                        <th >Dealer Name</th>
                        <th >Data Entry Name</th>
{{--                        <th >View</th>--}}
                        <th >Code </th>
                        <th >CC</th>
{{--                        <th >price order {{ $order->price }}</th>--}}
                        <th >Sum = {{ $price }}</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->

    @endrole
@endsection

@push('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <!-- DataTables  & Plugins -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    {{--    <script src="dist/js/demo.js"></script>--}}
    <!-- Page specific script -->
    <script>
        $(function () {
            $("#example1").DataTable({
                "responsive": true, "lengthChange": false, "autoWidth": false,
                "buttons": ["copy", "csv", "excel", "pdf", "print"]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
            // $('#tbl-pieces').DataTable({
            //     "paging": true,
            //     "lengthChange": true,
            //     "searching": true,
            //     "ordering": true,
            //     "info": true,
            //     "autoWidth": false,
            //     "responsive": true,
            // });
        });
    </script>
    <script>

        $("#list_order").addClass('active');
        $("#list_order").parent().parent().parent().addClass('menu-open');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#barcode').change(function (){
            let id = $(this).val();
            let barcode = $(this).val();

            $.ajax({
                url: '{{ route('result_barcode') }}',
                type: 'POST',
                async: true,
                cache: false,
                data: {
                    'id':id,
                    'barcode':barcode,

                },
                success: function (response) {
                    if (response.status === 200)
                        window.location.href = '/manager/package/'+response.id;
                    else
                        $('.form_barcode').append('Not Found Please Try Again');

                },
                error: function () {
                    $('.status').append('حصل خطأ أثناء الاتصال بالانترنت');
                },
            })
        });
        $("form").on("submit", function(event){

            event.preventDefault();

            let formData = new FormData(this);
            var formValues= $(this).serialize();

            console.log(formData);
            $.ajax({
                url: '{{ route('store_order') }}',
                type: 'POST',
                async: true,
                cache: false,
                data: formData,
                contentType: false,
                processData: false,
                success: function (response) {
                    printMsg(response);

                    console.log(response.info);
                    $('tbody').append('<tr>' +
                         '<td>'+response.info.count_box+'</td>'+
                        '<td>'+response.info.price+'</td>'+
                        '<td>'+response.info.price_deliver+'</td>'+
                        '<td>'+response.info.order_type +'</td>'+
                        '<td> </td>'+
                        '<td>'+response.zone +'</td>'+
                        '<td>'+response.state +'</td>'+
                        // '<td>'+response.info.zone +'</td>'+
                        '<td><a href="#" data-key="'+response.id+'" class="dropdown-item delete-record">'

                        +'</tr>');
                },
                error: function () {
                    $('.status').append('حصل خطأ أثناء الاتصال بالانترنت');
                },
            })
        });
        function printMsg (msg) {
            if($.isEmptyObject(msg.error)){
                console.log(msg.success);
                toastr.success(" Your work has been saved", "Success");
                $('input').val('') ;
            }else{
                $.each( msg.error, function( key, value ) {
                    $('.status').text(value).css('color','red');
                });
            }
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('.delete-record').click(function (e) {
            e.preventDefault();
            let id = $(this).data("key");
            console.log(id);
            $.ajax({
                url: '{{ route('sdelete_order') }}',
                type: 'POST',
                async: true,
                cache: false,
                data: {
                    'id':id
                },
                success: function (response) {
                    toastr.success(" Your work has been saved", "Success");
                    $('.row_'+id).hide();

                },
                error: function () {
                    toastr.error(" Your work has been not saved", "Error");
                },
            })
        });
    </script>
@endpush

{{--@else--}}
{{--@endif--}}
{{--@endauth--}}
