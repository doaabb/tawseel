@extends('new_layout.app')

@section('title', 'Orders')
{{--@section('page_title','List Delivered Order')--}}


@push('style')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

    <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
@endpush
@section('content')


    @role('admin|super_admin|manager|store')

    <!-- Complex Headers -->


    <div class="col-12">

        <!-- /.card -->

        <div class="card">
            <div class="card-header">
                <h3 class="card-title"></h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">
                <div class="col-md-4">
                    <!-- Widget: user widget style 2 -->
                    <div class="card card-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header">
                            <div class="widget-user-image text-center">
                                <img class="img-circle elevation-2  rounded-circle media-bordered mb-2"
                                     src="{{ $info->image_path ?? '/dist/img/user6-128x128.jpg' }}" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">{{ $info->name }}</h3>
                            <h3 class="widget-user-username">{{ $info->email }}</h3>
                            <h5 class="widget-user-desc">{{ $info->address }}</h5>
                        </div>
                        <div class="card-footer p-0">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <p  class="nav-link">
                                        State: <span class="float-right badge  badge-light-primary mr-1">{{ $info->states?$info->states->name:'' }}</span>
                                    </p>
                                </li>
                                <li class="nav-item">
                                    <p class="nav-link">
                                        Governorate: <span class="float-right badge  badge-light-info mr-1">{{ $info->governorates?$info->governorates->name:'' }}</span>
                                    </p>
                                </li>
                                <li class="nav-item">
                                    <p class="nav-link">
                                       Zone: {{ $info->zones?'<span class="float-right badge badge-light-success mr-1 ">'.$info->zones->name.'</span>':'' }}
                                    </p>
                                </li>
                                <li class="nav-item">
                                    <p   class="nav-link">
                                        Mobile: <span class="float-right badge badge-light-danger mr-1 ">
                                            {{ $info->mobile }}
                                        </span>
                                    </p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- /.widget-user -->
                    <form action="{{ route('result_code_or_barcode') }}" method="post" >
                        @csrf
                        <div class="form-group">
                            <label >
                                Search with barcode Or code
                            </label>
                            <input name="barcode" class="form-control" type="text">
                        </div>
                        <div class="form-group">
                            <input type="submit" />
                        </div>
                    </form>
                </div>

                    <div class="col-md-8">
                       <table id="tbl-pieces"  class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th >CC</th>
                            <th >Status</th>
                            <th >View</th>
                            <th >Code</th>
                            <th >Pieces</th>
                            <th >Price</th>
                            <th >Price Deliver</th>
                            <th >Price Cancel</th>
                            <th >Driver</th>
                            <th >Delivery name</th>
                            <th >Delivery mobile</th>
                            <th >Price from customer</th>
                            <th >State Shipper</th>
                            <th >State Receiver</th>
                            <th >Date Deliver</th>
                            <th >Total Price From Dealer</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach( $orders as $index=>$order)
                        @foreach($order->packages->whereNull('price_cancel')->whereNull('package_status') as $key=>$package)

{{--                            @var_dump()--}}
                            <tr class="row_{{ $order->id }}">
                                <td>{{ $package->cc}} </td>
                                <td id="status_{{$package->id}}">

                                    @if( $package->package_status ===  null)

                                        <span>
                                    <a  href="#" data-key="{{ $package->id }}"
                                        class=" badge badge-success badge-light-success mr-1 active-record"> موافقة عليه؟</a>
                                </span>
                                    @else
                                        تم

                                    @endif
                                </td>
                                <td><a class=" badge badge-info badge-light-info mr-1" href="{{ route('package_manager',['id'=>$package->id])}}">view</a> </td>

                                <td>{{ $package->code}}</td>
                                <td>{{ $package->pieces}}</td>
                                <td>{{ $package->total_price}}</td>
                                <td>{{ $package->price_deliver}}</td>
                                <td>{{ $package->price_cancel}}</td>
                                <td>{{ $package->driver_name?$package->driver_name->mobile:''}}</td>
                                <td>{{ $order->representative?$order->representative->name:''}}</td>
                                <td>{{ $order->representative?$order->representative->mobile:''}}</td>
                                <td>{{ $package->price_customer}}</td>
                                <td>{{ $package->state_shipper? $package->state_shipper->name : ''}}</td>
                                <td>{{ $package->state_receiver? $package->state_receiver->name : ''}}</td>
                                <td>{{ date('Y-m-d H:m:s', strtotime($package->created_at))}}</td>
                                <td>{{ $order->price }}</td>

                            </tr>
                        @endforeach

                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th >CC</th>
                            <th >status</th>
                            <th >view</th>
                            <th >Code</th>
                            <th >Pieces</th>
                            <th >Price</th>
                            <th >Price Deliver</th>
                            <th >Price Cancel</th>
                            <th >Driver</th>
                            <th >Delivery name</th>
                            <th >Delivery mobile</th>
                            <th >Price from customer</th>
                            <th >State Shipper</th>
                            <th >State Receiver</th>
                            <th >Date Deliver</th>
                            <th >Total Price From Dealer</th>
                        </tr>
                        </tfoot>
                    </table>
            </div></div> </div>

        <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->


    @endrole
@stop
@push('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <!-- DataTables  & Plugins -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    {{--    <script src="dist/js/demo.js"></script>--}}
    <!-- Page specific script -->

    <script>
        $(function () {
            // $("#example1").DataTable({
            //     "responsive": true, "lengthChange": false, "autoWidth": false,
            //     "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
            // }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
            // $('table').DataTable();

            $('table.table').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });

        });
    </script>
    <script>

        $("#dealer_user").addClass('active');
        $("#dealer_user").parent().parent().parent().addClass('menu-open');
    </script>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $("form").on("submit", function(event){

            event.preventDefault();

            let formData = new FormData(this);
            var formValues= $(this).serialize();

            console.log(formData);
            $.ajax({
                url: '{{ route('result_code_or_barcode') }}',
                type: 'POST',
                async: true,
                cache: false,
                data: formData,
                contentType: false,
                processData: false,
                success: function (response) {
                    // printMsg(response);
                    toastr.success(" Your work has been saved", "Success");

                    if(response.status === 200){
                        $('#status_'+response.id).html('تم');
                        $('.row_'+response.id).hide(2000);
                        {{--window.location.replace("{{ route('my_order') }}");--}}
                    }
                },
                error: function () {
                    toastr.error('Error Connection', "Danger");
                },
            })
        });

        $('.active-record').click(function (e) {
            e.preventDefault();
            let id = $(this).data("key");
            console.log(id);
            $.ajax({
                url: '{{ route('check_package') }}',
                type: 'POST',
                async: true,
                cache: false,
                data: {
                    'id':id
                },
                success: function (response) {
                    toastr.success(" Your work has been saved", "Success");

                    if(response.status_code === 200){
                        $('#status_'+id).html('تم');
                        {{--window.location.replace("{{ route('my_order') }}");--}}
                    }
                },
                error: function () {
                    toastr.error(" Your work has been not saved", "Error");
                },
            })
        });
        function printMsg (msg) {
            if($.isEmptyObject(msg.error)){
                console.log(msg.success);
                toastr.success(" Your work has been saved", "Success");
                $('input').val('') ;
            }else{
                $.each( msg.error, function( key, value ) {
                    $('.status').text(value).css('color','red');
                });
            }
        }
    </script>
@endpush
