<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
{{--    <meta name="description" content="Vuexy admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">--}}
{{--    <meta name="keywords" content="admin template, Vuexy admin template, dashboard template, flat admin template, responsive admin template, web app">--}}
    <meta name="author" content="PIXINVENT">
    <title>Tawssel @yield('title')</title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
{{--    <link rel="apple-touch-icon" href="../../../app-assets/images/ico/apple-icon-120.png">--}}
{{--    <link rel="shortcut icon" type="image/x-icon" href="../../../app-assets/images/ico/favicon.ico">--}}
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/charts/apexcharts.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/bootstrap-extended.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/colors.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/components.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/themes/dark-layout.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/themes/bordered-layout.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/themes/semi-dark-layout.css') }}">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/menu/menu-types/vertical-menu.css') }}">
{{--    <link rel="stylesheet" type="text/css" href="../../../app-assets/css/pages/dashboard-ecommerce.css">--}}
{{--    <link rel="stylesheet" type="text/css" href="../../../app-assets/css/plugins/charts/chart-apex.css">--}}
{{--    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-toastr.css') }}">--}}
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style.css') }}">
    <!-- END: Custom CSS-->
    @stack('style')
    <style>
        button.buttons-html5 {
            padding: 0.786rem 0.235rem !important;
        }
    </style>
</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern  navbar-floating footer-static  " data-open="click" data-menu="vertical-menu-modern" data-col="">

<!-- BEGIN: Header-->
<nav class="header-navbar navbar navbar-expand-lg align-items-center floating-nav navbar-light navbar-shadow container-xxl">
    <div class="navbar-container d-flex content">
{{--        <div class="bookmark-wrapper d-flex align-items-center">--}}
{{--            <ul class="nav navbar-nav d-xl-none">--}}
{{--                <li class="nav-item"><a class="nav-link menu-toggle" href="javascript:void(0);"><i class="ficon" data-feather="menu"></i></a></li>--}}
{{--            </ul>--}}
{{--            <ul class="nav navbar-nav bookmark-icons">--}}
{{--                <li class="nav-item d-none d-lg-block"><a class="nav-link" href="app-email.html" data-toggle="tooltip" data-placement="top" title="Email"><i class="ficon" data-feather="mail"></i></a></li>--}}
{{--                <li class="nav-item d-none d-lg-block"><a class="nav-link" href="app-chat.html" data-toggle="tooltip" data-placement="top" title="Chat"><i class="ficon" data-feather="message-square"></i></a></li>--}}
{{--                <li class="nav-item d-none d-lg-block"><a class="nav-link" href="app-calendar.html" data-toggle="tooltip" data-placement="top" title="Calendar"><i class="ficon" data-feather="calendar"></i></a></li>--}}
{{--                <li class="nav-item d-none d-lg-block"><a class="nav-link" href="app-todo.html" data-toggle="tooltip" data-placement="top" title="Todo"><i class="ficon" data-feather="check-square"></i></a></li>--}}
{{--            </ul>--}}
{{--            <ul class="nav navbar-nav">--}}
{{--                <li class="nav-item d-none d-lg-block"><a class="nav-link bookmark-star"><i class="ficon text-warning" data-feather="star"></i></a>--}}
{{--                    <div class="bookmark-input search-input">--}}
{{--                        <div class="bookmark-input-icon"><i data-feather="search"></i></div>--}}
{{--                        <input class="form-control input" type="text" placeholder="Bookmark" tabindex="0" data-search="search">--}}
{{--                        <ul class="search-list search-list-bookmark"></ul>--}}
{{--                    </div>--}}
{{--                </li>--}}
{{--            </ul>--}}
{{--        </div>--}}
        <ul class="nav navbar-nav align-items-center ml-auto">
            <li class="nav-item dropdown dropdown-language"><a class="nav-link dropdown-toggle" id="dropdown-flag" href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="flag-icon flag-icon-us"></i><span class="selected-language">English</span></a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown-flag"><a class="dropdown-item" href="javascript:void(0);" data-language="en"><i class="flag-icon flag-icon-us"></i> English</a><a class="dropdown-item" href="javascript:void(0);" data-language="fr"><i class="flag-icon flag-icon-fr"></i> French</a><a class="dropdown-item" href="javascript:void(0);" data-language="de"><i class="flag-icon flag-icon-de"></i> German</a><a class="dropdown-item" href="javascript:void(0);" data-language="pt"><i class="flag-icon flag-icon-pt"></i> Portuguese</a></div>
            </li>
            <li class="nav-item d-none d-lg-block"><a class="nav-link nav-link-style"><i class="ficon" data-feather="moon"></i></a></li>
{{--            <li class="nav-item nav-search"><a class="nav-link nav-link-search"><i class="ficon" data-feather="search"></i></a>--}}
{{--                <div class="search-input">--}}
{{--                    <div class="search-input-icon"><i data-feather="search"></i></div>--}}
{{--                    <input class="form-control input" type="text" placeholder="Explore Vuexy..." tabindex="-1" data-search="search">--}}
{{--                    <div class="search-input-close"><i data-feather="x"></i></div>--}}
{{--                    <ul class="search-list search-list-main"></ul>--}}
{{--                </div>--}}
{{--            </li>--}}
{{--            <li class="nav-item dropdown dropdown-cart mr-25"><a class="nav-link" href="javascript:void(0);" data-toggle="dropdown"><i class="ficon" data-feather="shopping-cart"></i><span class="badge badge-pill badge-primary badge-up cart-item-count">6</span></a>--}}
{{--                <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">--}}
{{--                    <li class="dropdown-menu-header">--}}
{{--                        <div class="dropdown-header d-flex">--}}
{{--                            <h4 class="notification-title mb-0 mr-auto">My Cart</h4>--}}
{{--                            <div class="badge badge-pill badge-light-primary">4 Items</div>--}}
{{--                        </div>--}}
{{--                    </li>--}}
{{--                    <li class="scrollable-container media-list">--}}
{{--                        <div class="media align-items-center"><img class="d-block rounded mr-1" src="../../../app-assets/images/pages/eCommerce/1.png" alt="donuts" width="62">--}}
{{--                            <div class="media-body"><i class="ficon cart-item-remove" data-feather="x"></i>--}}
{{--                                <div class="media-heading">--}}
{{--                                    <h6 class="cart-item-title"><a class="text-body" href="app-ecommerce-details.html"> Apple watch 5</a></h6><small class="cart-item-by">By Apple</small>--}}
{{--                                </div>--}}
{{--                                <div class="cart-item-qty">--}}
{{--                                    <div class="input-group">--}}
{{--                                        <input class="touchspin-cart" type="number" value="1">--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <h5 class="cart-item-price">$374.90</h5>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="media align-items-center"><img class="d-block rounded mr-1" src="../../../app-assets/images/pages/eCommerce/7.png" alt="donuts" width="62">--}}
{{--                            <div class="media-body"><i class="ficon cart-item-remove" data-feather="x"></i>--}}
{{--                                <div class="media-heading">--}}
{{--                                    <h6 class="cart-item-title"><a class="text-body" href="app-ecommerce-details.html"> Google Home Mini</a></h6><small class="cart-item-by">By Google</small>--}}
{{--                                </div>--}}
{{--                                <div class="cart-item-qty">--}}
{{--                                    <div class="input-group">--}}
{{--                                        <input class="touchspin-cart" type="number" value="3">--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <h5 class="cart-item-price">$129.40</h5>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="media align-items-center"><img class="d-block rounded mr-1" src="../../../app-assets/images/pages/eCommerce/2.png" alt="donuts" width="62">--}}
{{--                            <div class="media-body"><i class="ficon cart-item-remove" data-feather="x"></i>--}}
{{--                                <div class="media-heading">--}}
{{--                                    <h6 class="cart-item-title"><a class="text-body" href="app-ecommerce-details.html"> iPhone 11 Pro</a></h6><small class="cart-item-by">By Apple</small>--}}
{{--                                </div>--}}
{{--                                <div class="cart-item-qty">--}}
{{--                                    <div class="input-group">--}}
{{--                                        <input class="touchspin-cart" type="number" value="2">--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <h5 class="cart-item-price">$699.00</h5>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="media align-items-center"><img class="d-block rounded mr-1" src="../../../app-assets/images/pages/eCommerce/3.png" alt="donuts" width="62">--}}
{{--                            <div class="media-body"><i class="ficon cart-item-remove" data-feather="x"></i>--}}
{{--                                <div class="media-heading">--}}
{{--                                    <h6 class="cart-item-title"><a class="text-body" href="app-ecommerce-details.html"> iMac Pro</a></h6><small class="cart-item-by">By Apple</small>--}}
{{--                                </div>--}}
{{--                                <div class="cart-item-qty">--}}
{{--                                    <div class="input-group">--}}
{{--                                        <input class="touchspin-cart" type="number" value="1">--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <h5 class="cart-item-price">$4,999.00</h5>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="media align-items-center"><img class="d-block rounded mr-1" src="../../../app-assets/images/pages/eCommerce/5.png" alt="donuts" width="62">--}}
{{--                            <div class="media-body"><i class="ficon cart-item-remove" data-feather="x"></i>--}}
{{--                                <div class="media-heading">--}}
{{--                                    <h6 class="cart-item-title"><a class="text-body" href="app-ecommerce-details.html"> MacBook Pro</a></h6><small class="cart-item-by">By Apple</small>--}}
{{--                                </div>--}}
{{--                                <div class="cart-item-qty">--}}
{{--                                    <div class="input-group">--}}
{{--                                        <input class="touchspin-cart" type="number" value="1">--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <h5 class="cart-item-price">$2,999.00</h5>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </li>--}}
{{--                    <li class="dropdown-menu-footer">--}}
{{--                        <div class="d-flex justify-content-between mb-1">--}}
{{--                            <h6 class="font-weight-bolder mb-0">Total:</h6>--}}
{{--                            <h6 class="text-primary font-weight-bolder mb-0">$10,999.00</h6>--}}
{{--                        </div><a class="btn btn-primary btn-block" href="app-ecommerce-checkout.html">Checkout</a>--}}
{{--                    </li>--}}
{{--                </ul>--}}
{{--            </li>--}}

            @role('admin|super_admin')
            <li class="nav-item dropdown dropdown-notification mr-25">
                <a class="nav-link" href="javascript:void(0);" data-toggle="dropdown">
                    <i class="ficon" data-feather="bell"></i><span class="badge badge-pill badge-danger badge-up"> {{ count(\App\Models\Order::whereNull('representative_id')->get()) }}</span></a>
                <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                    <li class="dropdown-menu-header">
                        <div class="dropdown-header d-flex">
                            <h4 class="notification-title mb-0 mr-auto">Notifications</h4>
                            <div class="badge badge-pill badge-light-primary">{{ count(\App\Models\Order::whereNull('representative_id')->get()) }} New</div>
                        </div>
                    </li>
                    <li class="scrollable-container media-list">
                        <a class="d-flex" href="{{ route('admin_orders') }}">
                            <div class="media d-flex align-items-start">

                                <div class="media-body">
                                    <p class="media-heading"> {{ count(\App\Models\Order::whereNull('representative_id')->get()) }} <span class="font-weight-bolder">

new orders                                        </span>
                                    </p><small class="notification-text"> see all
                                    </small>
                                </div>
                            </div>
                        </a>

                    </li>

                </ul>
            </li>
            @endrole
            @role('admin|dealer')
            <li class="nav-item dropdown dropdown-notification mr-25"><a class="nav-link" href="javascript:void(0);" data-toggle="dropdown"><i class="ficon" data-feather="bell"></i><span class="badge badge-pill badge-danger badge-up">5</span></a>
                <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                    <li class="dropdown-menu-header">
                        <div class="dropdown-header d-flex">
                            <h4 class="notification-title mb-0 mr-auto">Notifications</h4>
                            <div class="badge badge-pill badge-light-primary">{{ count(auth()->user()->order) }} New</div>
                        </div>
                    </li>
                    <li class="scrollable-container media-list">
                        <a class="d-flex" href="{{ route('orders') }}">
                            <div class="media d-flex align-items-start">
{{--                                <div class="media-left">--}}
{{--                                    <div class="avatar">--}}
{{--                                        <img src="app-assets/images/portrait/small/avatar-s-15.jpg" alt="avatar" width="32" height="32"></div>--}}
{{--                                </div>--}}
                                <div class="media-body">
                                    <p class="media-heading">
                                        <span class="font-weight-bolder">{{ count(auth()->user()->order) }}
                                        </span>Orders</p><small class="notification-text">
                                        See More ..</small>
                                </div>
                            </div>
                        </a>

                    </li>
                    <li class="scrollable-container media-list">
                        <a class="d-flex" href="{{ route('waiting_orders') }}">
                            <div class="media d-flex align-items-start">
                                <div class="media-body">
                                    <p class="media-heading">
                                        <span class="font-weight-bolder">{{ count(auth()->user()->order->where('is_active','!=',1)
            ->where('is_calculate','!=',1))  }}
                                        </span> In waiting</p><small class="notification-text">
                                        See More ..</small>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="scrollable-container media-list">
                        <a class="d-flex" href="{{ route('deliver_order') }}">
                            <div class="media d-flex align-items-start">
                                <div class="media-body">
                                    <p class="media-heading">
                                        <span class="font-weight-bolder">{{ count(auth()->user()->order->where('is_active','!=',1)
            ->where('is_calculate',1)) }}
                                        </span> Completed</p><small class="notification-text">
                                        See More ..</small>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="scrollable-container media-list">
                        <a class="d-flex" href="{{ route('cancel_orders') }}">
                            <div class="media d-flex align-items-start">
                                <div class="media-body">
                                    <p class="media-heading">
                                        <span class="font-weight-bolder">{{ count(auth()->user()->order->where('is_active','=',1)
            ->where('is_calculate',0)) }}
                                        </span> Cancelled</p><small class="notification-text">
                                        See More ..</small>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="dropdown-menu-footer"><a class="btn btn-primary btn-block" href="{{ route('orders') }}">
                            See All Orders
                        </a></li>
                </ul>
            </li>
            @endrole
            @role('accountant|accountant_dealer')
            <li class="nav-item dropdown dropdown-notification mr-25"><a class="nav-link" href="javascript:void(0);" data-toggle="dropdown"><i class="ficon" data-feather="bell"></i><span class="badge badge-pill badge-danger badge-up">5</span></a>
                <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                    <li class="dropdown-menu-header">
                        <div class="dropdown-header d-flex">
                            <h4 class="notification-title mb-0 mr-auto">Notifications</h4>
                            <div class="badge badge-pill badge-light-primary">{{ count(\App\Models\Order::where('is_active','=',1)->get()) }} New</div>
                        </div>
                    </li>
                    <li class="scrollable-container media-list">
                        <a class="d-flex" href="{{ route('alert_orders') }}">
                            <div class="media d-flex align-items-start">
                                <div class="media-body">
                                    <p class="media-heading">
                                        <span class="font-weight-bolder">
                                            0
                                        </span>Alert</p>
                                    <small class="notification-text"> See all ..</small>
                                </div>
                            </div>
                        </a>

                    </li>
                    <li class="scrollable-container media-list">
                        <a class="d-flex" href="{{ route('alert_orders') }}">
                            <div class="media d-flex align-items-start">
                                <div class="media-body">
                                    <p class="media-heading">
                                        <span class="font-weight-bolder">
                                             {{ count(\App\Models\Order::where('is_calculate','=',1)->get()) }} </span>Completed
                                    </p>
                                    <small class="notification-text"> See all ..</small>
                                </div>
                            </div>
                        </a>

                    </li>
                </ul>
            </li>
            @endrole
            @role('representative|driver|delivery')
            <li class="nav-item dropdown dropdown-notification mr-25">
                <a class="nav-link" href="javascript:void(0);" data-toggle="dropdown">
                    <i class="ficon" data-feather="bell"></i><span class="badge badge-pill badge-danger badge-up">        {{ count(\App\Models\Order::where('representative_id','=',auth()->user()->id)
->where('order_status',4)->get()) }}
                    </span></a>
                <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                    <li class="dropdown-menu-header">
                        <div class="dropdown-header d-flex">
                            <h4 class="notification-title mb-0 mr-auto">Notifications</h4>
                            <div class="badge badge-pill badge-light-primary">
                                {{ count(\App\Models\Order::where('representative_id','=',auth()->user()->id)->where('order_status',4)->get()) }} New</div>
                        </div>
                    </li>
                    <li class="scrollable-container media-list">
                        <a class="d-flex" href="{{ route('orders') }}">
                            <div class="media d-flex align-items-start">
                                <div class="media-body">
                                    <p class="media-heading">
                                        <span class="font-weight-bolder">
                                           {{ count(\App\Models\Order::where('representative_id','=',auth()->user()->id)
->where('order_status',4)->get()) }}
                                        </span>orders
                                    </p>
                                    <small class="notification-text"> see more.</small>
                                </div>
                            </div>
                        </a>

                    </li>
                    <li class="scrollable-container media-list">
                        <a class="d-flex" href="{{ route('order_alert') }}">
                            <div class="media d-flex align-items-start">
                                <div class="media-body">
                                    <p class="media-heading">
                                        <span class="font-weight-bolder">
                                     {{ count(\App\Models\Order::where('representative_id','=',auth()->user()->id)
->where('order_status',4)->get()) }}
                                        </span>Alerts
                                    </p>
                                    <small class="notification-text"> see more.</small>
                                </div>
                            </div>
                        </a>

                    </li>
                </ul>
            </li>
            @endrole
            @role('data_entry')
            @php
                $media = App\Models\Media::where('alert_entry',1)->pluck('model_id');
               $order= App\Models\Order::where('order_status','>=',2)->whereIn('id',$media)
                   ->get();
            @endphp
            <li class="nav-item dropdown dropdown-notification mr-25">
                <a class="nav-link" href="javascript:void(0);" data-toggle="dropdown">
                    <i class="ficon" data-feather="bell"></i><span class="badge badge-pill badge-danger badge-up">    {{ count(App\Models\Order::where('order_status','>=',2)->get()) }}</span></a>
                <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                    <li class="dropdown-menu-header">
                        <div class="dropdown-header d-flex">
                            <h4 class="notification-title mb-0 mr-auto">Notifications</h4>
                            <div class="badge badge-pill badge-light-primary">    {{ count(App\Models\Order::where('order_status','>=',2)->get()) }} New</div>
                        </div>
                    </li>
                    <li class="scrollable-container media-list">
                        <a class="d-flex" href="{{ route('dashboard-entry') }}">
                            <div class="media d-flex align-items-start">

                                <div class="media-body">
                                    <p class="media-heading"><span class="font-weight-bolder">
                                         {{ count(App\Models\Order::where('order_status','>=',2)->get()) }}
                                        </span>
                                        Order
                                    </p>
                                    <small class="notification-text"> See more.</small>
                                </div>
                            </div>
                        </a>

                    </li>
                    <li class="scrollable-container media-list">
                        <a class="d-flex" href="{{ route('alerts-entry') }}">
                            <div class="media d-flex align-items-start">

                                <div class="media-body">
                                    <p class="media-heading"><span class="font-weight-bolder">
                                        {{ count($order) }}
                                        </span>
                                        Alert
                                    </p>
                                    <small class="notification-text"> See more.</small>
                                </div>
                            </div>
                        </a>

                    </li>
                    <li class="scrollable-container media-list">
                        <a class="d-flex" href="{{ route('all_order') }}">
                            <div class="media d-flex align-items-start">

                                <div class="media-body">
                                    <p class="media-heading"><span class="font-weight-bolder">
                                       {{ count(App\Models\Order::where('order_status','>=',2)->get()) }}
                                        </span>
                                        In waiting
                                    </p>
                                    <small class="notification-text"> See more.</small>
                                </div>
                            </div>
                        </a>

                    </li>

                </ul>
            </li>
            @endrole
            <li class="nav-item dropdown dropdown-user">
                <a class="nav-link dropdown-toggle dropdown-user-link" id="dropdown-user" href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <div class="user-nav d-sm-flex d-none">
                        <span class="user-name font-weight-bolder">
                            {{ auth()->user()->name }}
                        </span>
                        <span class="user-status">{{ auth()->user()->roles->first()->display_name }}</span>
                    </div><span class="avatar"><img class="round" src="{{ auth()->user()->image_path ?? asset('app-assets/images/portrait/small/avatar-s-11.jpg') }}" alt="avatar" height="40" width="40"><span class="avatar-status-online"></span></span>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown-user">
                    <a class="dropdown-item" href="{{ route('profile') }}"><i class="mr-50" data-feather="user"></i> Profile</a>

                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        <i class="mr-50" data-feather="power"></i> Logout
                    </a>


                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                </div>
            </li>
        </ul>
    </div>
</nav>
{{--<ul class="main-search-list-defaultlist d-none">--}}
{{--    <li class="d-flex align-items-center"><a href="javascript:void(0);">--}}
{{--            <h6 class="section-label mt-75 mb-0">Files</h6>--}}
{{--        </a></li>--}}
{{--    <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between w-100" href="app-file-manager.html">--}}
{{--            <div class="d-flex">--}}
{{--                <div class="mr-75"><img src="../../../app-assets/images/icons/xls.png" alt="png" height="32"></div>--}}
{{--                <div class="search-data">--}}
{{--                    <p class="search-data-title mb-0">Two new item submitted</p><small class="text-muted">Marketing Manager</small>--}}
{{--                </div>--}}
{{--            </div><small class="search-data-size mr-50 text-muted">&apos;17kb</small>--}}
{{--        </a></li>--}}
{{--    <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between w-100" href="app-file-manager.html">--}}
{{--            <div class="d-flex">--}}
{{--                <div class="mr-75"><img src="../../../app-assets/images/icons/jpg.png" alt="png" height="32"></div>--}}
{{--                <div class="search-data">--}}
{{--                    <p class="search-data-title mb-0">52 JPG file Generated</p><small class="text-muted">FontEnd Developer</small>--}}
{{--                </div>--}}
{{--            </div><small class="search-data-size mr-50 text-muted">&apos;11kb</small>--}}
{{--        </a></li>--}}
{{--    <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between w-100" href="app-file-manager.html">--}}
{{--            <div class="d-flex">--}}
{{--                <div class="mr-75"><img src="../../../app-assets/images/icons/pdf.png" alt="png" height="32"></div>--}}
{{--                <div class="search-data">--}}
{{--                    <p class="search-data-title mb-0">25 PDF File Uploaded</p><small class="text-muted">Digital Marketing Manager</small>--}}
{{--                </div>--}}
{{--            </div><small class="search-data-size mr-50 text-muted">&apos;150kb</small>--}}
{{--        </a></li>--}}
{{--    <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between w-100" href="app-file-manager.html">--}}
{{--            <div class="d-flex">--}}
{{--                <div class="mr-75"><img src="../../../app-assets/images/icons/doc.png" alt="png" height="32"></div>--}}
{{--                <div class="search-data">--}}
{{--                    <p class="search-data-title mb-0">Anna_Strong.doc</p><small class="text-muted">Web Designer</small>--}}
{{--                </div>--}}
{{--            </div><small class="search-data-size mr-50 text-muted">&apos;256kb</small>--}}
{{--        </a></li>--}}
{{--    <li class="d-flex align-items-center"><a href="javascript:void(0);">--}}
{{--            <h6 class="section-label mt-75 mb-0">Members</h6>--}}
{{--        </a></li>--}}
{{--    <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between py-50 w-100" href="app-user-view.html">--}}
{{--            <div class="d-flex align-items-center">--}}
{{--                <div class="avatar mr-75"><img src="../../../app-assets/images/portrait/small/avatar-s-8.jpg" alt="png" height="32"></div>--}}
{{--                <div class="search-data">--}}
{{--                    <p class="search-data-title mb-0">John Doe</p><small class="text-muted">UI designer</small>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </a></li>--}}
{{--    <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between py-50 w-100" href="app-user-view.html">--}}
{{--            <div class="d-flex align-items-center">--}}
{{--                <div class="avatar mr-75"><img src="../../../app-assets/images/portrait/small/avatar-s-1.jpg" alt="png" height="32"></div>--}}
{{--                <div class="search-data">--}}
{{--                    <p class="search-data-title mb-0">Michal Clark</p><small class="text-muted">FontEnd Developer</small>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </a></li>--}}
{{--    <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between py-50 w-100" href="app-user-view.html">--}}
{{--            <div class="d-flex align-items-center">--}}
{{--                <div class="avatar mr-75"><img src="../../../app-assets/images/portrait/small/avatar-s-14.jpg" alt="png" height="32"></div>--}}
{{--                <div class="search-data">--}}
{{--                    <p class="search-data-title mb-0">Milena Gibson</p><small class="text-muted">Digital Marketing Manager</small>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </a></li>--}}
{{--    <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between py-50 w-100" href="app-user-view.html">--}}
{{--            <div class="d-flex align-items-center">--}}
{{--                <div class="avatar mr-75"><img src="../../../app-assets/images/portrait/small/avatar-s-6.jpg" alt="png" height="32"></div>--}}
{{--                <div class="search-data">--}}
{{--                    <p class="search-data-title mb-0">Anna Strong</p><small class="text-muted">Web Designer</small>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </a></li>--}}
{{--</ul>--}}
{{--<ul class="main-search-list-defaultlist-other-list d-none">--}}
{{--    <li class="auto-suggestion justify-content-between"><a class="d-flex align-items-center justify-content-between w-100 py-50">--}}
{{--            <div class="d-flex justify-content-start"><span class="mr-75" data-feather="alert-circle"></span><span>No results found.</span></div>--}}
{{--        </a></li>--}}
{{--</ul>--}}
<!-- END: Header-->


<!-- BEGIN: Main Menu-->
@include('new_layout.component.menu')
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper container-xxl p-0">
        <div class="content-header row">
        </div>
<!-- BEGIN: Content-->
@yield('content')
    </div>
</div>
</div>
<!-- END: Content-->

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

<!-- BEGIN: Footer-->
<footer class="footer footer-static footer-light">
    <p class="clearfix mb-0"><span class="float-md-left d-block d-md-inline-block mt-25">
            COPYRIGHT &copy; 2021<a class="ml-25" href="#" target="_blank">

            </a><span class="d-none d-sm-inline-block">, All rights Reserved</span></span>
        <span class="float-md-right d-none d-md-block">
            <i data-feather="heart"></i></span></p>
</footer>
<button class="btn btn-primary btn-icon scroll-top" type="button"><i data-feather="arrow-up"></i></button>
<!-- END: Footer-->


<!-- BEGIN: Vendor JS-->
<script src="{{ asset('app-assets/vendors/js/vendors.min.js') }}"></script>
<!-- BEGIN Vendor JS-->

<!-- BEGIN: Page Vendor JS-->
<script src="{{ asset('app-assets/vendors/js/charts/apexcharts.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<!-- END: Page Vendor JS-->

<!-- BEGIN: Theme JS-->
<script src="{{ asset('app-assets/js/core/app-menu.js') }}"></script>
<script src="{{ asset('app-assets/js/core/app.js') }}"></script>
<!-- END: Theme JS-->

<!-- BEGIN: Page JS-->
{{--<script src="{{ asset('app-assets/js/scripts/pages/dashboard-ecommerce.js') }}"></script>--}}
<!-- END: Page JS-->

<script>
    $(window).on('load', function() {
        if (feather) {
            feather.replace({
                width: 14,
                height: 14
            });
        }
    })
</script>
@stack('script')
</body>
<!-- END: Body-->

</html>
