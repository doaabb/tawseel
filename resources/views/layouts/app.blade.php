<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html  lang="{{ str_replace('_', '-', app()->getLocale()) }}>
@include('layouts.component.head')
@stack('style')
    <body class="hold-transition sidebar-mini ">
@auth()
<div class="wrapper">


@include('layouts.component.navbar')
@include('layouts.component.aside-left')
<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        @include('layouts.component.content-header')
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">

                    @yield('content')
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Control Sidebar -->
@include('layouts.component.aside-right')
<!-- /.control-sidebar -->

    <!-- Main Footer -->
    @include('layouts.component.footer')
    @include('layouts.component.script')
</div>
<!-- ./wrapper -->

@stack('script')
@endauth
<!-- REQUIRED SCRIPTS -->
{{--@stack('script')--}}
{{--<script>--}}
{{--    var timeSinceLastMove = 0;--}}

{{--    $(document).mousemove(function() {--}}

{{--        timeSinceLastMove = 0;--}}
{{--    });--}}

{{--    $(document).keyup(function() {--}}

{{--        timeSinceLastMove = 0;--}}
{{--    });--}}

{{--    checkTime();--}}

{{--    function checkTime() {--}}

{{--        timeSinceLastMove++;--}}

{{--        if (timeSinceLastMove > 10 * 60) {--}}

{{--            {{ session_destroy() }}--}}
{{--            window.location = "{{ route('home') }}";--}}
{{--        }--}}

{{--        setTimeout(checkTime, 1000);--}}
{{--    }--}}
{{--    session_set_cookie_params(3600)--}}
{{--</script>--}}

</body>
</html>
