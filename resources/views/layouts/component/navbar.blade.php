<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
{{--        <li class="nav-item d-none d-sm-inline-block">--}}
{{--            <a href="index3.html" class="nav-link">Home</a>--}}
{{--        </li>--}}
{{--        <li class="nav-item d-none d-sm-inline-block">--}}
{{--            <a href="#" class="nav-link">Contact</a>--}}
{{--        </li>--}}
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        <!-- Navbar Search -->
{{--        <li class="nav-item">--}}
{{--            <a class="nav-link" data-widget="navbar-search" href="#" role="button">--}}
{{--                <i class="fas fa-search"></i>--}}
{{--            </a>--}}
{{--            <div class="navbar-search-block">--}}
{{--                <form class="form-inline">--}}
{{--                    <div class="input-group input-group-sm">--}}
{{--                        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">--}}
{{--                        <div class="input-group-append">--}}
{{--                            <button class="btn btn-navbar" type="submit">--}}
{{--                                <i class="fas fa-search"></i>--}}
{{--                            </button>--}}
{{--                            <button class="btn btn-navbar" type="button" data-widget="navbar-search">--}}
{{--                                <i class="fas fa-times"></i>--}}
{{--                            </button>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </form>--}}
{{--            </div>--}}
{{--        </li>--}}

        <!-- Messages Dropdown Menu -->
{{--        <li class="nav-item dropdown">--}}
{{--            <a class="nav-link" data-toggle="dropdown" href="#">--}}
{{--                <i class="far fa-comments"></i>--}}
{{--                <span class="badge badge-danger navbar-badge">3</span>--}}
{{--            </a>--}}
{{--            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">--}}
{{--                <a href="#" class="dropdown-item">--}}
{{--                    <!-- Message Start -->--}}
{{--                    <div class="media">--}}
{{--                        <img src="{{ asset('dist/img/user1-128x128.jpg') }}" alt="User Avatar" class="img-size-50 mr-3 img-circle">--}}
{{--                        <div class="media-body">--}}
{{--                            <h3 class="dropdown-item-title">--}}
{{--                                Brad Diesel--}}
{{--                                <span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span>--}}
{{--                            </h3>--}}
{{--                            <p class="text-sm">Call me whenever you can...</p>--}}
{{--                            <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <!-- Message End -->--}}
{{--                </a>--}}
{{--                <div class="dropdown-divider"></div>--}}
{{--                <a href="#" class="dropdown-item">--}}
{{--                    <!-- Message Start -->--}}
{{--                    <div class="media">--}}
{{--                        <img src="{{ asset('dist/img/user8-128x128.jpg') }}" alt="User Avatar" class="img-size-50 img-circle mr-3">--}}
{{--                        <div class="media-body">--}}
{{--                            <h3 class="dropdown-item-title">--}}
{{--                                John Pierce--}}
{{--                                <span class="float-right text-sm text-muted"><i class="fas fa-star"></i></span>--}}
{{--                            </h3>--}}
{{--                            <p class="text-sm">I got your message bro</p>--}}
{{--                            <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <!-- Message End -->--}}
{{--                </a>--}}
{{--                <div class="dropdown-divider"></div>--}}
{{--                <a href="#" class="dropdown-item">--}}
{{--                    <!-- Message Start -->--}}
{{--                    <div class="media">--}}
{{--                        <img src="{{ asset('dist/img/user3-128x128.jpg') }}" alt="User Avatar" class="img-size-50 img-circle mr-3">--}}
{{--                        <div class="media-body">--}}
{{--                            <h3 class="dropdown-item-title">--}}
{{--                                Nora Silvester--}}
{{--                                <span class="float-right text-sm text-warning"><i class="fas fa-star"></i></span>--}}
{{--                            </h3>--}}
{{--                            <p class="text-sm">The subject goes here</p>--}}
{{--                            <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <!-- Message End -->--}}
{{--                </a>--}}
{{--                <div class="dropdown-divider"></div>--}}
{{--                <a href="#" class="dropdown-item dropdown-footer">See All Messages</a>--}}
{{--            </div>--}}
{{--        </li>--}}
        <!-- Notifications Dropdown Menu -->
        @role('admin|super_admin')
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#">
                <i class="far fa-bell"></i>
                <span class="badge badge-warning navbar-badge">{{ count(auth()->user()->order) }}</span>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <span class="dropdown-header">15 Notifications</span>
                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item">
                    <i class="fas fa-envelope mr-2"></i> 4 new messages
                    <span class="float-right text-muted text-sm">3 mins</span>
                </a>
                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item">
                    <i class="fas fa-users mr-2"></i> 8 friend requests
                    <span class="float-right text-muted text-sm">12 hours</span>
                </a>
                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item">
                    <i class="fas fa-file mr-2"></i> 3 new reports
                    <span class="float-right text-muted text-sm">2 days</span>
                </a>
                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
            </div>
        </li>
        @endrole
        @role('admin|dealer')
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="{{ route('orders') }}">
                <i class="far fa-bell"></i>
                <span class="badge badge-warning navbar-badge">{{ count(auth()->user()->order) }}</span>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <span class="dropdown-header">{{ count(auth()->user()->order) }} Orders</span>
                <div class="dropdown-divider"></div>
                <a href="{{ route('waiting_orders') }}" class="dropdown-item">
                    <i class="fas fa-stopwatch mr-2"></i>
                    {{ count(auth()->user()->order->where('is_active','!=',1)
            ->where('is_calculate','!=',1)) }} In waiting
{{--                    <span class="float-right text-muted text-sm">3 mins</span>--}}
                </a>
                <div class="dropdown-divider"></div>
                <a href="{{ route('deliver_order') }}" class="dropdown-item">
                    <i class="fas fa-shipping-fast mr-2"></i>
                    {{ count(auth()->user()->order->where('is_active','!=',1)
            ->where('is_calculate',1)) }} Completed
{{--                    <span class="float-right text-muted text-sm">12 hours</span>--}}
                </a>
                <div class="dropdown-divider"></div>
                <a href="{{ route('cancel_orders') }}" class="dropdown-item">
                    <i class="fas fa-times-circle mr-2"></i>
                    {{ count(auth()->user()->order->where('is_active','=',1)
            ->where('is_calculate',0)) }}
                    Cancelled
{{--                    <span class="float-right text-muted text-sm">2 days</span>--}}
                </a>
                <div class="dropdown-divider"></div>
                <a href="{{ route('orders') }}" class="dropdown-item dropdown-footer">See All Orders</a>
            </div>
        </li>
        @endrole
        @role('accountant|accountant_dealer')
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="{{ route('dashboard-account') }}">
                <i class="far fa-bell"></i>
                <span class="badge badge-warning navbar-badge">{{ count(\App\Models\Order::where('is_active','=',1)->get()) }}</span>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <span class="dropdown-header">{{ count(\App\Models\Order::where('is_active','!=',1)->get()) }} Orders</span>
                <div class="dropdown-divider"></div>
                <a href="{{ route('alert_orders') }}" class="dropdown-item">
                    <i class="fas fa-stopwatch mr-2"></i>
                    0 Alert
{{--                    <span class="float-right text-muted text-sm">3 mins</span>--}}
                </a>
                <div class="dropdown-divider"></div>
                <a href="{{ route('delivery') }}" class="dropdown-item">
                    <i class="fas fa-shipping-fast mr-2"></i>
                    {{ count(\App\Models\Order::where('is_calculate','=',1)->get()) }} Completed
{{--                    <span class="float-right text-muted text-sm">12 hours</span>--}}
                </a>
{{--                <div class="dropdown-divider"></div>--}}
{{--                <a href="{{ route('orders') }}" class="dropdown-item dropdown-footer">See All Orders</a>--}}
            </div>
        </li>
        @endrole
        @role('representative|driver|delivery')
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="{{ route('orders') }}">
                <i class="far fa-bell"></i>
                <span class="badge badge-warning navbar-badge">
                    {{ count(\App\Models\Order::where('representative_id','=',auth()->user()->id)
->where('order_status',4)->get()) }}
                </span>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <span class="dropdown-header">
                    {{ count(\App\Models\Order::where('representative_id','=',auth()->user()->id)
->where('order_status',4)
->get()) }} Orders</span>
                <div class="dropdown-divider"></div>

                <a href="{{ route('order_alert') }}" class="dropdown-item">
                    <i class="fas fa-times-circle mr-2"></i>
                    {{ count(\App\Models\Order::where('representative_id','=',auth()->user()->id)
->where('order_status',4)->get()) }}
                    has alerts
{{--                    <span class="float-right text-muted text-sm">2 days</span>--}}
                </a>
            </div>
        </li>
        @endrole

        @role('data_entry')
        @php
            $media = App\Models\Media::where('alert_entry',1)->pluck('model_id');
           $order= App\Models\Order::where('order_status','>=',2)->whereIn('id',$media)
               ->get();
            @endphp
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="{{ route('dashboard-entry') }}">
                <i class="far fa-bell"></i>
                <span class="badge badge-warning navbar-badge">
                    {{ count(App\Models\Order::where('order_status','>=',2)->get()) }}</span>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <span class="dropdown-header">{{ count($order) }} Orders</span>
                <div class="dropdown-divider"></div>
                <a href="{{ route('alerts-entry') }}" class="dropdown-item">
                    <i class="fas fa-stopwatch mr-2"></i>
                    {{ count($order) }} Alert
{{--                    <span class="float-right text-muted text-sm">3 mins</span>--}}
                </a>
                <div class="dropdown-divider"></div>
                <a href="{{ route('all_order') }}" class="dropdown-item">
                    <i class="fas fa-stopwatch mr-2"></i>
                    {{ count(App\Models\Order::where('order_status','>=',2)->get()) }} In waiting
                    {{--                    <span class="float-right text-muted text-sm">3 mins</span>--}}
                </a>
{{--                <div class="dropdown-divider"></div>--}}
{{--                <a href="{{ route('orders') }}" class="dropdown-item dropdown-footer">See All Orders</a>--}}
            </div>
        </li>
        @endrole
        <li class="nav-item">
            <a class="nav-link" data-widget="fullscreen" href="#" role="button">
                <i class="fas fa-expand-arrows-alt"></i>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
                <i class="fas fa-th-large"></i>
            </a>
        </li>
    </ul>
</nav>
<!-- /.navbar -->
