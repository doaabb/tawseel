<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
        <img src="{{ (auth()->user()->image_path == null ?asset('dist/img/AdminLTELogo.png') :  auth()->user()->image_path )}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">{{ auth()->user()->name }}</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
{{--                <img src="{{ (auth()->user()->image_path == null ?asset('dist/img/user2-160x160.jpg') :  auth()->user()->image_path )}}" class="img-circle elevation-2" alt="User Image">--}}
            </div>
            <div class="info">
                <a href="#" class="d-block">{{ auth()->user()->roles->first()->display_name }}</a>
            </div>
        </div>

        <!-- SidebarSearch Form -->
{{--        <div class="form-inline">--}}
{{--            <div class="input-group" data-widget="sidebar-search">--}}
{{--                <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">--}}
{{--                <div class="input-group-append">--}}
{{--                    <button class="btn btn-sidebar">--}}
{{--                        <i class="fas fa-search fa-fw"></i>--}}
{{--                    </button>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                @role('accountant|accountant_dealer')

                <li class="nav-item ">
                    <a href="#" class="nav-link ">
                        <i class="nav-icon fas fa-users"></i>
                        <p>
                            Orders
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('dashboard-account') }}" class="nav-link" id="list_users">
                                <i class="far fa-circle nav-icon"></i>
                                <p>List Users</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('dealer_account') }}" class="nav-link " id="dealer_user">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Dealer</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('all_closed_account') }}" class="nav-link " id="delivery_user">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Closed Order</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('alert_orders') }}" class="nav-link " id="delivery_user">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Alert Order</p>
                            </a>
                        </li>
                    </ul>
                </li>
                @endrole
                @role('admin|super_admin')

                <li class="nav-item">
                    <a href="{{ route('dealer') }}" class="nav-link">
                        <i class="nav-icon fas fa-users"></i>
                        <p>
                            Dealer
                            <span class="right badge badge-success">view</span>
                        </p>
                    </a>
                </li>
                <li class="nav-item ">
                    <a href="#" class="nav-link ">
                        <i class="nav-icon fas fa-users"></i>
                        <p>
                             Users
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('create_user') }}" class="nav-link " id="create_user">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Create User</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('users') }}" class="nav-link" id="list_users">
                                <i class="far fa-circle nav-icon"></i>
                                <p>List User</p>
                            </a>
                        </li>
                    </ul>
                </li>
{{--                 Start Role --}}
                <li class="nav-item ">
                    <a href="#" class="nav-link ">
                        <i class="nav-icon fas fa-star"></i>
                        <p>
                             Role
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('create_role') }}" class="nav-link " id="create_role">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Create Role</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('role') }}" class="nav-link" id="list_role">
                                <i class="far fa-circle nav-icon"></i>
                                <p>List Role</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('res_role') }}" class="nav-link" id="res_role">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Representative Role</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('acc_role') }}" class="nav-link" id="acc_role">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Accountant Role</p>
                            </a>
                        </li>
                    </ul>
                </li>
                {{--                Start Locations--}}
                <li class="nav-item ">
                    <a href="#" class="nav-link ">
                        <i class="nav-icon fas fa-synagogue"></i>
                        <p>
                            Governorate
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('create_gov') }}" class="nav-link " id="create_gov">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Create & List</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('state') }}" class="nav-link" id="list_state">
                                <i class="far fa-circle nav-icon"></i>
                                <p>List State</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('zone') }}" class="nav-link" id="list_zone">
                                <i class="far fa-circle nav-icon"></i>
                                <p>List Zone</p>
                            </a>
                        </li>
                    </ul>
                </li>

                {{-- End Locations --}}
{{-- End Role--}}
{{--                 Start Price--}}
                 <li class="nav-item">
                    <a href="{{ route('prices') }}" class="nav-link">
                        <i class="nav-icon fas fa-money-bill-wave"></i>
                        <p>
                            Prices
                            <span class="right badge badge-warning">view&add</span>
                        </p>
                    </a>
                </li>
{{--                End  Price --}}
{{--                 Start Barcode--}}
                 <li class="nav-item">
                    <a href="{{ route('admin_read_barcode') }}" class="nav-link">
                        <i class="nav-icon fas fa-search"></i>
                        <p>
                            Search With Barcode
{{--                            <span class="right badge badge-warning"></span>--}}
                        </p>
                    </a>
                </li>
{{--                End  Price --}}
{{--                 Start Order Type--}}
                 <li class="nav-item">
                    <a href="{{ route('order_type') }}" class="nav-link">
                        <i class="nav-icon fas fa-tape"></i>
                        <p>
                            Order Type
                            <span class="right badge badge-info">view&add</span>
                        </p>
                    </a>
                </li>
{{--                End Order Type--}}
{{--                 Start Order --}}
                 <li class="nav-item ">
                    <a href="#" class="nav-link ">
                        <i class="nav-icon fas fa-shipping-fast"></i>
                        <p>
                            Orders
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('admin_orders') }}" class="nav-link" id="list_order">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Create && List Order</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('orders_old') }}" class="nav-link" id="orders_old">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Old Orders</p>
                            </a>
                        </li>
                    </ul>
                </li>
{{--                 End Order --}}
                @endrole

                @role('dealer')

                <li class="nav-item ">
                    <a href="#" class="nav-link ">
                        <i class="nav-icon fas-fashipping-fast"></i>
                        <p>
                            Orders
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('create_order') }}" class="nav-link " id="create_order">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Create Order</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('orders') }}" class="nav-link" id="list_order">
                                <i class="far fa-circle nav-icon"></i>
                                <p>List Order</p>
                            </a>
                        </li>
                    </ul>
                </li>

                {{--                 Start statement --}}
                <li class="nav-item">
                    <a href="{{ route('statement_dealer') }}" class="nav-link">
                        <i class="nav-icon fas fa-money-bill-wave"></i>
                        <p>
                            Statement
                            <span class="right badge badge-info">view</span>
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('deliver_order') }}" class="nav-link">
                        <i class="nav-icon fas fa-truck"></i>
                        <p>
                            Delivered Order
                            <span class="right badge badge-success">view</span>
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('waiting_orders') }}" class="nav-link">
                        <i class="nav-icon fas fa-clock"></i>
                        <p>
                            Waiting Orders
                            <span class="right badge badge-primary">view</span>
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('cancel_orders') }}" class="nav-link">
                        <i class="nav-icon fas fa-ban"></i>
                        <p>
                            Canceled Order
                            <span class="right badge badge-danger">view</span>
                        </p>
                    </a>
                </li>
                {{--                End statement --}}
                @endrole
                @role('representative')

                <li class="nav-item ">
                    <a href="#" class="nav-link ">
                        <i class="nav-icon fas-fashipping-fast"></i>
                        <p>
                            Orders
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('my_order') }}" class="nav-link" id="list_order">
                                <i class="far fa-circle nav-icon"></i>
                                <p>My Orders</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('orders_rep') }}" class="nav-link" id="list_order">
                                <i class="far fa-circle nav-icon"></i>
                                <p>New Orders</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('packages_rep') }}" class="nav-link" id="list_order">
                                <i class="far fa-circle nav-icon"></i>
                                <p>List Packages</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">

                    <a href="{{ route('order_alert') }}" class="nav-link">
                        <i class="nav-icon fas fa-ban"></i>
                        <p>
                            Alert Order
                            <span class="right badge badge-danger">view</span>
                        </p>
                    </a>
                </li>
                <li class="nav-item">

                    <a href="{{ route('delivery') }}" class="nav-link">
                        <i class="nav-icon fas fa-ban"></i>
                        <p>
                            Delivery Order
                            <span class="right badge badge-danger">view</span>
                        </p>
                    </a>
                </li>
                @endrole
                @role('manager|operation|store')

                <li class="nav-item">
                    <a href="{{ route('dealer') }}" class="nav-link">
                        <i class="nav-icon fas fa-users"></i>
                        <p>
                            Dealer
                            <span class="right badge badge-info">view</span>
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('search_res') }}" class="nav-link">
                        <i class="nav-icon fas fa-users"></i>
                        <p>
                            Representative
                            <span class="right badge badge-info">view</span>
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('Receipt_from_delivery') }}" class="nav-link">
                        <i class="nav-icon fas fa-shipping-fast"></i>
                        <p>
                            Receipt from delivery
{{--                            <span class="right badge badge-success">search</span>--}}
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('search_with_code') }}" class="nav-link">
                        <i class="nav-icon fas fa-search"></i>
                        <p>
                            Search with code
                        </p>
                    </a>
                </li>
                <li class="nav-item ">
                    <a href="#" class="nav-link ">
                        <i class="nav-icon fas-fashipping-fast"></i>
                        <p>
                            Orders
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('dashboard-manager') }}" class="nav-link" id="list_order">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Orders</p>
                            </a>
                        </li>
                    </ul>
                </li>
                @endrole
                @role('data_entry')

                <li class="nav-item ">
                    <a href="#" class="nav-link ">
                        <i class="nav-icon fas-fashipping-fast"></i>
                        <p>
                            Orders
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('dashboard-entry') }}" class="nav-link" id="list_order">
                                <i class="far fa-circle nav-icon"></i>
                                <p>List New Orders</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('dashboard-entry') }}" class="nav-link" id="all_order">
                                <i class="far fa-circle nav-icon"></i>
                                <p>List all Orders</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">

                    <a href="{{ route('alerts-entry') }}" class="nav-link">
                        <i class="nav-icon fas fa-ban"></i>
                        <p>
                            Alert Order
                            <span class="right badge badge-danger">view</span>
                        </p>
                    </a>
                </li>
                @endrole

                @role('brunch')

                <li class="nav-item ">
                    <a href="#" class="nav-link ">
                        <i class="nav-icon fas-fashipping-fast"></i>
                        <p>
                            Orders
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('dashboard-brunch') }}" class="nav-link" id="list_order">
                                <i class="far fa-circle nav-icon"></i>
                                <p>List Order</p>
                            </a>
                        </li>
                    </ul>
                </li>
                @endrole
                @auth()

                    <li class="nav-item">
                        <a href="{{ route('profile') }}" class="nav-link">
                            <i class="nav-icon fas fa-user-circle"></i>
                            <p>
                                View Profile
{{--                                <span class="right badge badge-info">view&add</span>--}}
                            </p>
                        </a>
                    </li>
                <li class="nav-item">

                    <a class="nav-link" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">

                        <i class="nav-icon fa fa-sign-out-alt sign-out-alt"></i>
                        <p>
                            {{ __('Logout') }}
                        </p>
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                </li>
                @endauth
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
