@extends('new_layout.app')

@section('title', 'Orders')
{{--@section('page_title','List Delivered Order')--}}


@push('style')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

    <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
@endpush
@section('content')


    @role('admin|super_admin|dealer')

    <!-- Complex Headers -->


    <div class="col-12">

        <!-- /.card -->

        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Orders Completed (OPENING)</h3>
            </div>
            <!-- /.card-header -->
            @foreach( $orders as $index=>$order)
                <div class="card-body">
                    <h4>
                        Order Number : {{ $order->id }}
                        @if($order->order_status ==null || $order->order_status == 0)
                            You Can Edit From <a href="{{ route('edit_order', ['id'=>$order->id]) }}">
                                Here
                            </a>
                        @endif
                    </h4>
                    {{--                    <p >Total Price Order: <b>{{ $order->packages->whereNull('price_cancel')->sum('total_price') + $order->packages->whereNull('price_cancel')->sum('price_deliver')  }}</b> OMR--}}
                    {{--                    </p>--}}
                    {{--                    <p >Count Revers Orders: <b>{{ count($order->packages->whereNotNull('price_cancel')) }}</b> Cost is<b>  {{ $order->packages->whereNotNull('price_cancel')->sum('price_cancel') }}</b> OMR--}}

                    {{--                    </p>--}}
                    {{--                    <p >Count Receiver Orders: <b>{{ count($order->packages->whereNull('price_cancel')) }}</b> Cost is<b> {{ $order->packages->whereNull('price_cancel')->sum('total_price') }}</b> OMR</p>--}}
                    {{--                    <p >Count Pieces Of Orders: <b>{{ $order->packages->sum('pieces') }}</b></p>--}}
                    {{--                    <p >Date Last Order:  <b>{{ $order->packages->last()?$order->packages->last()->created_at:'' }}</b></p>--}}
                    <table id="tbl-pieces"  class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th >CC</th>
                            <th >Code</th>
                            <th >Pieces</th>
                            <th >Price</th>
                            <th >Price Deliver</th>
                            <th >Price Cancel</th>
                            <th >Driver</th>
                            <th >Delivery</th>
                            <th >Price from customer</th>
                            <th >State Shipper</th>
                            <th >State Receiver</th>
                            <th >Date Deliver</th>
                            <th >Total Price From Dealer</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php $total_price_deliver = 0; @endphp
                        @php $total_price_cancel = 0; @endphp
                        @php $total_price = 0; @endphp
                        @php $total_price_customer = 0; @endphp
                        @foreach($order->packages->whereNull('price_cancel') as $key=>$package)

                            <tr class="row_{{ $order->id }}">
                                <td>{{ $package->cc}}</td>
                                <td>{{ $package->code}}</td>
                                <td>{{ $package->pieces}}</td>
                                <td>{{ $package->total_price}}</td>
                                <td>{{ $package->price_deliver}}</td>
                                <td>{{ $package->price_cancel}}</td>
                                <td>{{ $package->driver}}</td>
                                <td>{{ $package->delivery}}</td>
                                <td>{{ $package->price_customer}}</td>
                                <td>{{ $package->state_shipper? $package->state_shipper->name : ''}}</td>
                                <td>{{ $package->state_receiver? $package->state_receiver->name : ''}}</td>
                                <td>{{ date('Y-m-d H:m:s', strtotime($package->created_at))}}</td>
                                <td>{{ $order->price }}</td>

                            </tr>
                            @php $total_price_deliver += $package->price_deliver; @endphp
                            @php $total_price_cancel += $package->price_cancel; @endphp
                            @php $total_price += $package->total_price; @endphp
                            @php $total_price_customer += $package->price_customer; @endphp
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th >CC</th>
                            <th >Code</th>
                            <th >Pieces</th>
                            <th >{{ $total_price }}</th>
                            <th >{{ $total_price_deliver }}</th>
                            <th >{{ $total_price_cancel }}</th>
                            <th >Driver</th>
                            <th >Delivery</th>
                            <th >{{ $total_price_customer }}</th>
                            <th >State Shipper</th>
                            <th >State Receiver</th>
                            <th >Date Deliver</th>
                            <th >Total Price From Dealer</th>
                        </tr>
                        </tfoot>
                    </table>

                </div>
        @endforeach
        <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->


    @endrole
@stop
@push('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <!-- DataTables  & Plugins -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    {{--    <script src="dist/js/demo.js"></script>--}}
    <!-- Page specific script -->

    <script>
        $(function () {
            // $("#example1").DataTable({
            //     "responsive": true, "lengthChange": false, "autoWidth": false,
            //     "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
            // }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
            // $('table').DataTable();

            $('table.table').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });

        });
    </script>
    <script>

        $("#dealer_user").addClass('active');
        $("#dealer_user").parent().parent().parent().addClass('menu-open');
    </script>
@endpush
