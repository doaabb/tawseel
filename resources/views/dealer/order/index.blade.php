{{--@auth()--}}
{{--    @if(auth()->user()->hasRole('super_admin') != null )--}}
{{--@extends('new_layout.app')--}}

@section('title', 'Create New Order')
@section('page_title','Create New Order')
@push('style')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

    <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
    <style>
        .hide{
            display:none;
        }
    </style>
@endpush
@section('content')

    @role('admin|super_admin|dealer')
    @php
        date_default_timezone_set('Asia/Muscat');
    @endphp
    <div class="card card-default">
        <div class="card-header">
            <h3 class="card-title">Add Order</h3>

            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                </button>
                <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                </button>
            </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <div class="row">


                <div class="status">

                </div>
                <form class="form-horizontal col-md-12" id="create" METHOD="post" action="#"
                      enctype="multipart/form-data" autocomplete="on"
                >
                    {{ csrf_field()}}

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="form-label" for="basic-addon-count_box">
                                The number of goods
                            </label>

                            <input
                                type="number"
                                id="basic-addon-count_box"
                                class="form-control"
                                placeholder="The number of goods"
                                aria-label="The number of goods"
                                name="count_box"
                                aria-describedby="basic-addon-count_box"
                                required
                            />
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-label" for="basic-addon-count_box">
                                Governorate & State
                            </label>


                            <select class="form-control select2" name="state_id" id="state_id" style="width: 100%;">
                                @foreach($gov as $gover)
                                    <optgroup label="{{ $gover->name }}">
                                        @foreach($gover->state as $state)
                                            <option value="{{ $state->id }}">{{ $state->name }}</option>
                                        @endforeach
                                    </optgroup>

                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-label" for="basic-addon-count_box">
                                Zone
                            </label>


                            <select class="form-control select2" name="zone" id="get_zone" style="width: 100%;">
                                <option value="">اختر ولاية لتظهر لك المناطق التابعة لها هنا</option>
                            </select>
                        </div>

                    </div>

                    <div class="row">

                        <div class="form-group col-md-6">

                            <label  class="form-label">
                                Date
                            </label>
                            <input type="date" value="<?php echo date('Y-m-d'); ?>" id="datepicker" name="date"
                                   class="form-control" >
                        </div>

                        <div class="form-group col-md-6">
                            <label  class="form-label">Period</label>

                            <input type="time" value="<?php echo date('H:i:s'); ?>"  name="time"
                                   class="form-control" >

                        </div>
                        <!-- /.input group -->
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="form-label" for="basic-addon-price">
                                Price
                            </label>

                            <input
                                type="text"
                                id="basic-addon-price"
                                class="form-control"
                                placeholder="Price"
                                aria-label="Price"
                                name="price"
                                aria-describedby="basic-addon-price"
                            />
                        </div>
                        <div class="form-group col-md-6">

                            <label>Order Type</label>
                            <select class="form-control select2" name="order_type_id" style="width: 100%;">
                                <option></option>
                                @foreach($order_type as $type)
                                    <option value="{{ $type->id }}">{{ $type->name }}</option>
                                @endforeach
                            </select>
                        </div>


                    </div>

                    <div class="form-group">
                        <label>Description</label>
                        <textarea
                            class="form-control"
                            aria-label="description"
                            name="description"
                            aria-describedby="basic-addon-description"
                        >
                                </textarea>
                    </div>


                    {{--                                <div class="form-group">--}}
                    {{--                                    <label>Governorate</label>--}}
                    {{--                                    <select class="form-control select2" name="governorate_id" style="width: 100%;">--}}
                    {{--                                        @foreach($gov as $gover)--}}
                    {{--                                        <option value="{{ $gover->id }}">{{ $gover->name }}</option>--}}
                    {{--                                        @endforeach--}}
                    {{--                                    </select>--}}
                    {{--                                </div>--}}


                    {{--                                <div class="form-group">--}}
                    {{--                                    <label>State</label>--}}
                    {{--                                    <select class="form-control select2" name="state_id" style="width: 100%;">--}}
                    {{--                                        @foreach($state as $gover)--}}
                    {{--                                        <option value="{{ $gover->id }}">{{ $gover->name }}</option>--}}
                    {{--                                        @endforeach--}}
                    {{--                                    </select>--}}
                    {{--                                </div>--}}


                    <div class="row">
                        <div class="col-12">
                            <button type="submit" id="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.card-body -->
            {{--                    <div class="card-footer">--}}
            {{--                        .--}}
            {{--                    </div>--}}
        </div>
        <!-- /.card -->
    </div>

    <!-- /.content -->
    <div class="col-12">
        <div class="modal fade" id="modal-lg" role="dialog" >
            <div class="modal-dialog modal-lg  modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Order Status</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        <div class="tab-pane" id="timeline">
                            <!-- The timeline -->
                            <div class="timeline timeline-inverse">
                                <!-- timeline time label -->
                                <div class="time-label hide dealer_date">
                                        <span class="bg-danger">
{{--                                            10 Feb. 2014--}}
                                        </span>
                                </div>
                                <!-- /.timeline-label -->

                                <!-- timeline item -->
                                <div>
                                    <i class="fas fa-envelope bg-primary"></i>

                                    <div class="timeline-item dealer_time">
                                            <span class="time ">
                                                <i class="far fa-clock"></i> </span>

                                        <h3 class="timeline-header">
                                            {{--                                                <a href="#">Support Team</a> --}}
                                            sent </h3>

                                        <div class="timeline-body">
                                            You Sent Order
                                        </div>
                                    </div>
                                </div>
                                <!-- END timeline item -->
                                <!-- timeline item -->
                                <div class="admin_area hide">
                                    <i class="fas fa-user bg-info"></i>

                                    <div class="timeline-item admin_time">
                                            <span class="time"><i class="far fa-clock"></i>
                                                </span>

                                        <h3 class="timeline-header border-0 admin_name">
                                            <a href="#"></a>
                                        </h3>
                                    </div>
                                </div>
                                <!-- END timeline item -->
                                <!-- timeline item -->
                                <div class="res_area hide">
                                    <i class="fas fa-comments bg-warning"></i>

                                    <div class="timeline-item res_time">
                                            <span class="time"><i class="far fa-clock"></i>
                                            </span>

                                        <h3 class="timeline-header res_name">
                                            <a href="#"></a>

                                        </h3>

                                    </div>
                                </div>
                                <!-- END timeline item -->
                                <!-- timeline item -->
                                <div class="data_entry_area hide">
                                    <i class="fas fa-comments bg-primary"></i>

                                    <div class="timeline-item data_entry_time">
                                            <span class="time"><i class="far fa-clock"></i>
                                            </span>

                                        <h3 class="timeline-header data_entry_name">
                                            <a href="#"></a>

                                        </h3>

                                    </div>
                                </div>
                                <!-- END timeline item -->
                                <!-- timeline item -->
                                <div class="manager_area hide">
                                    <i class="fas fa-comments bg-primary"></i>

                                    <div class="timeline-item manager_time">
                                            <span class="time"><i class="far fa-clock"></i>
                                            </span>

                                        <h3 class="timeline-header manager_name">
                                            <a href="#"></a>

                                        </h3>

                                    </div>
                                </div>
                                <!-- END timeline item -->
                                <!-- timeline item -->
                                <div class="representative_area hide">
                                    <i class="fas fa-comments bg-primary"></i>

                                    <div class="timeline-item representative_time">
                                            <span class="time"><i class="far fa-clock"></i>
                                            </span>

                                        <h3 class="timeline-header representative_name">
                                            <a href="#"></a>

                                        </h3>

                                    </div>
                                </div>
                                <!-- END timeline item -->
                                <!-- timeline time label -->
                                <div class="time-label">
                                        <span class="bg-success">
                                        waiting
                                         </span>
                                </div>
                                <!-- /.timeline-label -->
                                {{--                                    <div>--}}
                                {{--                                        <i class="far fa-clock bg-gray"></i>--}}
                                {{--                                    </div>--}}
                            </div>
                        </div>
                        <!-- /.tab-pane -->
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

        <!-- /.card -->

        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Orders </h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        {{--                            <th >id</th>--}}
                        <th >Count Boxes</th>
                        <th >Deliver Name</th>
                        <th >Deliver mobile</th>
                        <th >Price</th>
                        <th >Price Deliver</th>
                        <th >Order Type</th>
                        <th >Order Status</th>
                        <th >Zone</th>
                        <th >State</th>
                        <th >Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach( auth()->user()->order as $index=>$orders)
                        <tr class="row_{{ $orders->id }}">
                            {{--                                <td >{{ $orders->id}}</td>--}}
                            <td>{{ $orders->count_box}}</td>
                            <td>{{ $orders->representative?$orders->representative->name:''}}</td>
                            <td>{{ $orders->representative?$orders->representative->mobile:''}}</td>
                            <td>{{ $orders->price}}</td>
                            <td>{{ $orders->price_deliver}}</td>
                            <td>{{ $orders->order_type}}</td>
                            <td>
                                    <span  data-target="#modal-lg"
                                           data-toggle="modal"
                                           data-key="{{ $orders->id }}"
                                           class="badge bg-info logs">logs</span>
                            </td>
                            <td>{{ $orders->zone?$orders->zone->name:''}}</td>
                            <td>{{ $orders->state->name}}</td>
                            <td>
                                <div class="d-inline-flex">
                                    <a class="pr-1 dropdown-toggle hide-arrow text-primary"
                                       data-toggle="dropdown">

                                        <i data-feather="more-vertical"></i>
                                        <i class="more-vertical"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        {{--                                                <a href="javascript:;" class="dropdown-item">--}}
                                        {{--                                                    <i class="file-text"></i>View Users</a>--}}
                                        {{--                                        <a href="{{ route('edit_role', ['id' => $roles->id]) }}" class="dropdown-item">--}}
                                        {{--                                            Edit</a>--}}
                                        @if($orders->order_status != null ||  $orders->order_status != 1)
                                            <a href="{{ route('edit_order',['id'=>$orders->id]) }}"
                                               class="dropdown-item">

                                                Edit</a>

                                        @endif
                                        <a href="#" data-key="{{ $orders->id }}"  class="dropdown-item delete-record">
                                            Delete
                                        </a>

                                    </div>
                                </div>
                                <a href="javascript:;" class="item-edit">
                                    <i class="icon-wrapper"></i></a>
                            </td>

                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        {{--                            <th >id</th>--}}
                        <th >Count Boxes</th>
                        <th >Deliver Name</th>
                        <th >Deliver mobile</th>
                        <th >Price</th>
                        <th >Price Deliver</th>
                        <th >Order Type</th>
                        <th >Order Status</th>
                        <th >Zone</th>
                        <th >State</th>
                        <th >Action</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->

    @endrole
@endsection

@push('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <!-- DataTables  & Plugins -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    {{--    <script src="dist/js/demo.js"></script>--}}
    <!-- Page specific script -->
    <script>
        $(function () {
            $("#example1").DataTable({
                "responsive": true, "lengthChange": false, "autoWidth": false,
                "buttons": ["copy", "csv", "excel", "pdf", "print"]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
            $('#tbl-pieces').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });
        });
        // $('#timepicker').date(new Date())

    </script>
    <script>

        $('#state_id').change(function (){
            let id = $(this).val();
            console.log(id);

            $.ajax({
                url: '{{ route('get_zone') }}',
                type: 'POST',
                async: true,
                cache: false,
                data: {
                    'id':id
                },

                success: function (response) {
                    if(response.status === 200) {
                        // $('#get_zone').html('<option value="'+response.zone)
                        console.log(response.zone.length);

                        if(response.zone.length === 2){
                            $('#get_zone').html('<option  value="">لا يوجد مناطق تابعة لهذه الولاية </option>');

                        }else{

                            $('#get_zone').html('');
                            jQuery.each(JSON.parse(response.zone), function(i, val) {
                                // $("#" + i).append(document.createTextNode(" - " + val));
                                $('#get_zone').append('<option  value="'+val+'">'+i +'</option>');
                                // console.log(i + val)
                            });

                        }
                    }
                },
                error: function () {
                },
            })

        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('.logs').click(function (){

            let id = $(this).data("key");

            $.ajax({
                url: '{{ route('get_status') }}',
                type: 'POST',
                async: true,
                cache: false,
                data: {
                    'id':id
                },
                success: function (response) {
                    if(response.status === 200){
                        let lenght_id = response.created_date.length;
                        // while (lenght_id > 0){
                        //     $('.timeline-inverse').append(' <div class="admin_area hide"> ' +
                        //         '<i class="fas fa-user bg-info"></i> ' +
                        //         '<div class="timeline-item admin_time"> ' +
                        //         '<span class="time"><i class="far fa-clock"></i> ' +
                        //         response.created_date[lenght_id]+ response.created_time[lenght_id]+
                        //         '</span> ' +
                        //         '<h3 class="timeline-header border-0 "> ' +
                        //         '<a href="#">' + response.user_role[lenght_id]+ '</a> ' +
                        //          + response.new_log[lenght_id]+ '</h3> ' +
                        //         '</div> </div>');
                        //     console.log(response.created_date[lenght_id] + ' ' +lenght_id);
                        //     lenght_id--;
                        // }
                        $('.dealer_date').show();
                        $('.dealer_date span').html(response.created_date[0]);
                        $('.dealer_time span').html(response.created_time[0]);
                        if(response.user_role[1] === 'admin'|| response.user_role[1] ===  'super_admin' ){
                            $('.admin_area').show();
                            $('.admin_name').html('Admin Received Your Order ');
                            $('.admin_time span').html(response.created_time[1]);
                            $('.admin_name a').html(response.user_name[1]);

                        }else if(response.user_role[1] === 'representative'||
                            response.user_role[1] === 'super_admin' ){

                            $('.admin_area').show();
                            $('.admin_name').html('Representative Taking Your Order ');
                            $('.admin_time span').html(response.created_date[1]+ response.created_time[1]);
                            $('.admin_name a').html(response.user_name[1]);
                        }
                        if(response.user_role[2] === 'representative' ) {

                            console.log('true');
                            $('.res_area').show();
                            $('.res_name').html('Representative Received Your Order ');
                            $('.res_time span').html(response.created_date[2] + response.created_time[2]);
                            $('.res_name a').html(response.user_name[2]);
                        }else if(response.user_role[2] ==='data_entry'  ) {

                            $('.res_area').show();
                            $('.res_name').html('Data Entry Start Store Your Order');
                            $('.res_time span').html(response.created_date[2] + response.created_time[2]);
                            $('.res_name a').html(response.user_name[2]);
                        }
                        if(response.user_role[3] === 'data_entry' ) {

                            console.log('true');
                            $('.data_entry_area').show();
                            $('.data_entry_name').html('Data Entry Start Store Your Order');
                            $('.data_entry_time span').html(response.created_date[3] +response.created_time[3]);
                            $('.data_entry_name a').html(response.user_name[3]);
                        }else if(response.user_role[3] === 'manager' ){

                            $('.data_entry_area').show();
                            $('.data_entry_name').html('Manager Sent Your Order To Driver');
                            $('.data_entry_time span').html(response.created_date[3] +response.created_time[3]);
                            $('.data_entry_name a').html(response.user_name[3]);
                        }
                        if(response.user_role[4] === 'manager' ) {

                            console.log('true');
                            $('.manager_area').show();
                            $('.manager_name').html('Manager Sent Your Order To Driver');
                            $('.manager_time span').html(response.created_date[4] +response.created_time[4]);
                            $('.manager_name a').html(response.user_name[4]);
                        }else if(response.user_role[4] === 'representative' ){
                            $('.manager_area').show();
                            $('.manager_name').html('Driver delivery Your Order');
                            $('.manager_time span').html(response.created_date[4] +response.created_time[4]);
                            $('.manager_name a').html(response.user_name[4]);

                        }
                        if(response.user_role[5] === 'representative' ) {

                            console.log('true');
                            $('.representative_area').show();
                            $('.representative_name').html('Driver delivery Your Order');
                            $('.representative_time span').html(response.created_time[5]);
                            $('.representative_name a').html(response.user_name[5]);
                        }else{

                        }
                    }

                    // $.each( response.logs, function( key, value ) {
                    //
                    //
                    //     toastr.error(value, "Danger");
                    //     console.log(key, response.logs[key]);
                    // });
                    // printMsg(response);
                    /*

                 'status'=>200,
                 'user_name'=>$user_name,
                 'user_role'=>$user_role,
                 'created_date'=>$created_date,
                 'created_time'=>$created_time,
                     */
                    // console.log(response);
                    // console.log(response[0]);
                    // console.log(response[1][user_id]);
                },
                error: function () {
                },
            })
        });
        $("form").on("submit", function(event){

            event.preventDefault();

            let formData = new FormData(this);
            var formValues= $(this).serialize();

            console.log(formData);
            $.ajax({
                url: '{{ route('store_order') }}',
                type: 'POST',
                async: true,
                cache: false,
                data: formData,
                contentType: false,
                processData: false,
                success: function (response) {
                    printMsg(response);

                    console.log(response.info);
                    $('tbody').append('<tr>' +
                        '<td>'+response.info.count_box+'</td>'+
                        '<td>'+response.info.price+'</td>'+
                        '<td>'+response.info.price_deliver+'</td>'+
                        '<td>'+response.info.order_type +'</td>'+
                        '<td> </td>'+
                        '<td>'+response.zone +'</td>'+
                        '<td>'+response.state +'</td>'+
                        // '<td>'+response.info.zone +'</td>'+
                        '<td><a href="#" data-key="'+response.id+'" class="dropdown-item delete-record">'

                        +'</tr>');
                },
                error: function () {
                    $('.status').append('حصل خطأ أثناء الاتصال بالانترنت');
                },
            })
        });
        function printMsg (msg) {
            if($.isEmptyObject(msg.error)){
                console.log(msg.success);
                toastr.success(" Your work has been saved", "Success");
                // $('input').val('') ;
            }else{
                $.each( msg.error, function( key, value ) {
                    toastr.error(value, "Danger");
                });
            }
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('.delete-record').click(function (e) {
            e.preventDefault();
            let id = $(this).data("key");
            console.log(id);
            $.ajax({
                url: '{{ route('sdelete_order') }}',
                type: 'POST',
                async: true,
                cache: false,
                data: {
                    'id':id
                },
                success: function (response) {
                    toastr.success(" Your work has been saved", "Success");
                    $('.row_'+id).hide();

                },
                error: function () {
                    toastr.error(" Your work has been not saved", "Error");

                },
            })
        });
    </script>
@endpush

{{--@else--}}
{{--@endif--}}
{{--@endauth--}}
