@extends('new_layout.app')

@section('title', 'To')
{{--@section('page_title','List State')--}}

@push('style')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <style>
        .hide{
            display:none;
        }
        .image-upload>input {
             display: none;
         }
    </style>
@endpush
@section('content')


    @role('admin|super_admin|representative|driver|delivery')

    <!-- Complex Headers -->

    <div class="col-12">

        <!-- /.card -->

        <div class="card">
            <div class="modal fade" id="modal-md" role="dialog" >
                <div class="modal-dialog modal-md  modal-dialog-scrollable" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Update</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">

                            <form class="form-horizontal col-md-12" id="myform" METHOD="post" action="#"
                                  enctype="multipart/form-data" autocomplete="on"
                            >
                                {{ csrf_field()}}
                                {{--                        @dd($id->media)--}}
                                <input type="hidden" name="order_id" value="" id="order_id">
                                <input type="hidden" name="image_id" value="" id="image_id">


                                <div class="row">
                                    <div class="form-group col-md-12">
                                      <img id="image_src" src="">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <input
                                            type="file"
                                            name="file"
                                            id="file"
                                            class="form-control input-file"
                                            required
                                        />
                                    </div>
                                </div>

                                <div class="modal-footer justify-content-between">
                                    {{--                                <button type="button" class="btn btn-primary">Save changes</button>--}}
                                    <div class="row">
                                        <div class="col-12">
                                            <button type="submit" id="update_image" class="btn btn-primary">Submit</button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->

            <div class="card-header">
                <h3 class="card-title">Order Number {{ $order->id }} </h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">


                    <div class="status">

                    </div>
                    <form class="form-horizontal col-md-12" id="create" METHOD="post" action="#"
                          enctype="multipart/form-data" autocomplete="on"
                    >
                        {{ csrf_field()}}
                        <input type="hidden" name="id" value="{{ $order->id }}">


                        <div class="form-group increment">
                            <label>Photos</label>
                            <div class="input-group control-group " >
                            <input type="file" name="file[]" multiple required class="form-control">

                            <div class="input-group-btn">
                                <button class="btn btn-success" type="button">
                                    <i class="fa fa-plus"></i>Add</button>
                            </div>
{{--                                <input type="file" name="filename[]" class="form-control">--}}
                            </div>
                            <div class="clone hide">
                                <div class="control-group input-group" style="margin-top:10px">
                                    <input type="file" name="file[]" class="form-control">
                                    <div class="input-group-btn">
                                        <button class="btn btn-danger" type="button">
                                            <i data-feather='trash-2'></i> Remove</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <button type="submit" id="submit" class="btn btn-primary">Submit</button>
                            </div>
{{--                            <div class="col-6">--}}
{{--                                <a href="{{  }}"--}}
{{--                                <button type="button" id="alert" class="btn btn-warning">Send alert to dealer</button>--}}
{{--                            </div>--}}
                        </div>
                    </form>
                    @if($order->media != null)
                        <hr>
                        <div class="m-1 card card-success col-md-12">

                            <!-- card-header -->
                            <div class="card-body">
                                <table id="example1" class="table table-bordered table-striped" style="width:100%;">
                                    <thead>
                                    <tr>
                                        <th >id</th>
                                        <th>Image</th>
                                        <th>Dealer Name</th>
                                        <th>Update</th>
                                        <th>Delete</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach( $order->media as $media)
{{--                                        @dd($media->package)--}}
                                        <tr class="row_{{ $media->id }}">
                                            <td >{{ $media->id}} </td>

                                            <td>
                                                <img src="{{ str_replace('/storage','',$media->file_path) }}" class="image_{{$media->id}}" width="250">
                                            </td>
                                            <td>{{ $order->dealer?$order->dealer->name.'
Enter data Enter this merchandise
When you switch the photo we\'ll send him a notification to re-enter it again':' It hasn\'t been entered yet, you can replace it
'}}</td>
                                            <td>
                                                @if($media->package&& $media->package->package_status === 0)

                                                    Done
                                                @else
                                                    <span data-target="#modal-md"
                                                           data-gallery="{{ str_replace('/storage','',$media->file_path) }}"
                                                           {{--                                                      data-gallery="{{ $media->file_path }}"--}}

                                                           data-key="{{ $order->id }}"
                                                           data-toggle="modal"
                                                           data-id="{{ $media->id }}"
                                                           class="update badge  badge-light-warning mr-1">
                                                   <i data-feather='edit'></i>
                                                </span>
                                                @endif
                                            </td>
                                            <td>
                                                <span  data-key="{{ $media->id }}" class="badge   badge-light-danger mr-1 delete-record">
                                                  <i data-id="{{ $media->id }}"  data-feather='trash delete'></i>

                                                </span>
                                            </td>

                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    @endrole
@stop
@push('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script type="text/javascript">

        $(document).ready(function() {

            $(".btn-success").click(function(){
                var html = $(".clone").html();
                $(".increment").after(html);
            });

            $("body").on("click",".btn-danger",function(){
                $(this).parents(".control-group").remove();
            });

        });

        $('.update').click(function (){

            let id = $(this).data("key");
            var image_id = $(this).data("id");
            let image_src = $(this).data("gallery");
            $('#order_id').val(id);
            $('#image_id').val(image_id);
            $('#image_src').attr('src',image_src);
        });
        function readURL(input,target_id) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.readAsDataURL(input.files[0]);
                reader.onload = function (e) {
                    let target = "#"+target_id+" img";
                }
            }
        }
        $(".input-file").on("change", function (e) {
            id=e.target.id;
            let target_id = $(this).data('target');
            let target = $(this).val();
            var tmppath = URL.createObjectURL(e.target.files[0]);
            $('#image_src').attr('src',tmppath );
            var inp = $(this)[0];
        });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $("#update_image").on("click", function(e){

            e.preventDefault();

            var form = $('#myform')[0];
            var formData = new FormData(form);
            // var form = $form.find(this).val();
            // var form = $(this).closest('form');
            //  let formData = new FormData(form);
            // var formValues= formData.serialize();
            // var formValues= form.serialize();

            console.log(form);

            // let file = $('#file');
            let media_id = $('#image_id').val();
            // let order_id = $('#order_id').val();
            $.ajax({
                url: '{{ route('update_img_res') }}',
                type: 'POST',
                async: true,
                data: formData,
                contentType: false,
                processData: false,
                beforeSend:function (){
                    $('#update_image').prop('disabled', true);
                },
                success: function (response) {
                    toastr.success(" Your work has been saved", "Success");
                    $('.image_' + media_id).attr('src',response.image_src);
                    $('#update_image').prop('disabled', false);
                    $('#modal-md').modal('hide');
                },
                error: function () {
                    toastr.error(" Your work has been not saved", "Error");
                },
            })
        });
    </script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $("form").on("submit", function(event){

            event.preventDefault();

            let formData = new FormData(this);
            var formValues= $(this).serialize();

            console.log(formData);
            $.ajax({
                url: '{{ route('store_enable') }}',
                type: 'POST',
                async: true,
                cache: false,
                data: formData,
                contentType: false,
                processData: false,
                success: function (response) {
                    printMsg(response);
                    {{--window.location.replace("{{ route('orders_rep') }}");--}}

                },
                error: function () {
                    $('.status').html('حصل خطأ أثناء الاتصال بالانترنت');
                },
            })
        });
        $('.delete-record').click(function (e) {
            e.preventDefault();
            let id = $(this).data("key");
            console.log(id);
            $.ajax({
                url: '{{ route('sdelete_media') }}',
                type: 'POST',
                async: true,
                cache: false,
                data: {
                    'id':id
                },
                success: function (response) {
                    toastr.success(" Your work has been saved", "Success");
                    $('.row_'+id).hide();

                },
                error: function () {
                    toastr.error(" Your work has been not saved", "Error");
                },
            })
        });
        function printMsg (msg) {
            if($.isEmptyObject(msg.error)){
                console.log(msg.success);
                toastr.success(" Your work has been saved", "Success");
                $('input').val('') ;
            }else{
                $.each( msg.error, function( key, value ) {
                    $('.status').html(value).css('color','red');
                });
            }
        }
    </script>
@endpush


