{{--@auth()--}}
{{--    @if(auth()->user()->hasRole('super_admin') != null )--}}
@extends('new_layout.app')

@section('title', ' List Orders')
{{--@section('page_title','Create New Order')--}}
@push('style')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

    <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
@endpush
@section('content')

    @role('admin|super_admin|representative|driver|delivery')
    <!-- /.content -->
    <div class="col-12">

        <!-- /.card -->

        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Orders </h3>
            </div>
            <!-- card-header -->
            <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        {{--                            <th >id</th>--}}
                        <th>Dealer Name</th>
                        <th >Count Boxes</th>
                        <th >Price</th>
                        <th >Price Deliver</th>
                        <th >Order Type</th>
                        <th >Action</th>
                        <th >Zone</th>
                        <th >State</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach( $order as $index=>$orders)
                        <tr class="row_{{ $orders->id }}">
                            <td>{{ $orders->dealer->name}}</td>
                            <td>{{ $orders->count_box}}</td>
                            <td>{{ $orders->price}}</td>
                            <td>{{ $orders->price_deliver}}</td>
                            <td>{{ $orders->order_type}}</td>
                            <td>
{{--                                @if($orders->order_status > 2 )--}}
{{--                                    ---}}
{{--                                @else--}}
                                <span class="badge  badge-light-warning mr-1">
                                    <a href="{{ route('enable_order',['id'=>$orders->id]) }}">Enable</a>
                                </span>
{{--                                    @endif--}}
                            </td>
                            <td>{{ $orders->zone?$orders->zone->name:''}}</td>
                            <td>{{ $orders->state->name}}</td>

                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->

    @endrole
@endsection

@push('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <!-- DataTables  & Plugins -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    {{--    <script src="dist/js/demo.js"></script>--}}
    <!-- Page specific script -->
    <script>
        $(function () {
            $("#example1").DataTable({
                "responsive": true, "lengthChange": false, "autoWidth": false,
                "buttons": ["copy", "csv", "excel", "pdf", "print"]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

        });
    </script>
    <script>

    </script>
@endpush

{{--@else--}}
{{--@endif--}}
{{--@endauth--}}
