{{--@auth()--}}
{{--    @if(auth()->user()->hasRole('super_admin') != null )--}}
@extends('new_layout.app')

@section('title', 'Create & List Orders')
{{--@section('page_title','Create New Order')--}}
@push('style')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

    <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
@endpush
@section('content')

    @role('admin|super_admin|representative|driver|delivery')
    <!-- /.content -->
    <div class="col-12">

        <!-- /.card -->

        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Orders </h3>
            </div>
            <!-- card-header -->
            <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        {{--                            <th >id</th>--}}
                        <th >Action</th>
                        <th>Dealer Name</th>
                         <th >Count Boxes</th>
                        <th >Price</th>
                        <th >Price Deliver</th>
                        <th >Order Type</th>
                        <th >Zone</th>
                        <th >State</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach( $order as $index=>$orders)
                        <tr class="row_{{ $orders->id }}">

                            <td>
                                <span>
                                    <a  href="#" data-key="{{ $orders->id }}"
                                        class="badge bg-success active-record">Taking</a>
                                </span>

                            </td>
                            {{--                                <td >{{ $orders->id}}</td>--}}
                            <td>{{ $orders->dealer?$orders->dealer->name:''}}</td>

                            <td>{{ $orders->count_box}}</td>
                            <td>{{ $orders->price}}</td>
                            <td>{{ $orders->price_deliver}}</td>
                            <td>{{ $orders->order_type}}</td>
                            <td>{{ $orders->zone?$orders->zone->name:''}}</td>
                            <td>{{ $orders->state->name}}</td>

                        </tr>
                    @endforeach

                    @foreach( $order_state as $index=>$orders)

                        <tr class="row_{{ $orders->id }}">
                            <td>
                                <span>
                                    <a  href="#" data-key="{{ $orders->id }}"
                                        class="badge   badge-light-success mr-1 active-record">Taking</a>
                                </span>

                            </td>
                            {{--                                <td >{{ $orders->id}}</td>--}}
                            <td>{{ $orders->dealer?$orders->dealer->name:''}}</td>

                            <td>{{ $orders->count_box}}</td>
                            <td>{{ $orders->price}}</td>
                            <td>{{ $orders->price_deliver}}</td>
                            <td>{{ $orders->order_type}}</td>

                            <td>{{ $orders->zone?$orders->zone->name:''}}</td>
                            <td>{{ $orders->state->name}}</td>

                        </tr>
                    @endforeach

                    @foreach( $order_gov as $index=>$orders)

                        <tr class="row_{{ $orders->id }}">
                            <td>
                                <span>
                                    <a  href="#" data-key="{{ $orders->id }}"
                                        class="badge bg-success active-record">Taking</a>
                                </span>

                            </td>
                            {{--                                <td >{{ $orders->id}}</td>--}}
                            <td>{{ $orders->dealer?$orders->dealer->name:''}}</td>

                            <td>{{ $orders->count_box}}</td>
                            <td>{{ $orders->price}}</td>
                            <td>{{ $orders->price_deliver}}</td>
                            <td>{{ $orders->order_type}}</td>

                            <td>{{ $orders->zone?$orders->zone->name:''}}</td>
                            <td>{{ $orders->state->name}}</td>

                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        {{--                            <th >id</th>--}}
                        <th >Action</th>
                        <th>Dealer Name</th>
                        <th >Count Boxes</th>
                        <th >Price</th>
                        <th >Price Deliver</th>
                        <th >Order Type</th>
                        <th >Zone</th>
                        <th >State</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->

    @endrole
@endsection

@push('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <!-- DataTables  & Plugins -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    {{--    <script src="dist/js/demo.js"></script>--}}
    <!-- Page specific script -->
    <script>
        $(function () {
            $("#example1").DataTable({
                "responsive": true, "lengthChange": false, "autoWidth": false,
                "buttons": ["copy", "csv", "excel", "pdf", "print"]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
            // $('#tbl-pieces').DataTable({
            //     "paging": true,
            //     "lengthChange": true,
            //     "searching": true,
            //     "ordering": true,
            //     "info": true,
            //     "autoWidth": false,
            //     "responsive": true,
            // });
        });
    </script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('.active-record').click(function (e) {
            e.preventDefault();
            let id = $(this).data("key");
            console.log(id);
            $.ajax({
                url: '{{ route('take_order') }}',
                type: 'POST',
                async: true,
                cache: false,
                data: {
                    'id':id
                },
                success: function (response) {
                    toastr.success(" Your work has been saved", "Success");

                    if(response.status_code === 200){

                        window.location.replace("{{ route('my_order') }}");
                    }
                },
                error: function () {
                    toastr.error(" Your work has been not saved", "Error");
                },
            })
        });
        function printMsg (msg) {
            if($.isEmptyObject(msg.error)){
                console.log(msg.success);
                toastr.success(" Your work has been saved", "Success");
                $('input').val('') ;
            }else{
                $.each( msg.error, function( key, value ) {
                    $('.status').text(value).css('color','red');
                });
            }
        }
    </script>
@endpush

{{--@else--}}
{{--@endif--}}
{{--@endauth--}}
