{{--@auth()--}}
{{--    @if(auth()->user()->hasRole('super_admin') != null )--}}
@extends('new_layout.app')

@section('title', 'Create & List Orders')
{{--@section('page_title','Create New Order')--}}
@push('style')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

    <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
@endpush
@section('content')

    @role('admin|super_admin|representative|driver|delivery')
    <!-- /.content -->
    <div class="col-12">

        <!-- /.card -->

        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Orders </h3>
            </div>
            <div class="modal fade" id="modal-md" role="dialog" >
                <div class="modal-dialog modal-md  modal-dialog-scrollable" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Plaese Enter Description</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">

                            <form class="form-horizontal col-md-12" id="create" METHOD="post" action="#"
                                  enctype="multipart/form-data" autocomplete="on"
                            >
                                {{ csrf_field()}}
                                {{--                        @dd($id->media)--}}
                                <input type="hidden" name="id" value="" id="order_id">
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label class="form-label" for="basic-addon-tel_receiver">
                                            Note *
                                        </label>

                                        <textarea
                                            type="text"
                                            rows="2"
                                            name="description"
                                        ></textarea>
                                    </div>
                                </div>
                                <div class="modal-footer justify-content-between">
                                    {{--                                <button type="button" class="btn btn-primary">Save changes</button>--}}
                                    <div class="row">
                                        <div class="col-12">
                                            <button type="submit" id="submit" class="btn btn-primary">Submit</button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>

            <!-- card-header -->
            <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        {{--                            <th >id</th>--}}

                        <th >CC</th>
                        <th >Code</th>
                        <th >Pieces</th>
                        <th >Price</th>
                        <th >Price Deliver</th>
                        <th >Price Cancel</th>
                        <th >Cancel</th>
                        <th>Status</th>
                        <th >Price from customer</th>
                        <th >State Shipper</th>
                        <th >State Receiver</th>
                        <th >Date Deliver</th>
                        <th >Total Price From Dealer</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($order as $key=>$package)
                        <tr class="row_{{ $package->id }}">
                            <td>{{ $package->cc}}</td><td>{{ $package->code}}</td><td>{{ $package->pieces}}</td>
                            <td>{{ $package->total_price}}</td><td>{{ $package->price_deliver}}</td><td>{{ $package->price_cancel}}</td>
                            <td>@if($package->price_cancel != null)-@else
                                    <span class="send-record_cancel badge bg-danger"
                                          data-key="{{ $package->id }}" data-toggle="modal"
                                          data-target="#modal-md">
                                            cancel
                                    </span>
                                @endif
                            </td>
                            <td>@if($package->is_delivery == null)

                                    <a href="#" class="badge bg-info active-record record_{{ $package->id }}"
                                       data-key="{{ $package->id }}"  >
                                        تحديد كموصل ؟
                                    </a>
                                @elseif($package->is_delivery = 1)
                                    <span class="badge bg-success " >
                                            تم توصيله
                                        </span>
                                @else
                                    تم الالغاء
                                @endif
                            </td>
                            <td>{{ $package->price_customer}}</td>
                            <td>{{ $package->state_shipper?$package->state_shipper->name:''}}</td>
                            <td>{{ $package->state_receiver?$package->state_receiver->name:''}}</td>
                            <td>{{ date('Y-m-d H:m:s', strtotime($package->created_at))}}</td>
                            <td>{{ $package->order->price }}</td>

                        </tr>
                    @endforeach
                    </tbody>

                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->

    @endrole
@endsection

@push('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <!-- DataTables  & Plugins -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    {{--    <script src="dist/js/demo.js"></script>--}}
    <!-- Page specific script -->
    <script>
        $(function () {
            $("#example1").DataTable({
                "responsive": true, "lengthChange": false, "autoWidth": false,
                "buttons": ["copy", "csv", "excel", "pdf", "print"]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

        });
    </script>
    <script>
        $("#delivery").addClass('active');
        $("#delivery").parent().parent().parent().addClass('menu-open');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('.active-record').click(function (e) {
            e.preventDefault();
            let id = $(this).data("key");
            console.log(id);
            $.ajax({
                url: '{{ route('is_active') }}',
                type: 'POST',
                async: true,
                cache: false,
                data: {
                    'id':id
                },
                success: function (response) {
                    toastr.success(" Your work has been saved", "Success");
                    $('.record_'+id).html('تم توصيله');

                },
                error: function () {
                    toastr.error(" Your work has been not saved", "Error");
                },
            })
        });
    </script>
@endpush

{{--@else--}}
{{--@endif--}}
{{--@endauth--}}
