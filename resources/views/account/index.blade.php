@extends('new_layout.app')

@section('title', 'Users')
{{--@section('page_title','List Users')--}}

@push('style')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

    <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
@endpush
@section('content')


    @role('admin|super_admin|accountant|accountant_dealer|accountant_delivery')

    <!-- Complex Headers -->

    <div class="col-12">

        <!-- /.card -->

        <div class="card">
            <div class="card-header">
                <h3 class="card-title">All Users </h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table id="tbl-pieces" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th >id</th>
                        <th >Name</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>Image</th>
                        <th>Mobile</th>
                        <th>Governorate</th>
                        <th>State</th>
                        <th>Zone</th>
                        <th>Coordinator Name</th>
                        <th>Address</th>
                        <th>Facebook</th>
                        <th>Whatsapp</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach( $users as $index=>$user)
                        <tr class="row_{{ $user->id }}">
                            <td >{{ $index +1 }}</td>
                            <td>{{ $user->name}}</td><td>{{ $user->email}}</td>

                            <td>{{ $user->roles != null ? $user->roles->first()->display_name : 'users'}}</td>
                            <td><img src="{{ $user->image_path ?? asset('dist/img/AdminLTELogo.png')}}" height="45" width="45"></td>
                         <td>{{ $user->mobile}}</td><td>{{ $user->governorate}}</td>
                            <td>{{ $user->state}}</td><td>{{ $user->zone}}</td><td>{{ $user->coordinator_name}}</td>
                            <td>{{ $user->address}}</td><td>{{ $user->facebook}}</td><td>{{ $user->whatsapp}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th >id</th>
                        <th >Name</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>Image</th>
                        <th>Mobile</th>
                        <th>Governorate</th>
                        <th>State</th>
                        <th>Zone</th>
                        <th>Coordinator Name</th>
                        <th>Address</th>
                        <th>Facebook</th>
                        <th>Whatsapp</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->


    @endrole
@stop
@push('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <!-- DataTables  & Plugins -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    {{--    <script src="dist/js/demo.js"></script>--}}
    <!-- Page specific script -->
    <script>
        $(function () {
          $('#tbl-pieces').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });
        });
    </script>
    <script>

        $("#list_users").addClass('active');
        $("#list_users").parent().parent().parent().addClass('menu-open');

    </script>
@endpush
