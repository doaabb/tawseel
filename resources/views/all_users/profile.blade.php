{{--@auth()--}}
{{--    @if(auth()->user()->hasRole('super_admin') != null )--}}
@extends('new_layout.app')

@section('title', 'Create & List Orders')
{{--@section('page_title','Create New Order')--}}
@push('style')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

{{--    <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">--}}
{{--    <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">--}}
{{--    <link rel="stylesheet" href="{{ asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">--}}
@endpush
@section('content')

    @auth()
        <div class="row">
            <div class="col-md-3">

                <!-- Profile Image -->
                <div class="card card-primary card-outline">
                    <div class="card-body box-profile">
                        <div class="text-center">
                            <img class="profile-user-img img-fluid img-circle rounded-circle"
                                 src="{{ auth()->user()->image_path ?? 'dist/img/user6-128x128.jpg' }}"
                                 alt="User profile picture">
                        </div>

                        <h3 class="profile-username text-center">{{ auth()->user()->name }}</h3>

                        <p class="text-muted text-center">{{ auth()->user()->roles->first()->display_name }}</p>

                        @role('dealer')
                        <ul class="list-group list-group-unbordered mb-3">
                            <li class="list-group-item">
                                <b>Orders</b> <a class="float-right">{{ auth()->user()->dealer }}</a>
                            </li>
{{--                            <li class="list-group-item">--}}
{{--                                <b>Following</b> <a class="float-right">543</a>--}}
{{--                            </li>--}}
{{--                            <li class="list-group-item">--}}
{{--                                <b>Friends</b> <a class="float-right">13,287</a>--}}
{{--                            </li>--}}
                        </ul>
                        @endrole

{{--                        <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a>--}}
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->

                <!-- About Me Box -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">About Me</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <strong><i class="fas fa-globe mr-1"></i> Email </strong>

                        <p class="text-muted">
                            {{ auth()->user()->email }}
                        </p>

                        <hr>

                        <strong><i class="fas fa-map-marker-alt mr-1"></i> Location</strong>

                        <p class="text-muted">{{ auth()->user()->governorates ? auth()->user()->governorates->name : ''}}
                            -{{ auth()->user()->states ? auth()->user()->states->name :''}}
                            -{{ auth()->user()->zones?auth()->user()->zones->name:'' }}
                            -{{ auth()->user()->address }}
                        </p>

                        <hr>

                        <strong><i class="fas fa-pencil-alt mr-1"></i> Social</strong>

                        <p class="text-muted">
                           <a href="{{ auth()->user()->facebook }}">
                               <span class="fas fa-facebook"></span>
                           </a>
                           <a href="{{ auth()->user()->whatsapp }}">
                               <span class="fa fa-whatsapp"></span>
                           </a>
                           <a href="{{ auth()->user()->instagram }}">
                               <span class="fa fa-instagram"></span>
                           </a>
                           <a href="{{ auth()->user()->mobile }}">
                               <span class="fa fa-phone"></span>
                           </a>
                        </p>

                        <hr>

                      </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
            <div class="col-md-9">
                <div class="card">
                    <div class="card-header p-2">
                        <ul class="nav nav-pills">
                            <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">Last Orders</a></li>


                            @role('dealer')
                            <li class="nav-item"><a class="nav-link" href="#timeline" data-toggle="tab">Timeline Order</a></li>
                            @endrole
                            <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">Settings</a></li>
                        </ul>
                    </div><!-- /.card-header -->
                    <div class="card-body">
                        <div class="tab-content">
                            <div class="active tab-pane" id="activity">
                                <!-- Post -->
                                <div class="post">
                                    @role('super_admin|admin')
                                    @foreach(\App\Models\Order::orderBy('created_at','ASC')->take(6)->get() as $order)
                                    <div class="user-block">
                                        <img class="img-circle img-bordered-sm rounded-circle media-bordered mb-2"
                                             src="{{ $order->dealer->image_path ?? 'dist/img/user1-128x128.jpg' }}"  width="64" alt="user image">
                                        <span class="username">
                                            <a href="#">Order Number {{ $order->id }}</a>
                                            <a href="#" class="float-right btn-tool"><i class="fas fa-times"></i></a>
                                        </span>
                                        <span class="description">Added at - {{ $order->created_at->diffForHumans() }}</span>
                                    </div>
                                    @endforeach
                                    @endrole

                                    @role('dealer')
                                    @foreach(\App\Models\Order::where('dealer_id',auth()->user()->id)->take(6)->get() as $order)
                                    <div class="user-block">
                                        <img class="img-circle img-bordered-sm"
                                             src="{{ $order->dealer->image_path ?? 'dist/img/user1-128x128.jpg' }}" alt="user image">
                                        <span class="username">
                                            <a href="#">Order Number {{ $order->id }}</a>
                                            <a href="#" class="float-right btn-tool"><i class="fas fa-times"></i></a>
                                        </span>
                                        <span class="description">
                                            Added at - {{ $order->created_at->diffForHumans() }}</span>
                                    </div>
                                    @endforeach
                                    @endrole

                                    @role('store')
                                    @foreach(\App\Models\Order::where('order_status',2)->take(6)->get() as $order)
                                    <div class="user-block">
                                        <img class="img-circle img-bordered-sm"
                                             src="{{ $order->dealer->image_path ?? 'dist/img/user1-128x128.jpg' }}" alt="user image">
                                        <span class="username">
                                            <a href="#">Order Number {{ $order->id }}</a>
                                            <a href="#" class="float-right btn-tool"><i class="fas fa-times"></i></a>
                                        </span>
                                        <span class="description">Added at - {{ $order->created_at->diffForHumans() }}</span>
                                    </div>
                                    @endforeach
                                    @endrole

                                    @role('data_entry')
                                    @foreach(\App\Models\Order::where('order_status','>=',2)->take(6)->get() as $order)
                                    <div class="user-block">
                                        <img class="img-circle img-bordered-sm"
                                             src="{{ $order->dealer->image_path ?? 'dist/img/user1-128x128.jpg' }}" alt="user image">
                                        <span class="username">
                                            <a href="#">Order Number {{ $order->id }}</a>
                                            <a href="#" class="float-right btn-tool"><i class="fas fa-times"></i></a>
                                        </span>
                                        <span class="description">Added at - {{ $order->created_at->diffForHumans() }}</span>
                                    </div>
                                    @endforeach
                                    @endrole


                                    @role('representative')
                                    @foreach(\App\Models\Order::where('order_status',1)->take(6)->get() as $order)
                                    <div class="user-block">
                                        <img class="img-circle img-bordered-sm"
                                             src="{{ $order->dealer->image_path ?? 'dist/img/user1-128x128.jpg' }}" alt="user image">
                                        <span class="username">
                                            <a href="#">Order Number {{ $order->id }}</a>
                                            <a href="#" class="float-right btn-tool"><i class="fas fa-times"></i></a>
                                        </span>
                                        <span class="description">Added at - {{ $order->created_at->diffForHumans() }}</span>
                                    </div>
                                    @endforeach
                                    @endrole

                                    @role('representative')
                                    @foreach(\App\Models\Order::where('representative_id','=',auth()->user()->id)->take(6)->get() as $order)
                                    <div class="user-block">
                                        <img class="img-circle img-bordered-sm"
                                             src="{{ $order->dealer->image_path ?? 'dist/img/user1-128x128.jpg' }}" alt="user image">
                                        <span class="username">
                                            <a href="#">Order Number {{ $order->id }}</a>
                                            <a href="#" class="float-right btn-tool"><i class="fas fa-times"></i></a>
                                        </span>
                                        <span class="description">Added at - {{ $order->created_at->diffForHumans() }}</span>
                                    </div>
                                    @endforeach
                                    @endrole
                                    <!-- /.user-block -->

                                    <p>
                                    </p>

                                </div>
                                <!-- /.post -->
                            </div>
                            <!-- /.tab-pane -->
                            @role('dealer')
{{--                            @dd(\App\Models\Order::where('dealer_id',auth()->user()->id)->get())--}}
                            <div class="tab-pane" id="timeline">
                            @foreach(\App\Models\Order::where('dealer_id',auth()->user()->id)->get() as $order  )
                                <!-- The timeline -->
                                <div class="timeline timeline-inverse">
                                    <!-- timeline time label -->
                                    <div class="time-label">
                                        <span class="bg-danger">
                                            {{ date('Y-m-d',strtotime($order->created_at)) }}
                                        </span>
                                    </div>
                                         <!-- /.timeline-label -->
                                    @foreach($order->activity_order as $activity)

                                        <!-- timeline item -->
                                    <div>
                                        <i class="fas fa-user-circle bg-success"></i>

                                        <div class="timeline-item">
                                            <span class="time"><i class="far fa-clock"></i>
                                                {{ $activity->created_at->diffForHumans() }}
                                            </span>

                                            <h3 class="timeline-header">
                                                {{--                                                <a href="#">Support Team</a> --}}
                                                {{  $activity->user->name }} From
                                                {{  $activity->ip_address }}
                                            </h3>

                                            <div class="timeline-body">
                                              <a href="#"> {{  $activity->user->name }}: </a>   {{  $activity->description }}
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END timeline item -->
                                    <!-- timeline time label -->
                                    <div class="time-label">
{{--                                        <span class="bg-success">--}}
{{--                                        </span>--}}
                                    </div>
                                        @endforeach
                                  </div>
                                @endforeach
                            </div>
                            <!-- /.tab-pane -->
                            @endrole
                            <div class="tab-pane" id="settings">
                                <form class="form-horizontal col-md-12" id="create" METHOD="post" action="{{ route('update_profile') }}"
                                      enctype="multipart/form-data" autocomplete="on">

                                    {{ csrf_field()}}
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class="form-label" for="basic-addon-name">Name</label>

                                            <input
                                                type="text"
                                                id="basic-addon-name"
                                                class="form-control"
                                                placeholder="Name"
                                                aria-label="Name"
                                                name="name"
                                                value="{{ auth()->user()->name }}"
                                                aria-describedby="basic-addon-name"
                                                required
                                            />
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="form-label" for="basic-addon-email">Email</label>
                                            <input
                                                type="email"
                                                id="basic-addon-email"
                                                class="form-control"
                                                placeholder="Email"
                                                aria-label="Email"
                                                name="email"
                                                value="{{ auth()->user()->email }}"
                                                aria-describedby="basic-addon-email"
                                                disabled
                                            />
                                        </div>
                                    </div>
{{--                                    <div class="row">--}}

{{--                                        <div class="form-group col-md-6">--}}
{{--                                            <label class="form-label" for="basic-addon-password">Password</label>--}}
{{--                                            <input--}}
{{--                                                type="password"--}}
{{--                                                id="basic-addon-password"--}}
{{--                                                class="form-control"--}}
{{--                                                placeholder="Password"--}}
{{--                                                aria-label="password"--}}
{{--                                                name="password"--}}
{{--                                                value="{{ auth()->user()->password }}"--}}
{{--                                                aria-describedby="basic-addon-password"--}}
{{--                                                required--}}
{{--                                            />--}}
{{--                                        </div>--}}

{{--                                    </div>--}}
                                    <div class="form-group ">
                                        <label class="form-label" for="basic-addon-image_name">Avatar</label>
                                        <input
                                            type="file"
                                            id="basic-addon-image_name"
                                            class="form-control"
                                            placeholder="image_name"
                                            aria-label="image_name"
                                            name="file"

                                            aria-describedby="basic-addon-image_name"
                                        />
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">

                                            <label>Governorate</label>
                                            <select class="form-control select2" name="governorate_id" style="width: 100%;">
{{--                                                <option selected value="{{ auth()->user()->governorate_id }}">{{ auth()->user()->governorate }}</option>--}}
                                                @foreach($gov as $gover)
                                                    <option value="{{ $gover->id }}" {{ (auth()->user()->governorate_id == $gover->id) ? 'selected' : '' }}>{{ $gover->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6">

                                            <label >State</label>
                                            <select class="form-control select2" name="state_id" style="width: 100%;">
                                                @foreach($state as $gover)
                                                    <option value="{{ $gover->id }}" {{ (auth()->user()->state_id == $gover->id) ? 'selected' : '' }}>{{ $gover->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                    </div>


                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class="form-label" for="basic-addon-mobile">Mobile</label>

                                            <input
                                                type="text"
                                                id="basic-addon-mobile"
                                                class="form-control"
                                                placeholder="Mobile"
                                                aria-label="mobile"
                                                name="mobile"
                                                value="{{ auth()->user()->mobile }}"
                                                aria-describedby="basic-addon-mobile"
                                                required
                                            />
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label class="form-label" for="basic-addon-whatsapp">Whatsapp</label>

                                            <input
                                                type="text"
                                                id="basic-addon-whatsapp"
                                                class="form-control"
                                                placeholder="Whatsapp"
                                                aria-label="whatsapp"
                                                name="whatsapp"
                                                value="{{ auth()->user()->whatsapp }}"
                                                required
                                                aria-describedby="basic-addon-whatsapp"
                                            />
                                        </div>


                                    </div>

                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class="form-label" for="basic-addon-facebook">Facebook</label>

                                            <input
                                                type="text"
                                                id="basic-addon-facebook"
                                                class="form-control"
                                                placeholder="Facebook"
                                                aria-label="facebook"
                                                name="facebook"
                                                value="{{ auth()->user()->facebook }}"
                                                aria-describedby="basic-addon-facebook"
                                            />
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label class="form-label" for="basic-addon-instagram">Instagram</label>

                                            <input
                                                type="text"
                                                id="basic-addon-instagram"
                                                class="form-control"
                                                placeholder="Instagram"
                                                aria-label="instagram"
                                                name="instagram"
                                                value="{{ auth()->user()->instagram }}"
                                                aria-describedby="basic-addon-instagram"
                                            />
                                        </div>
                                    </div>

                                    <div class="form-group ">
                                        <label class="form-label" for="basic-addon-address">Address</label>

                                        <input
                                            type="text"
                                            id="basic-addon-address"
                                            class="form-control"
                                            placeholder="Address"
                                            aria-label="address"
                                            name="address"
                                            value="{{ auth()->user()->address }}"
                                            aria-describedby="basic-addon-address"
                                        />
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <button type="submit" id="submit" class="btn btn-primary">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                    </div><!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    @endauth
@endsection
@push('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <!-- DataTables  & Plugins -->
{{--    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>--}}
{{--    <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>--}}
{{--    <script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>--}}
{{--    <script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>--}}
{{--    <script src="{{ asset('plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>--}}
{{--    <script src="{{ asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>--}}
{{--    <script src="{{ asset('plugins/jszip/jszip.min.js') }}"></script>--}}
{{--    <script src="{{ asset('plugins/pdfmake/pdfmake.min.js') }}"></script>--}}
{{--    <script src="{{ asset('plugins/pdfmake/vfs_fonts.js') }}"></script>--}}
{{--    <script src="{{ asset('plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>--}}
{{--    <script src="{{ asset('plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>--}}
{{--    <script src="{{ asset('plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>--}}
    <!-- AdminLTE for demo purposes -->
    {{--    <script src="dist/js/demo.js"></script>--}}
    <!-- Page specific script -->
{{--    <script>--}}
{{--        $(function () {--}}
{{--            $("#example1").DataTable({--}}
{{--                "responsive": true, "lengthChange": false, "autoWidth": false,--}}
{{--                "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]--}}
{{--            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');--}}
{{--            // $('#tbl-pieces').DataTable({--}}
{{--            //     "paging": true,--}}
{{--            //     "lengthChange": true,--}}
{{--            //     "searching": true,--}}
{{--            //     "ordering": true,--}}
{{--            //     "info": true,--}}
{{--            //     "autoWidth": false,--}}
{{--            //     "responsive": true,--}}
{{--            // });--}}
{{--        });--}}
{{--    </script>--}}
{{--    <script>--}}

{{--        $("#list_order").addClass('active');--}}
{{--        $("#list_order").parent().parent().parent().addClass('menu-open');--}}
{{--        $.ajaxSetup({--}}
{{--            headers: {--}}
{{--                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')--}}
{{--            }--}}
{{--        });--}}

{{--        $("form").on("submit", function(event){--}}

{{--            event.preventDefault();--}}

{{--            let formData = new FormData(this);--}}
{{--            var formValues= $(this).serialize();--}}

{{--            console.log(formData);--}}
{{--            $.ajax({--}}
{{--                url: '{{ route('store_order') }}',--}}
{{--                type: 'POST',--}}
{{--                async: true,--}}
{{--                cache: false,--}}
{{--                data: formData,--}}
{{--                contentType: false,--}}
{{--                processData: false,--}}
{{--                success: function (response) {--}}
{{--                    printMsg(response);--}}

{{--                    console.log(response.info);--}}
{{--                    $('tbody').append('<tr>' +--}}
{{--                        '<td>'+response.info.count_box+'</td>'+--}}
{{--                        '<td>'+response.info.price+'</td>'+--}}
{{--                        '<td>'+response.info.price_deliver+'</td>'+--}}
{{--                        '<td>'+response.info.order_type +'</td>'+--}}
{{--                        '<td> </td>'+--}}
{{--                        '<td>'+response.zone +'</td>'+--}}
{{--                        '<td>'+response.state +'</td>'+--}}
{{--                        // '<td>'+response.info.zone +'</td>'+--}}
{{--                        '<td><a href="#" data-key="'+response.id+'" class="dropdown-item delete-record">'--}}

{{--                        +'</tr>');--}}
{{--                },--}}
{{--                error: function () {--}}
{{--                    $('.status').append('حصل خطأ أثناء الاتصال بالانترنت');--}}
{{--                },--}}
{{--            })--}}
{{--        });--}}
{{--        function printMsg (msg) {--}}
{{--            if($.isEmptyObject(msg.error)){--}}
{{--                console.log(msg.success);--}}
{{--                toastr.success(" Your work has been saved", "Success");--}}
{{--                $('input').val('') ;--}}
{{--            }else{--}}
{{--                $.each( msg.error, function( key, value ) {--}}
{{--                    $('.status').text(value).css('color','red');--}}
{{--                });--}}
{{--            }--}}
{{--        }--}}
{{--        $.ajaxSetup({--}}
{{--            headers: {--}}
{{--                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')--}}
{{--            }--}}
{{--        });--}}

{{--        $('.delete-record').click(function (e) {--}}
{{--            e.preventDefault();--}}
{{--            let id = $(this).data("key");--}}
{{--            console.log(id);--}}
{{--            $.ajax({--}}
{{--                url: '{{ route('sdelete_order') }}',--}}
{{--                type: 'POST',--}}
{{--                async: true,--}}
{{--                cache: false,--}}
{{--                data: {--}}
{{--                    'id':id--}}
{{--                },--}}
{{--                success: function (response) {--}}
{{--                    toastr.success(" Your work has been saved", "Success");--}}
{{--                    $('.row_'+id).hide();--}}

{{--                },--}}
{{--                error: function () {--}}
{{--                    toastr.error(" Your work has been not saved", "Error");--}}
{{--                },--}}
{{--            })--}}
{{--        });--}}
{{--    </script>--}}
@endpush
