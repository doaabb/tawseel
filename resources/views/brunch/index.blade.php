{{--@auth()--}}
{{--    @if(auth()->user()->hasRole('super_admin') != null )--}}
@extends('new_layout.app')

@section('title', 'Create & List Orders')
{{--@section('page_title','Create New Order')--}}
@push('style')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

    <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
@endpush
@section('content')

    @role('admin|super_admin|brunch')
    <!-- /.content -->
    <div class="col-12">

        <!-- /.card -->

        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Orders </h3>
            </div>
            <!-- card-header -->
            <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        {{--                            <th >id</th>--}}
                        <th>Dealer Name</th>
                        <th >Count Boxes</th>
                        <th >Price</th>
                        <th >Price Deliver</th>
                        <th >Order Type</th>
                        <th >Zone</th>
                        <th >State</th>
                        <th >View</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach( $order as $index=>$orders)
                        <tr class="row_{{ $orders->id }}">
                            {{--                                <td >{{ $orders->id}}</td>--}}
                            <td>{{ $orders->dealer->name}}</td>
                            <td>{{ $orders->count_box}}</td>
                            <td>{{ $orders->price}}</td>
                            <td>{{ $orders->price_deliver}}</td>
                            <td>{{ $orders->order_type}}</td>
                            <td>{{ $orders->zone? $orders->zone->name: ''}}</td>
                            <td>{{ $orders->state->name}}</td>
                            {{--                            <td>--}}
                            {{--                                <div class="d-inline-flex">--}}
                            {{--                                    <a class="pr-1 dropdown-toggle hide-arrow text-primary"--}}
                            {{--                                       data-toggle="dropdown">--}}

                            {{--                                        <i data-feather="more-vertical"></i>--}}
                            {{--                                        <i class="more-vertical"></i>--}}
                            {{--                                    </a>--}}
                            {{--                                    <div class="dropdown-menu dropdown-menu-right">--}}
                            {{--                                        <a href="javascript:;" class="dropdown-item">--}}
                            {{--                                            <i class="file-text"></i>View Users</a>--}}
                            {{--                                        <a href="{{ route('to_representative',['id'=>$orders->id]) }}" class="dropdown-item send-record" data-key="{{ $orders->id }}">--}}
                            {{--                                            Send To Representative</a>--}}

                              <td ><a href="{{ route('brunch_order',['id'=>$orders->id ]) }}"
                                    class="badge bg-primary">

                                    View
                                </a></td>

                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                                <a href="javascript:;" class="item-edit">--}}
                            {{--                                    <i class="icon-wrapper"></i></a>--}}
                            {{--                            </td>--}}

                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        {{--                            <th >id</th>--}}
                        <th>Dealer Name</th>
                        <th >Count Boxes</th>
                        <th >Price</th>
                        <th >Price Deliver</th>
                        <th >Order Type</th>
                        <th >Zone</th>
                        <th >State</th>
                        <th >View</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->

    @endrole
@endsection

@push('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <!-- DataTables  & Plugins -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    {{--    <script src="dist/js/demo.js"></script>--}}
    <!-- Page specific script -->
    <script>
        $(function () {
            $("#example1").DataTable({
                "responsive": true, "lengthChange": false, "autoWidth": false,
                "buttons": ["copy", "csv", "excel", "pdf", "print"]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
            // $('#tbl-pieces').DataTable({
            //     "paging": true,
            //     "lengthChange": true,
            //     "searching": true,
            //     "ordering": true,
            //     "info": true,
            //     "autoWidth": false,
            //     "responsive": true,
            // });
        });
    </script>
    <script>

        $("#list_order").addClass('active');
        $("#list_order").parent().parent().parent().addClass('menu-open');
        {{--$.ajaxSetup({--}}
        {{--    headers: {--}}
        {{--        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')--}}
        {{--    }--}}
        {{--});--}}

        {{--$("form").on("submit", function(event){--}}

        {{--    event.preventDefault();--}}

        {{--    let formData = new FormData(this);--}}
        {{--    var formValues= $(this).serialize();--}}

        {{--    console.log(formData);--}}
        {{--    $.ajax({--}}
        {{--        url: '{{ route('store_order') }}',--}}
        {{--        type: 'POST',--}}
        {{--        async: true,--}}
        {{--        cache: false,--}}
        {{--        data: formData,--}}
        {{--        contentType: false,--}}
        {{--        processData: false,--}}
        {{--        success: function (response) {--}}
        {{--            printMsg(response);--}}

        {{--            console.log(response.info);--}}
        {{--            $('tbody').append('<tr>' +--}}
        {{--                '<td>'+response.info.count_box+'</td>'+--}}
        {{--                '<td>'+response.info.price+'</td>'+--}}
        {{--                '<td>'+response.info.price_deliver+'</td>'+--}}
        {{--                '<td>'+response.info.order_type +'</td>'+--}}
        {{--                '<td> </td>'+--}}
        {{--                '<td>'+response.zone +'</td>'+--}}
        {{--                '<td>'+response.state +'</td>'+--}}
        {{--                // '<td>'+response.info.zone +'</td>'+--}}
        {{--                '<td><a href="#" data-key="'+response.id+'" class="dropdown-item delete-record">'--}}

        {{--                +'</tr>');--}}
        {{--        },--}}
        {{--        error: function () {--}}
        {{--            $('.status').append('حصل خطأ أثناء الاتصال بالانترنت');--}}
        {{--        },--}}
        {{--    })--}}
        {{--});--}}
        {{--function printMsg (msg) {--}}
        {{--    if($.isEmptyObject(msg.error)){--}}
        {{--        console.log(msg.success);--}}
        {{--        toastr.success(" Your work has been saved", "Success");--}}
        {{--        $('input').val('') ;--}}
        {{--    }else{--}}
        {{--        $.each( msg.error, function( key, value ) {--}}
        {{--            $('.status').text(value).css('color','red');--}}
        {{--        });--}}
        {{--    }--}}
        {{--}--}}
        {{--$.ajaxSetup({--}}
        {{--    headers: {--}}
        {{--        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')--}}
        {{--    }--}}
        {{--});--}}

        {{--$('.delete-record').click(function (e) {--}}
        {{--    e.preventDefault();--}}
        {{--    let id = $(this).data("key");--}}
        {{--    console.log(id);--}}
        {{--    $.ajax({--}}
        {{--        url: '{{ route('sdelete_order') }}',--}}
        {{--        type: 'POST',--}}
        {{--        async: true,--}}
        {{--        cache: false,--}}
        {{--        data: {--}}
        {{--            'id':id--}}
        {{--        },--}}
        {{--        success: function (response) {--}}
        {{--            toastr.success(" Your work has been saved", "Success");--}}
        {{--            $('.row_'+id).hide();--}}

        {{--        },--}}
        {{--        error: function () {--}}
        {{--            toastr.error(" Your work has been not saved", "Error");--}}
        {{--        },--}}
        {{--    })--}}
        {{--});--}}
        {{--$('.active-record').click(function (e) {--}}
        {{--    e.preventDefault();--}}
        {{--    let id = $(this).data("key");--}}
        {{--    console.log(id);--}}
        {{--    $.ajax({--}}
        {{--        url: '{{ route('dis_active') }}',--}}
        {{--        type: 'POST',--}}
        {{--        async: true,--}}
        {{--        cache: false,--}}
        {{--        data: {--}}
        {{--            'id':id--}}
        {{--        },--}}
        {{--        success: function (response) {--}}
        {{--            toastr.success(" Your work has been saved", "Success");--}}
        {{--            $('.status_'+id).html('تم توصيله');--}}

        {{--        },--}}
        {{--        error: function () {--}}
        {{--            toastr.error(" Your work has been not saved", "Error");--}}
        {{--        },--}}
        {{--    })--}}
        {{--});--}}
    </script>
@endpush

{{--@else--}}
{{--@endif--}}
{{--@endauth--}}
