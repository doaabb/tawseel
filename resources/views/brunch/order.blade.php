{{--@auth()--}}
{{--    @if(auth()->user()->hasRole('super_admin') != null )--}}
@extends('new_layout.app')

@section('title', 'View Order')
{{--@section('page_title','Create New Order')--}}
@push('style')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

    <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
@endpush
@section('content')

    @role('admin|super_admin|brunch')

    <!-- /.content -->
    <div class="col-12">


        <div class="modal fade" id="modal-xl" role="dialog" >
            <div class="modal-dialog modal-xl  modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">transfer</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        <form class="form-horizontal col-md-12" id="create" METHOD="post" action="#"
                              enctype="multipart/form-data" autocomplete="on"
                        >
                            {{ csrf_field()}}
                            {{--                        @dd($id->media)--}}
                            <input type="hidden" name="id" value="" id="order_id">

                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="form-label" for="basic-addon-count_box">
                                     From   Governorate && State
                                    </label>


                                    <select class="form-control select2" name="state_id_shipper" id="state_id" style="width: 100%;">
                                        @foreach(\App\Models\Governorate::all() as $gover)
                                            <optgroup label="{{ $gover->name }}">
                                                @foreach($gover->state as $state)
                                                    <option value="{{ $state->id }}">{{ $state->name }}</option>
                                                @endforeach
                                            </optgroup>

                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="form-label" for="basic-addon-count_box">
                                     to   Governorate && State
                                    </label>


                                    <select class="form-control select2" name="state" id="state_id" style="width: 100%;">
                                        @foreach(\App\Models\Governorate::all() as $gover)
                                            <optgroup label="{{ $gover->name }}">
                                                @foreach($gover->state as $state)
                                                    <option value="{{ $state->name }}">{{ $state->name }}</option>
                                                @endforeach
                                            </optgroup>

                                        @endforeach
                                    </select>
                                </div>
                            </div>

                             <div class="modal-footer justify-content-between">
                                {{--                                <button type="button" class="btn btn-primary">Save changes</button>--}}
                                <div class="row">
                                    <div class="col-12">
                                        <button type="submit" id="submit" class="btn btn-primary">Submit</button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

        <!-- /.card -->

        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Order for {{ $order->dealer_name }} </h3>

            </div>
            <!-- card-header -->
            <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        {{--                            <th >id</th>--}}
                        <th >Delivery Name</th>
                        <th >Delivery Number</th>
                        <th >Dealer Name</th>
                        <th >Data Entry Name</th>
                        {{--                        <th >View</th>--}}
                        <th >Code </th>
                        <th >CC</th>
                        <th >transfer</th>
                        <th >price order </th>
                        {{--                        <th >State</th>--}}
                        {{--                        <th >Action</th>--}}
                    </tr>
                    </thead>
                    <tbody>
                    @php $price = 0; @endphp
                    @foreach( $order->packages as $index=>$orders)
                        {{--                                            @dd($orders)--}}
                        <tr class="row_{{ $orders->id }}">
                            <td>{{ $order->representative?$order->representative->name: ''}}</td>
                            <td>{{ $order->representative?$order->representative->mobile: ''}}</td>
                            <td>{{ $order->dealer?$order->dealer->name: ''}}</td>
                            <td>{{ $orders->user?$orders->user->name: ''}}</td>
                            <td>{{ $orders->code}}</td>
                            <td>{{ $orders->cc}}</td>
                            <td>{{ $orders->total_price}}</td>
                                                        <td >
                                                            <a  class="send-record"
                                                                data-toggle="modal"    data-target="#modal-xl"
                                                                href="#" data-key="{{ $orders->id }}"
                                                                class="badge bg-info delete-record">
                                                                transfer
                                                            </a>
                                                        </td>

                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->

    @endrole
@endsection

@push('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <!-- DataTables  & Plugins -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    {{--    <script src="dist/js/demo.js"></script>--}}
    <!-- Page specific script -->
    <script>
        $(function () {
            $("#example1").DataTable({
                "responsive": true, "lengthChange": false, "autoWidth": false,
                "buttons": ["copy", "csv", "excel", "pdf", "print"]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
            // $('#tbl-pieces').DataTable({
            //     "paging": true,
            //     "lengthChange": true,
            //     "searching": true,
            //     "ordering": true,
            //     "info": true,
            //     "autoWidth": false,
            //     "responsive": true,
            // });
        });
        $('.send-record').click(function (){

            let id = $(this).data("key");
            $('#order_id').val(id);
        });
    </script>
    <script>

        $("#list_order").addClass('active');
        $("#list_order").parent().parent().parent().addClass('menu-open');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $("form").on("submit", function(event){

            event.preventDefault();

            let formData = new FormData(this);
            var formValues= $(this).serialize();

            // console.log(formData);
            $.ajax({
                url: '{{ route('transfer_order') }}',
                type: 'POST',
                async: true,
                cache: false,
                data: formData,
                contentType: false,
                processData: false,
                success: function (response) {
                    printMsg(response);

                },
                error: function (msg) {
                    $.each( msg.error, function( key, value ) {
                        toastr.error(value, "Danger");
                    });
                },
            })
        });
        function printMsg (msg) {
            if($.isEmptyObject(msg.error)){
                console.log(msg.success);
                toastr.success(" Your work has been saved", "Success");
                $('input').val('') ;
            }else{
                $.each( msg.error, function( key, value ) {
                    $('.status').text(value).css('color','red');
                });
            }
        }
    </script>
@endpush

{{--@else--}}
{{--@endif--}}
{{--@endauth--}}
