@extends('new_layout.app')

@section('title', 'Dashboard')

@push('style')

    <style>

        tr.title-cod>td {
            height: 50px;
            font-size: 20px;
        }
        tr.content-code>td {
            border: 1px solid #000;
        }
        td.cod {
            width: 120px;
        }
        td.cc {
            width: 100px;
        }
        td.total {
            width: 120px;
        }
        td.driver {
            width: 120px;
        }
        td.picket {
            width: 120px;
        }
        td.content-cod {
            width: 120px;
        }
        tr.description-package>td {
            border: 1px solid #000;
            padding: 2%;
        }
        td.shipper {
            padding-top: 3%;
            padding-left: 1%;
        }

        td.receiver {
            padding-top: 3%;
            padding-left: 1%;
        }
        td.barcode {
            transform: rotate(
                90deg
            );
            padding-top: 12%;
            text-align: left;
            padding-left: 0%;
            position: absolute;
            right: 0%;
        }
        table {
            margin : auto;
            size: 148mm 210mm !important;
        }

        td.tel_shipper {
            border: 1px solid #000;
        }
        td.state_shipper {
            border: 1px solid #000;
        }
        td.tel_receiver {
            border: 1px solid #000;
        }
        td.state_receiver {
            border: 1px solid #000;
        }
        /*tr.from-to>td {*/
        /*    border: 1px solid #000;*/
        /*}*/
    </style>
@endpush
@section('content')



    <!-- Complex Headers -->

    <div class="col-12" style="size:A5;">

        <!-- /.card -->

        <div class="card">
            <div class="card-header">
                <h3 class="card-title"><a href="{{ route('download_pdf',$order->first()->order->id) }}">{{ $order->first()->order->id }}</a> </h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row" style="">

                    @foreach($order as $id)
                    <table>

                        <thead>
                        <tr class="logo">
                            <td> <img src=""></td>
                            <td> #وصل_لي_وين_ماكنت</td>
                        </tr>
                        <tr class="title-cod">
                            <td class="cod">COD</td>
                            <td class="cc">CC</td>
                            <td class="total">Total Price</td>
                            <td class="driver" colspan="3">Driver</td>
                            <td class="picket" colspan="3">Picket By</td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="content-code" style="min-width:40px;     height: 50px !important;">
                            <td class="content-cod">{{ $id->code }}</td>
                            <td class="content-cc">{{ $id->cc }}</td>
                            <td class="content-total">{{ $id->total_price }}</td>
                            <td class="content-driver" colspan="3">{{ $id->driver }}</td>
                            <td class="content-picket" colspan="3">{{ $id->picket_by }}</td>
                        </tr>
                        <tr class="description-package" style="height: 50px;">
                            <td colspan="3" class="content-picket-date">Pickup Date: {{ $id->pickup_date }}</td>
                            <td class="content-pieces" colspan="3">Pieces: {{ $id->pieces }}</td>
                            <td class="content-weight" colspan="3">Weight: {{ $id->weight }}</td>
                        </tr>

                        {{--'barcode', 'random_number', '',
                                'driver_id', 'delivery_id',
                                'delivery', 'price_customer', 'price_deliver', 'price_cancel',--}}
                        <tr>
                            <td class="shipper" colspan="7">
                               <b>Shipper</b>
                            </td>
                        </tr>
                        <tr class="from-to">
                            <td class="state_shipper" colspan="4">From:
                                {{ $id->state_shipper? $id->state_shipper->name :''}}</td>
                            <td class="tel_shipper" colspan="3">Tel: {{ $id->tel_shipper }}</td>
                            <td class="barcode" colspan="1" rowspan="5">
                                <p style="    text-align: center;   padding: 0%;" >{{ $id->random_number }}</p>
                                <img src="{{ asset($id->path_barcode) }}"   style=" width: 282px;    height: 40px;" >
                            </td>
                        </tr>
                        <tr>
                            <td class="receiver" colspan="7">
                              <b>Receiver</b>
                            </td>
                        </tr>
                        <tr  class="from-to">
                            <td class="state_receiver" colspan="4">To:
                                {{ $id->state_receiver?$id->state_receiver->name:'' }}</td>
                            <td class="tel_receiver" colspan="3">Tel: {{ $id->tel_receiver }}</td>
                        </tr>
                        <tr style="height:12px;"></tr>
                        <tr  class="qr-code">
                            <td  colspan="2" style="padding: 3%;">
                                <?php echo DNS2D::getBarcodeHTML('2021', 'QRCODE'); ?></td>
                            <td class="tel_receiver" colspan="5"></td>
                        </tr>
                        <tr>
                            <td colspan="7">
                                <b> Contact: +968 94058330 / +968 94375710 / instagram : @wasel.lee.om </b>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    @endforeach
                </div>
                <!-- /.card-body -->
                {{--                    <div class="card-footer">--}}
                {{--                        .--}}
                {{--                    </div>--}}
            </div>
            <!-- /.card -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->

@stop
@push('script')

@endpush
