@extends('new_layout.app')

@section('title', 'To')
{{--@section('page_title','List State')--}}

@push('style')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <link rel="stylesheet" href="{{ asset('plugins/ekko-lightbox/ekko-lightbox.css') }}">
@endpush
@section('content')


    @role('admin|super_admin|data_entry')

    <!-- Complex Headers -->

    <div class="col-12">

        <!-- /.card -->

        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Order Number {{ $order->id }} </h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">


                    <div class="status">

                    </div>
                    <form class="form-horizontal col-md-12" id="create" METHOD="post" action="#"
                          enctype="multipart/form-data" autocomplete="on"
                    >
                        {{ csrf_field()}}

                        <input type="hidden" name="id" value="{{ $order->id }}">

                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label class="form-label" for="basic-addon-total_price">
                                        Count  Page
                                    </label>

                                    <input
                                        type="number"
                                        id="basic-addon-total_price"
                                        class="form-control"
                                        placeholder="Total Price"
                                        aria-label="Total Price"
                                        name="number"
                                        aria-describedby="basic-addon-total_price"
                                        required
                                    />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <button type="submit" id="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                    </form>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->


    @endrole
@stop
@push('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <!-- Ekko Lightbox -->
    <script src="{{ asset('plugins/ekko-lightbox/ekko-lightbox.min.js') }}"></script>
    <script>
        $(function () {
            $(document).on('click', '[data-toggle="lightbox"]', function(event) {
                event.preventDefault();
                $(this).ekkoLightbox({
                    alwaysShowClose: true
                });
            });

            // $('.filter-container').filterizr({gutterPixels: 3});
            // $('.btn[data-filter]').on('click', function() {
            //     $('.btn[data-filter]').removeClass('active');
            //     $(this).addClass('active');
            // });
        })
    </script>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $("form").on("submit", function(event){

            event.preventDefault();

            let formData = new FormData(this);
            var formValues= $(this).serialize();

            // console.log(formData);
            $.ajax({
                url: '{{ route('save_print',['id'=>$order->id]) }}',
                type: 'POST',
                async: true,
                cache: false,
                data: formData,
                contentType: false,
                processData: false,
                success: function (response) {
                    printMsg(response);
                    if(response.status_code === 200){
                        window.location.replace("{{ route('print',['id'=>$order->id]) }}");
                    }
                },
                error: function (msg) {
                    $.each( msg.error, function( key, value ) {
                        toastr.error(value, "Danger");
                    });
                },
            })
        });
        function printMsg (msg) {
            if($.isEmptyObject(msg.error)){
                console.log(msg.success);
                toastr.success(" Your work has been saved", "Success");
                $('input').val('') ;

            }else{
                $.each( msg.error, function( key, value ) {
                    toastr.error(value, "Danger");
                });
            }
        }
    </script>
@endpush
