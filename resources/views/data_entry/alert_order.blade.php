{{--@auth()--}}
{{--    @if(auth()->user()->hasRole('super_admin') != null )--}}
@extends('new_layout.app')

@section('title', 'Alert Orders')
{{--@section('page_title','Create New Order')--}}
@push('style')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <link rel="stylesheet" href="{{ asset('plugins/ekko-lightbox/ekko-lightbox.css') }}">

    <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
    <style>
        .filtr-item {
            text-align: center;
        }
        .text-primary {
            color: #32f900!important;
            font-size: 1.5rem;
        }
    </style>
    <style>
        .zoomify{cursor:pointer;cursor:-webkit-zoom-in;cursor:zoom-in}.zoomify.zoomed{cursor:-webkit-zoom-out;cursor:zoom-out;padding:0;margin:0;border:none;border-radius:0;box-shadow:none;position:relative;z-index:1501}.zoomify-shadow{position:fixed;top:0;left:0;right:0;bottom:0;width:100%;height:100%;display:block;z-index:1500;background:rgba(0,0,0 ,.3);opacity:0}.zoomify-shadow.zoomed{opacity:1;cursor:pointer;cursor:-webkit-zoom-out;cursor:zoom-out}
    </style>
@endpush
@section('content')

    @role('admin|super_admin|data_entry')
    <!-- /.content -->
    <div class="col-12">
    {{--    echo DNS1D::getBarcodeHTML('12345678', 'C39');--}}
    {{--echo DNS1D::getBarcodePNGPath('12345678', 'C39',3,33);--}}
    {{--echo DNS1D::getBarcodeHTML('4445645656', 'C93');--}}
    {{--@php--}}


    {{--@endphp--}}
    {{--        \Storage::disk('public')->put('test.png',base64_decode(DNS2D::getBarcodePNG("4", "PDF417")));--}}

    <!-- /.card -->
        {{--echo '<img src="data:image/png;base64,' . DNS2D::getBarcodePNG('4', 'PDF417') . '" alt="barcode"   />';--}}
        {{--    <img src="data:image/png;base64,{{DNS1D::getBarcodePNG('11', 'C39')}}" alt="barcode" /><br><br>--}}
        {{--    <img src="data:image/png;base64,{{DNS1D::getBarcodePNG('123456789', 'C39+',1,33,array(0,255,0), true)}}" alt="barcode" /><br><br>--}}
        {{--    <img src="data:image/png;base64,{{DNS1D::getBarcodePNG('4', 'C39+',3,33,array(255,0,0))}}" alt="barcode" /><br><br>--}}
        {{--    <img src="data:image/png;base64,{{DNS1D::getBarcodePNG('12', 'C39+')}}" alt="barcode" /><br><br>--}}
        {{--    <img src="data:image/png;base64,{{DNS1D::getBarcodePNG('23', 'POSTNET')}}" alt="barcode" /><br/><br/>--}}

        <div class="modal fade" id="modal-xl" role="dialog" >
            <div class="modal-dialog modal-xl  modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Enter</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        <form class="form-horizontal col-md-12" id="create" METHOD="post" action="#"
                              enctype="multipart/form-data" autocomplete="on"
                        >
                            {{ csrf_field()}}
                            {{--                        @dd($id->media)--}}
                            <input type="hidden" name="id" value="" id="order_id">
                            <input type="hidden" name="image_id" value="" id="image_id">
                            <div class="">
                                {{--                                @foreach($id->media as $image)--}}
                                <div class="form-group " style="">
                                    <label class="form-label" for="basic-addon-client_phone">
                                        Image
                                    </label>
                                    <div class="filtr-item " style="text-align: center;" data-category="1" >
                                        {{--                                            <a href="" id="image_a" data-toggle="lightbox" >--}}
                                        <img id="image_src" src="" class="img-fluid mb-2" style="width:200px; height:150px;"/>
                                        {{--                                            </a>--}}
                                    </div>
                                </div>
                                {{--                                @endforeach--}}
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="form-label" for="basic-addon-code">
                                        Code
                                    </label>

                                    <input
                                        type="text"
                                        id="basic-addon-code"
                                        class="form-control"
                                        placeholder="Code"
                                        aria-label="Code"
                                        name="code"
                                        aria-describedby="basic-addon-code"
                                        required
                                    />
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="form-label" for="basic-addon-cc">
                                        CC
                                    </label>

                                    <input
                                        type="text"
                                        id="basic-addon-cc"
                                        class="form-control"
                                        placeholder="CC"
                                        aria-label="CC"
                                        aria-describedby="basic-addon-cc"
                                        name="cc"
                                        required
                                    />
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="form-label" for="basic-addon-total_price">
                                        Total Price
                                    </label>

                                    <input
                                        type="text"
                                        id="basic-addon-total_price"
                                        class="form-control"
                                        placeholder="Total Price"
                                        aria-label="Total Price"
                                        name="total_price"
                                        aria-describedby="basic-addon-total_price"
                                        required
                                    />
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="form-label" for="basic-addon-driver">
                                        Driver
                                    </label>

                                    <select class="form-control" name="driver_id" >
                                        <option></option>
                                        @foreach($driver as $user)
                                            <option value="{{$user->id}}">{{ $user->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="form-label" for="basic-addon-picket_by">
                                        Picket By
                                    </label>

                                    <input
                                        type="text"
                                        id="basic-addon-picket_by"
                                        class="form-control"
                                        placeholder="Picket By"
                                        aria-label=" Picket By"
                                        name="picket_by"
                                        aria-describedby="basic-addon-Picket By"
                                        required
                                    />
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="form-label" for="basic-addon-pickup_date">
                                        pickup_date
                                    </label>

                                    <input
                                        type="datetime-local"
                                        id="basic-addon-pickup_date"
                                        class="form-control"
                                        placeholder="pickup_date"
                                        aria-label="pickup_date"
                                        aria-describedby="basic-addon-pickup_date"
                                        name="pickup_date"
                                        required
                                    />
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="form-label" for="basic-addon-pieces">
                                        pieces
                                    </label>

                                    <input
                                        type="number"
                                        id="basic-addon-pieces"
                                        class="form-control"
                                        placeholder="pieces"
                                        aria-label="pieces"
                                        name="pieces"
                                        aria-describedby="basic-addon-pieces"
                                        required
                                    />
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="form-label" for="basic-addon-weight">
                                        Weight
                                    </label>

                                    <input
                                        type="text"
                                        id="basic-addon-weight"
                                        class="form-control"
                                        placeholder="weight"
                                        aria-label="weight"
                                        name="weight"
                                        aria-describedby="basic-addon-weight"
                                        required
                                    />
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="form-label" for="basic-addon-tel_shipper">
                                        Tel Shipper
                                    </label>

                                    <input
                                        type="text"
                                        id="basic-addon-tel_shipper"
                                        class="form-control"
                                        placeholder="tel_shipper"
                                        aria-label="tel_shipper"
                                        name="tel_shipper"
                                        aria-describedby="basic-addon-tel_shipper"
                                        required
                                    />
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="form-label" for="basic-addon-count_box">
                                        Governorate && State Shiper
                                    </label>


                                    <select class="form-control select2" name="state_id_shipper" id="state_id" style="width: 100%;">
                                        @foreach($gov as $gover)
                                            <optgroup label="{{ $gover->name }}">
                                                @foreach($gover->state as $state)
                                                    <option value="{{ $state->id }}">{{ $state->name }}</option>
                                                @endforeach
                                            </optgroup>

                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="form-label" for="basic-addon-tel_receiver">
                                        Tel Receiver
                                    </label>

                                    <input
                                        type="text"
                                        id="basic-addon-tel_receiver"
                                        class="form-control"
                                        placeholder="tel_receiver"
                                        aria-label="tel_shipper"
                                        name="tel_receiver"
                                        aria-describedby="basic-addon-tel_receiver"
                                        required
                                    />
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="form-label" for="basic-addon-count_box">
                                        Governorate && State Reciever
                                    </label>


                                    <select class="form-control select2" name="state_id_receiver" id="state_id_receiver" style="width: 100%;">
                                        @foreach($gov as $gover)
                                            <optgroup label="{{ $gover->name }}">
                                                @foreach($gover->state as $state)
                                                    <option value="{{ $state->id }}">{{ $state->name }}</option>
                                                @endforeach
                                            </optgroup>

                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="modal-footer justify-content-between">
                                <div class="row">
                                    <div class="col-12">
                                        <button type="submit" id="submit" class="btn btn-primary">Submit</button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Orders </h3>
            </div>
            <!-- card-header -->
            <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        {{--                            <th >id</th>--}}
                        <th>Dealer Name</th>
                        <th>Dealer Mobile</th>
                        <th>Representative Name</th>
                        <th >Count Boxes</th>
                        <th >Action</th>
                        <th >Print</th>
                        <th >Price</th>
                        <th >Price Deliver</th>
                        <th >Order Type</th>
                        <th >Order Status</th>
                        <th >State</th>
                        <th >Zone</th>
                    </tr>
                    </thead>
                    <tbody>
{{--                    @php $alert_order  @endphp--}}
                    @foreach( $order as $index=>$orders)
                        <tr class="row_{{ $orders->id }}">
                            <td>{{ $orders->dealer->name}}</td>
                            <td><a href="tel://{{ $orders->dealer->mobile}}">Call</a> </td>
                            <td>{{ $orders->representative?$orders->representative->name:''}}</td>
                            <td>{{ $orders->count_box}}</td>
                            <td>
                                <div class="d-inline-flex">
                                    <a class="pr-1 dropdown-toggle hide-arrow text-primary"
                                       data-toggle="dropdown">
                                        <i data-feather="more-vertical"></i>
                                        <i class="more-vertical"></i>
                                    </a>
                                    <div class="dropdown-menu state dropdown-menu-right">
                                        @foreach($orders->media->where('alert_entry','=',1) as $image)
                                            <img class="send-record"  data-toggle="modal"
                                                 data-key="{{ $orders->id }}"
                                                 data-id="{{ $image->id }}"
                                                 data-gallery="{{ str_replace('uploads/','',$image->file_path) }}"
                                                 data-target="#modal-xl"
                                                 src="{{ str_replace('uploads/','',$image->file_path) }}" width="100">
                                            @if(\App\Models\Packages::where('media_id',$image->id)->first()!=null )
                                                <span class="fa fa-check"></span>
                                            @else
                                                <span class="fa fa-clock" id="clock_{{ $image->id }}"></span>
                                            @endif
                                            {{--                                        </a>--}}
                                            <br>
                                        @endforeach
                                    </div>
                                </div>
                                <a href="javascript:;" class="item-edit">
                                    <i class="icon-wrapper"></i></a>
                            </td>
                            <td><a class="badge badge-success" href="{{ route('setting_print',['id'=>$orders->id]) }}">Print</a></td>
                            <td>{{ $orders->price}}</td>
                            <td>{{ $orders->price_deliver}}</td>
                            <td>{{ $orders->order_type}}</td>
                            <td>{{ $orders->order_status}}</td>
                            <td>{{ $orders->state->name}}</td>

                            <td>{{ $orders->zone?$orders->zone->name:''}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        {{--                            <th >id</th>--}}
                        <th>Dealer Name</th>
                        <th>Dealer Mobile</th>
                        <th>Representative Name</th>
                        <th >Count Boxes</th>
                        <th >Action</th>
                        <th >Print</th>
                        <th >Price</th>
                        <th >Price Deliver</th>
                        <th >Order Type</th>
                        <th >Order Status</th>
                        <th >State</th>
                        <th >Zone</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->

    @endrole
@endsection

@push('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <!-- DataTables  & Plugins -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
    <script>/*! Zoomify - v0.2.5 - https://github.com/indrimuska/zoomify - (c) 2015 Indri Muska - MIT */

        !function(z){Zoomify=function(o,i){var t=this;this._zooming=!1,this._zoomed=!1,this._timeout=null,this.$shadow=null,this.$image=z(o).addClass("zoomify"),this.options=z.extend({},Zoomify.DEFAULTS,this.$image.data(),i),this.$image.on("click",function(){t.zoom()}),z(window).on("resize",function(){t.reposition()}),z(document).on("scroll",function(){t.reposition()}),z(window).on("keyup",function(o){t._zoomed&&27==o.keyCode&&t.zoomOut()})},Zoomify.DEFAULTS={duration:200,easing:"linear",scale:.9},Zoomify.prototype.transition=function(o,i){o.css({"-webkit-transition":i,"-moz-transition":i,"-ms-transition":i,"-o-transition":i,transition:i})},Zoomify.prototype.addTransition=function(o){this.transition(o,"all "+this.options.duration+"ms "+this.options.easing)},Zoomify.prototype.removeTransition=function(o,i){var t=this;clearTimeout(this._timeout),this._timeout=setTimeout(function(){t.transition(o,""),z.isFunction(i)&&i.call(t)},this.options.duration)},Zoomify.prototype.transform=function(o){this.$image.css({"-webkit-transform":o,"-moz-transform":o,"-ms-transform":o,"-o-transform":o,transform:o})},Zoomify.prototype.transformScaleAndTranslate=function(o,i,t,n){this.addTransition(this.$image),this.transform("scale("+o+") translate("+i+"px, "+t+"px)"),this.removeTransition(this.$image,n)},Zoomify.prototype.zoom=function(){this._zooming||(this._zoomed?this.zoomOut():this.zoomIn())},Zoomify.prototype.zoomIn=function(){var o=this,i=this.$image.css("transform");this.transition(this.$image,"none"),this.transform("none");var t=this.$image.offset(),n=this.$image.outerWidth(),s=this.$image.outerHeight(),a=this.$image[0].naturalWidth||1/0,e=this.$image[0].naturalHeight||1/0,m=z(window).width(),r=z(window).height(),h=Math.min(a,m*this.options.scale)/n,d=Math.min(e,r*this.options.scale)/s,f=Math.min(h,d),u=(-t.left+(m-n)/2)/f,c=(-t.top+(r-s)/2+z(document).scrollTop())/f;this.transform(i),this._zooming=!0,this.$image.addClass("zoomed").trigger("zoom-in.zoomify"),setTimeout(function(){o.addShadow(),o.transformScaleAndTranslate(f,u,c,function(){o._zooming=!1,o.$image.trigger("zoom-in-complete.zoomify")}),o._zoomed=!0})},Zoomify.prototype.zoomOut=function(){var o=this;this._zooming=!0,this.$image.trigger("zoom-out.zoomify"),this.transformScaleAndTranslate(1,0,0,function(){o._zooming=!1,o.$image.removeClass("zoomed").trigger("zoom-out-complete.zoomify")}),this.removeShadow(),this._zoomed=!1},Zoomify.prototype.reposition=function(){this._zoomed&&(this.transition(this.$image,"none"),this.zoomIn())},Zoomify.prototype.addShadow=function(){var o=this;this._zoomed||(o.$shadow&&o.$shadow.remove(),this.$shadow=z('<div class="zoomify-shadow"></div>'),z("body").append(this.$shadow),this.addTransition(this.$shadow),this.$shadow.on("click",function(){o.zoomOut()}),setTimeout(function(){o.$shadow.addClass("zoomed")},10))},Zoomify.prototype.removeShadow=function(){var o=this;this.$shadow&&(this.addTransition(this.$shadow),this.$shadow.removeClass("zoomed"),this.$image.one("zoom-out-complete.zoomify",function(){o.$shadow&&o.$shadow.remove(),o.$shadow=null}))},z.fn.zoomify=function(t){return this.each(function(){var o=z(this),i=o.data("zoomify");i||o.data("zoomify",i=new Zoomify(this,"object"==typeof t&&t)),"string"==typeof t&&0<=["zoom","zoomIn","zoomOut","reposition"].indexOf(t)&&i[t]()})}}(jQuery);

    </script>
    <!-- AdminLTE for demo purposes -->
    {{--    <script src="dist/js/demo.js"></script>--}}
    <!-- Ekko Lightbox -->
    <script src="{{ asset('plugins/ekko-lightbox/ekko-lightbox.min.js') }}"></script>
    <!-- Page specific script -->
    <script>

        $(function () {
            $(document).on('click', '[data-toggle="lightbox"]', function(event) {
                event.preventDefault();
                $(this).ekkoLightbox({
                    alwaysShowClose: true
                });
            });
        })
        $(function () {
            $("#example1").DataTable({
                "responsive": true, "lengthChange": false, "autoWidth": false,
                "buttons": ["copy", "csv", "excel", "pdf", "print"]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
            // $('#tbl-pieces').DataTable({
            //     "paging": true,
            //     "lengthChange": true,
            //     "searching": true,
            //     "ordering": true,
            //     "info": true,
            //     "autoWidth": false,
            //     "responsive": true,
            // });
        });

    </script>
    <script>
        $('.send-record').click(function (){

            let id = $(this).data("key");
            var image_id = $(this).data("id");
            let image_src = $(this).data("gallery");
            $('#order_id').val(id);
            $('#image_id').val(image_id);
            $('#image_src').attr('src',image_src);
            // $('#image_a').attr('href',image_src);
            $('#image_src').zoomify(
                {
                    // animation duration
                    duration: 200,

                    // easing effect
                    easing:   'linear',

                    // zoom scale
                    // 1 = fullscreen
                    scale:    0.9
                }
            );
        });
    </script>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $("form").on("submit", function(event){

            event.preventDefault();

            let formData = new FormData(this);
            var formValues= $(this).serialize();

            // console.log(formData);
            $.ajax({
                url: '{{ route('store_order_entry') }}',
                type: 'POST',
                async: true,
                cache: false,
                data: formData,
                contentType: false,
                processData: false,
                success: function (response) {
                    printMsg(response);
                    $("#clock_"+response.image_id).removeClass('fa-clock').addClass('fa-check');
                },
                error: function (msg) {
                    $.each( msg.error, function( key, value ) {
                        toastr.error(value, "Danger");
                    });
                },
            })
        });
        function printMsg (msg) {
            if($.isEmptyObject(msg.error)){
                console.log(msg.success);
                toastr.success(" Your work has been saved", "Success");
                $('input').val('') ;
                $('#modal-xl').modal('hide');

            }else{
                $.each( msg.error, function( key, value ) {
                    toastr.error(value, "Danger");
                });
            }
        }
    </script>
@endpush

{{--@else--}}
{{--@endif--}}
{{--@endauth--}}
